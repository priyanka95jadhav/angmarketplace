import type { CreateUpdateRedeemPointDto, RedeemPointDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class RedeemPointService {
  apiName = 'Default';

  create = (input: CreateUpdateRedeemPointDto) =>
    this.restService.request<any, RedeemPointDto>({
      method: 'POST',
      url: '/api/app/redeem-point',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/redeem-point/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, RedeemPointDto>({
      method: 'GET',
      url: `/api/app/redeem-point/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<RedeemPointDto>>({
      method: 'GET',
      url: '/api/app/redeem-point',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateRedeemPointDto) =>
    this.restService.request<any, RedeemPointDto>({
      method: 'PUT',
      url: `/api/app/redeem-point/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
