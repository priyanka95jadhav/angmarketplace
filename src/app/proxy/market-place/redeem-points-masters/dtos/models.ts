import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateRedeemPointDto {
  rewardPoints: number;
  pointValue: number;
}

export interface RedeemPointDto extends FullAuditedEntityDto<string> {
  rewardPoints: number;
  pointValue: number;
}
