import type { CreateUpdatePunchMasterDto, PunchMasterDto } from './dtos/models';
import type { PunchMasterLst } from './models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';
import type { ActionResult } from '../../microsoft/asp-net-core/mvc/models';

@Injectable({
  providedIn: 'root',
})
export class PunchMasterService {
  apiName = 'Default';

  create = (input: CreateUpdatePunchMasterDto) =>
    this.restService.request<any, PunchMasterDto>({
      method: 'POST',
      url: '/api/app/punch-master',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/punch-master/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, PunchMasterDto>({
      method: 'GET',
      url: `/api/app/punch-master/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<PunchMasterDto>>({
      method: 'GET',
      url: '/api/app/punch-master',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  getPunchHistoryLstByInput = (input: PunchMasterLst) =>
    this.restService.request<any, PagedResultDto<PunchMasterDto>>({
      method: 'GET',
      url: '/api/app/punch-master/punch-history-lst',
      params: { month: input.month, year: input.year, sorting: input.sorting, skipCount: input.skipCount, maxResultCount: input.maxResultCount },
    },
    { apiName: this.apiName });

  insertCheckIn = () =>
    this.restService.request<any, ActionResult>({
      method: 'POST',
      url: '/api/app/punch-master/check-in',
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdatePunchMasterDto) =>
    this.restService.request<any, PunchMasterDto>({
      method: 'PUT',
      url: `/api/app/punch-master/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
