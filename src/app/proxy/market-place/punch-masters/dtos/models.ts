import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdatePunchMasterDto {
  date?: string;
  checkIn?: string;
  checkOut?: string;
  userId?: string;
  month: number;
  year: number;
}

export interface PunchMasterDto extends FullAuditedEntityDto<string> {
  date?: string;
  checkIn?: string;
  checkOut?: string;
  userId?: string;
  month?: string;
}
