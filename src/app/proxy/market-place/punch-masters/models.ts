import type { PagedAndSortedResultRequestDto } from '@abp/ng.core';

export interface PunchMasterLst extends PagedAndSortedResultRequestDto {
  month: number;
  year: number;
}
