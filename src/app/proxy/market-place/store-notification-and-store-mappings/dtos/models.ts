import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateStoreNotificationAndStoreMappingDto {
  storeNotificationId?: string;
  storeId?: string;
}

export interface StoreNotificationAndStoreMappingDto extends FullAuditedEntityDto<string> {
  storeNotificationId?: string;
  storeId?: string;
}
