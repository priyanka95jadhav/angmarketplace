import type { CreateUpdateStoreNotificationAndStoreMappingDto, StoreNotificationAndStoreMappingDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class StoreNotificationAndStoreMappingService {
  apiName = 'Default';

  create = (input: CreateUpdateStoreNotificationAndStoreMappingDto) =>
    this.restService.request<any, StoreNotificationAndStoreMappingDto>({
      method: 'POST',
      url: '/api/app/store-notification-and-store-mapping',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/store-notification-and-store-mapping/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, StoreNotificationAndStoreMappingDto>({
      method: 'GET',
      url: `/api/app/store-notification-and-store-mapping/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<StoreNotificationAndStoreMappingDto>>({
      method: 'GET',
      url: '/api/app/store-notification-and-store-mapping',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateStoreNotificationAndStoreMappingDto) =>
    this.restService.request<any, StoreNotificationAndStoreMappingDto>({
      method: 'PUT',
      url: `/api/app/store-notification-and-store-mapping/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
