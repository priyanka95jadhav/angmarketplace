import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateStoreDocumentMasterDto {
  storeId?: string;
  identificationTypeId?: string;
  storeDocumentName?: string;
  storeDocumentPath?: string;
  expiryDate?: string;
}

export interface StoreDocumentMasterDto extends FullAuditedEntityDto<string> {
  storeId?: string;
  identificationTypeId?: string;
  storeDocumentName?: string;
  storeDocumentPath?: string;
  expiryDate?: string;
}
