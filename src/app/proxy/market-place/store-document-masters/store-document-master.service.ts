import type { CreateUpdateStoreDocumentMasterDto, StoreDocumentMasterDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class StoreDocumentMasterService {
  apiName = 'Default';

  create = (input: CreateUpdateStoreDocumentMasterDto) =>
    this.restService.request<any, StoreDocumentMasterDto>({
      method: 'POST',
      url: '/api/app/store-document-master',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/store-document-master/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, StoreDocumentMasterDto>({
      method: 'GET',
      url: `/api/app/store-document-master/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<StoreDocumentMasterDto>>({
      method: 'GET',
      url: '/api/app/store-document-master',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateStoreDocumentMasterDto) =>
    this.restService.request<any, StoreDocumentMasterDto>({
      method: 'PUT',
      url: `/api/app/store-document-master/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
