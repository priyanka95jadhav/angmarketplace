import type { FullAuditedEntityDto, PagedAndSortedResultRequestDto } from '@abp/ng.core';

export interface CreateUpdateOrderDto {
  number: number;
  totalAmount: number;
  tax_Amount: number;
  discount_Amount: number;
  cancellationReason?: string;
  statusId?: string;
  storeId?: string;
  userId?: string;
  orderDate?: string;
  deliveryDate?: string;
  timeSlotId?: string;
  address?: string;
  deliveryType?: string;
  paymentMode?: string;
  isPaid: boolean;
  paymentId?: string;
}

export interface OrderDto extends FullAuditedEntityDto<string> {
  number: number;
  amount: number;
  tax_Amount: number;
  discount_Amount: number;
  cancellationReason?: string;
  statusId?: string;
  statusName?: string;
  storeId?: string;
  storeName?: string;
  userId?: string;
  orderDate?: string;
  deliveryDate?: string;
  timeSlotId?: string;
  address?: string;
  deliveryType?: string;
  paymentMode?: string;
  isPaid: boolean;
  paymentId?: string;
  totalCount: number;
  isAssigned: boolean;
}

export interface OrderFilterByUser extends PagedAndSortedResultRequestDto {
  userId?: string;
  statusName?: string;
}

export interface OrderHistory extends PagedAndSortedResultRequestDto {
  userId?: string;
  date?: string;
}
