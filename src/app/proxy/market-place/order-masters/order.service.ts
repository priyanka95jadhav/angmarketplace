import type { CreateUpdateOrderDto, OrderDto, OrderFilterByUser, OrderHistory } from './dtos/models';
import type { CreateOrder, OrderStatusUpdate, UpdateCollectedOrder } from './models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';
import type { ActionResult } from '../../microsoft/asp-net-core/mvc/models';

@Injectable({
  providedIn: 'root',
})
export class OrderService {
  apiName = 'Default';

  create = (input: CreateUpdateOrderDto) =>
    this.restService.request<any, OrderDto>({
      method: 'POST',
      url: '/api/app/order',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/order/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, OrderDto>({
      method: 'GET',
      url: `/api/app/order/${id}`,
    },
    { apiName: this.apiName });

  getAssignedOrdersLst = () =>
    this.restService.request<any, ActionResult>({
      method: 'GET',
      url: '/api/app/order/assigned-orders-lst',
    },
    { apiName: this.apiName });

  getAssignedOrdersLstById = (Id: string) =>
    this.restService.request<any, ActionResult>({
      method: 'GET',
      url: `/api/app/order/assigned-orders-lst/${Id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<OrderDto>>({
      method: 'GET',
      url: '/api/app/order',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  getOrderDetailsByOrderId = (OrderId: string) =>
    this.restService.request<any, ActionResult>({
      method: 'GET',
      url: `/api/app/order/order-details/${OrderId}`,
    },
    { apiName: this.apiName });

  getOrderDetailsByUsersByInput = (input: OrderFilterByUser) =>
    this.restService.request<any, ActionResult>({
      method: 'GET',
      url: '/api/app/order/order-details-by-users',
      params: { userId: input.userId, statusName: input.statusName, sorting: input.sorting, skipCount: input.skipCount, maxResultCount: input.maxResultCount },
    },
    { apiName: this.apiName });

  getOrderHistoryByInput = (input: OrderHistory) =>
    this.restService.request<any, ActionResult>({
      method: 'GET',
      url: '/api/app/order/order-history',
      params: { userId: input.userId, date: input.date, sorting: input.sorting, skipCount: input.skipCount, maxResultCount: input.maxResultCount },
    },
    { apiName: this.apiName });

  getProductsByOrderByOrderId = (orderId: string) =>
    this.restService.request<any, ActionResult>({
      method: 'GET',
      url: `/api/app/order/products-by-order/${orderId}`,
    },
    { apiName: this.apiName });

  getTodayRevenue = () =>
    this.restService.request<any, ActionResult>({
      method: 'GET',
      url: '/api/app/order/today-revenue',
    },
    { apiName: this.apiName });

  insertOrderByCreateOrder = (createOrder: CreateOrder) =>
    this.restService.request<any, ActionResult>({
      method: 'POST',
      url: '/api/app/order/InsertOrder',
      body: createOrder,
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateOrderDto) =>
    this.restService.request<any, OrderDto>({
      method: 'PUT',
      url: `/api/app/order/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  updateCollectedProductByInput = (input: UpdateCollectedOrder) =>
    this.restService.request<any, ActionResult>({
      method: 'POST',
      url: '/api/app/order/UpdateCollectedProduct',
      body: input,
    },
    { apiName: this.apiName });

  updateOrderStatusByInput = (input: OrderStatusUpdate) =>
    this.restService.request<any, ActionResult>({
      method: 'POST',
      url: '/api/app/order/UpdateOrderStatus',
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
