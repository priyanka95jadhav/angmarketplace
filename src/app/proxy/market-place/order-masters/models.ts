
export interface CreateOrder {
  storeId?: string;
  timeSlotId?: string;
  address?: string;
  orderAttributes: OrderAttribute[];
}

export interface IsCollectedClass {
  mapId?: string;
  isCollected: boolean;
}

export interface OrderAttribute {
  productId?: string;
  productQuantity: number;
}

export interface OrderStatusUpdate {
  orderId?: string;
  statusNo: number;
  cancellationReason?: string;
}

export interface UpdateCollectedOrder {
  orderId?: string;
  collectedProducts: IsCollectedClass[];
}
