import type { IFormFile } from '../../../microsoft/asp-net-core/http/models';
import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateVehicleDetailDto {
  vehicleNumber?: string;
  vehicleClass?: string;
  pucc?: string;
  rcStatus?: string;
  registrationDate?: string;
  drivingLicenceName?: string;
  drivingLicencePath?: string;
  drivingLicenceImage: IFormFile;
  userId?: string;
}

export interface GetVehicleDetailDto {
  id?: string;
  vehicleNumber?: string;
  vehicleClass?: string;
  pucc?: string;
  rcStatus?: string;
  registrationDate?: string;
  drivingLicenceName?: string;
  drivingLicencePath?: string;
  drivingLicenceImage: IFormFile;
  userId?: string;
}

export interface VehicleDetailDto extends FullAuditedEntityDto<string> {
  vehicleNumber?: string;
  vehicleClass?: string;
  pucc?: string;
  rcStatus?: string;
  registrationDate?: string;
  drivingLicenceName?: string;
  drivingLicencePath?: string;
  userId?: string;
}
