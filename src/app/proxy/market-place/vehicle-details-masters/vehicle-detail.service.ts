import type { CreateUpdateVehicleDetailDto, GetVehicleDetailDto, VehicleDetailDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class VehicleDetailService {
  apiName = 'Default';

  create = (input: CreateUpdateVehicleDetailDto) =>
    this.restService.request<any, VehicleDetailDto>({
      method: 'POST',
      url: '/api/app/vehicle-detail',
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/vehicle-detail/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, VehicleDetailDto>({
      method: 'GET',
      url: `/api/app/vehicle-detail/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<VehicleDetailDto>>({
      method: 'GET',
      url: '/api/app/vehicle-detail',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  getVehicleDetailsByIdByVehicleDetailsId = (VehicleDetailsId: string) =>
    this.restService.request<any, GetVehicleDetailDto>({
      method: 'GET',
      url: `/api/app/vehicle-detail/vehicle-details-by-id/${VehicleDetailsId}`,
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateVehicleDetailDto) =>
    this.restService.request<any, VehicleDetailDto>({
      method: 'PUT',
      url: `/api/app/vehicle-detail/${id}`,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
