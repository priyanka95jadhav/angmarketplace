import type { CreateUpdateOfferDto, OfferDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class OfferService {
  apiName = 'Default';

  create = (input: CreateUpdateOfferDto) =>
    this.restService.request<any, OfferDto>({
      method: 'POST',
      url: '/api/app/offer',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/offer/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, OfferDto>({
      method: 'GET',
      url: `/api/app/offer/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<OfferDto>>({
      method: 'GET',
      url: '/api/app/offer',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateOfferDto) =>
    this.restService.request<any, OfferDto>({
      method: 'PUT',
      url: `/api/app/offer/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
