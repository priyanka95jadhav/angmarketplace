import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateOfferDto {
  isPercentage: boolean;
  offerAmount: number;
  cartValueLimit: number;
  usageLimit: number;
  startDate?: string;
  endDate?: string;
  productId?: string;
  storeId?: string;
}

export interface OfferDto extends FullAuditedEntityDto<string> {
  isPercentage: boolean;
  offerAmount: number;
  cartValueLimit: number;
  usageLimit: number;
  startDate?: string;
  endDate?: string;
  productId?: string;
  storeId?: string;
}
