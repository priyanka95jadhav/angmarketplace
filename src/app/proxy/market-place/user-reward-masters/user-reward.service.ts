import type { CreateUpdateUserRewardDto, UserRewardDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UserRewardService {
  apiName = 'Default';

  create = (input: CreateUpdateUserRewardDto) =>
    this.restService.request<any, UserRewardDto>({
      method: 'POST',
      url: '/api/app/user-reward',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/user-reward/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, UserRewardDto>({
      method: 'GET',
      url: `/api/app/user-reward/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<UserRewardDto>>({
      method: 'GET',
      url: '/api/app/user-reward',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateUserRewardDto) =>
    this.restService.request<any, UserRewardDto>({
      method: 'PUT',
      url: `/api/app/user-reward/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
