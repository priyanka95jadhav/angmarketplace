import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateUserRewardDto {
  rewardCartValue: number;
  rewardPoint: number;
  userId?: string;
}

export interface UserRewardDto extends FullAuditedEntityDto<string> {
  rewardCartValue: number;
  rewardPoint: number;
  userId?: string;
}
