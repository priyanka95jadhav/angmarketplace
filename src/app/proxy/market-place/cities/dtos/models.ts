import type { FullAuditedEntityDto, PagedAndSortedResultRequestDto } from '@abp/ng.core';

export interface CityDto extends FullAuditedEntityDto<string> {
  name?: string;
  stateId?: string;
  stateName?: string;
}

export interface CreateUpdateCityDto {
  name?: string;
  stateId?: string;
}

export interface Json {
  status: boolean;
  message?: string;
  data: object;
}

export interface MySearchFilterDto extends PagedAndSortedResultRequestDto {
  filter?: string;
}
