import { RestService } from '@abp/ng.core';
import { Injectable } from '@angular/core';
import type { ActionResult } from '../../microsoft/asp-net-core/mvc/models';
import type { UsersSelectList } from '../user-masters/models';

@Injectable({
  providedIn: 'root',
})
export class MarketPlaceService {
  apiName = 'Default';

  allUsers = () =>
    this.restService.request<any, UsersSelectList[]>({
      method: 'GET',
      url: '/api/app/users/all',
    },
    { apiName: this.apiName });

  getDrawerscreenData = () =>
    this.restService.request<any, ActionResult>({
      method: 'GET',
      url: '/api/app/drawerscreendata',
    },
    { apiName: this.apiName });

  getHomeScreenData = () =>
    this.restService.request<any, ActionResult>({
      method: 'GET',
      url: '/api/app/homescreendata',
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
