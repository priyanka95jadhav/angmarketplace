import type { Category, CategoryAttributeDto, CreateUpdateCategoryAttributeDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { ListResultDto, PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';
import type { ActionResult } from '../../microsoft/asp-net-core/mvc/models';

@Injectable({
  providedIn: 'root',
})
export class CategoryAttributeService {
  apiName = 'Default';

  checkExistingCategoryByCategoryname = (categoryname: string) =>
    this.restService.request<any, ActionResult>({
      method: 'POST',
      url: '/api/app/category-attribute/check-existing-category',
      params: { categoryname },
    },
    { apiName: this.apiName });

    create = (input: FormData) =>
    this.restService.request<any, CategoryAttributeDto>({
      method: 'POST',
      url: '/api/app/CreateCategory',
      body:input
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/category-attribute/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, CategoryAttributeDto>({
      method: 'GET',
      url: `/api/app/category-attribute/${id}`,
    },
    { apiName: this.apiName });

  getCategoryHierarchyList = () =>
    this.restService.request<any, Category[]>({
      method: 'GET',
      url: '/api/app/category/AllCategory',
    },
    { apiName: this.apiName });

  getCategoryList = (id: string) =>
    this.restService.request<any, ListResultDto<CategoryAttributeDto>>({
      method: 'GET',
      url: `/api/app/category/category-attribute/By${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<CategoryAttributeDto>>({
      method: 'GET',
      url: '/api/app/category-attribute',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  getOnlyMainCategoryList = () =>
    this.restService.request<any, ActionResult>({
      method: 'GET',
      url: '/api/app/category-attribute/only-main-category-list',
    },
    { apiName: this.apiName });

  getSubCategoriesByMainCategoryById = (id: string) =>
    this.restService.request<any, ActionResult>({
      method: 'POST',
      url: '/api/app/category/subcategoriesbymaincategory',
      params: { id },
    },
    { apiName: this.apiName });
    
update = (id: string, input: FormData) =>
    this.restService.request<any, CategoryAttributeDto>({
      method: 'PUT',
      url: `/api/app/category-attribute/${id}`,
      body:input
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
