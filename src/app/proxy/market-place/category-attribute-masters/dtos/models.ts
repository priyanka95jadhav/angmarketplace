import type { FullAuditedEntityDto } from '@abp/ng.core';
import type { IFormFile } from '../../../microsoft/asp-net-core/http/models';

export interface Category {
  categoryId?: string;
  categoryName?: string;
  categoryImageName?: string;
  categoryImagePath?: string;
  subCategoryList: SubCategory[];
}

export interface CategoryAttributeDto extends FullAuditedEntityDto<string> {
  categoryName?: string;
  categoryImageName?: string;
  categoryImagePath?: string;
  parentCategoryId?: string;
  categoryImageFile: IFormFile;
}

export interface CreateUpdateCategoryAttributeDto {
  categoryName?: string;
  categoryImageName?: string;
  categoryImagePath?: string;
  parentCategoryId?: string;
  categoryImageFile: IFormFile;
}

export interface SubCategory {
  subCategoryId?: string;
  subCategoryName?: string;
  categoryImageName?: string;
  categoryImagePath?: string;
  subSubCategoryList: SubSubCategory[];
}

export interface SubSubCategory {
  subSubCategoryId?: string;
  subSubCategoryName?: string;
  categoryImageName?: string;
  categoryImagePath?: string;
}
