import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateStateDto {
  stateName?: string;
  stateCode?: string;
}

export interface StateDto extends FullAuditedEntityDto<string> {
  stateName?: string;
  stateCode?: string;
}
