import type { CreateUpdateStateDto, StateDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { ListResultDto, PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class StateService {
  apiName = 'Default';

  create = (input: CreateUpdateStateDto) =>
    this.restService.request<any, StateDto>({
      method: 'POST',
      url: '/api/app/state',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/state/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, StateDto>({
      method: 'GET',
      url: `/api/app/state/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<StateDto>>({
      method: 'GET',
      url: '/api/app/state',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  getState = () =>
    this.restService.request<any, ListResultDto<StateDto>>({
      method: 'GET',
      url: '/api/app/state/AllState',
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateStateDto) =>
    this.restService.request<any, StateDto>({
      method: 'PUT',
      url: `/api/app/state/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
