import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateStorePayoutValidationDto {
  storeId?: string;
  storeMinAmount: number;
  storeMinDays: number;
}

export interface StorePayoutValidationDto extends FullAuditedEntityDto<string> {
  storeId?: string;
  storeMinAmount: number;
  storeMinDays: number;
}
