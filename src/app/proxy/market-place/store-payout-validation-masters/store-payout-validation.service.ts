import type { CreateUpdateStorePayoutValidationDto, StorePayoutValidationDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class StorePayoutValidationService {
  apiName = 'Default';

  create = (input: CreateUpdateStorePayoutValidationDto) =>
    this.restService.request<any, StorePayoutValidationDto>({
      method: 'POST',
      url: '/api/app/store-payout-validation',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/store-payout-validation/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, StorePayoutValidationDto>({
      method: 'GET',
      url: `/api/app/store-payout-validation/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<StorePayoutValidationDto>>({
      method: 'GET',
      url: '/api/app/store-payout-validation',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateStorePayoutValidationDto) =>
    this.restService.request<any, StorePayoutValidationDto>({
      method: 'PUT',
      url: `/api/app/store-payout-validation/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
