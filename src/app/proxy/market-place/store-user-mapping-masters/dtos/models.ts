import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateStoreUserMappingDto {
  storeId?: string;
  userId?: string;
}

export interface StoreUserMappingDto extends FullAuditedEntityDto<string> {
  storeId?: string;
  userId?: string;
}
