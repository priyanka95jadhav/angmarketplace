import type { CreateUpdateStoreUserMappingDto, StoreUserMappingDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class StoreUserMappingService {
  apiName = 'Default';

  create = (input: CreateUpdateStoreUserMappingDto) =>
    this.restService.request<any, StoreUserMappingDto>({
      method: 'POST',
      url: '/api/app/store-user-mapping',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/store-user-mapping/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, StoreUserMappingDto>({
      method: 'GET',
      url: `/api/app/store-user-mapping/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<StoreUserMappingDto>>({
      method: 'GET',
      url: '/api/app/store-user-mapping',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateStoreUserMappingDto) =>
    this.restService.request<any, StoreUserMappingDto>({
      method: 'PUT',
      url: `/api/app/store-user-mapping/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
