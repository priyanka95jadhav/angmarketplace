import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateFeedbackDto {
  ratings?: string;
  comment?: string;
  orderId?: string;
  userId?: string;
}

export interface FeedbackDto extends FullAuditedEntityDto<string> {
  ratings?: string;
  comment?: string;
  orderId?: string;
  userId?: string;
  orderNumber?: string;
  userName?: string;
}
