import type { CreateUpdateFeedbackDto, FeedbackDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class FeedbackService {
  apiName = 'Default';

  create = (input: CreateUpdateFeedbackDto) =>
    this.restService.request<any, FeedbackDto>({
      method: 'POST',
      url: '/api/app/feedback',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/feedback/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, FeedbackDto>({
      method: 'GET',
      url: `/api/app/feedback/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<FeedbackDto>>({
      method: 'GET',
      url: '/api/app/feedback',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateFeedbackDto) =>
    this.restService.request<any, FeedbackDto>({
      method: 'PUT',
      url: `/api/app/feedback/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
