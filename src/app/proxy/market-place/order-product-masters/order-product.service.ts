import type { CreateUpdateOrderProductDto, OrderProductDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class OrderProductService {
  apiName = 'Default';

  create = (input: CreateUpdateOrderProductDto) =>
    this.restService.request<any, OrderProductDto>({
      method: 'POST',
      url: '/api/app/order-product',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/order-product/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, OrderProductDto>({
      method: 'GET',
      url: `/api/app/order-product/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<OrderProductDto>>({
      method: 'GET',
      url: '/api/app/order-product',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateOrderProductDto) =>
    this.restService.request<any, OrderProductDto>({
      method: 'PUT',
      url: `/api/app/order-product/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
