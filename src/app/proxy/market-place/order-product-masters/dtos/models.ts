import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateOrderProductDto {
  product_Quantity: number;
  orderId?: string;
  productId?: string;
}

export interface OrderProductDto extends FullAuditedEntityDto<string> {
  product_Quantity: number;
  orderId?: string;
  productId?: string;
}
