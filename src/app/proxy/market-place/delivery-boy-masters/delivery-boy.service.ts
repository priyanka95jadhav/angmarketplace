import type { CreateUpdateDeliveryBoyDto, DeliveryBoyDto, DeliveryboyList } from './dtos/models';
import type { DeliveryBoyDocuments, DeliveryBoyProfile } from './models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';
import type { ActionResult } from '../../microsoft/asp-net-core/mvc/models';

@Injectable({
  providedIn: 'root',
})
export class DeliveryBoyService {
  apiName = 'Default';

  addDeliveryBoyByInput = (input: FormData) =>
    this.restService.request<any, ActionResult>({
      method: 'POST',
      url: '/api/app/AddDeliveryBoy',
      body:input
    },
    { apiName: this.apiName });

  allDeliveryBoy = () =>
    this.restService.request<any, DeliveryboyList[]>({
      method: 'GET',
      url: '/api/app/delivery-boy/all-delivery-boy',
    },
    { apiName: this.apiName });

    create = (input: FormData) =>
    this.restService.request<any, DeliveryBoyDto>({
      method: 'POST',
      url: '/api/app/CreateDeliveryBoy',
      body:input
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/delivery-boy/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, DeliveryBoyDto>({
      method: 'GET',
      url: `/api/app/delivery-boy/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<DeliveryBoyDto>>({
      method: 'GET',
      url: '/api/app/delivery-boy',
      params: { sorting: input.sorting, skipCount: input.skipCount, maxResultCount: input.maxResultCount },
    },
    { apiName: this.apiName });

    update = (id: string, input: FormData) =>
    this.restService.request<any, DeliveryBoyDto>({
      method: 'PUT',
      url: `/api/app/delivery-boy/${id}`,
      body:input
    },
    { apiName: this.apiName });

  updateDeliveryBoyByIdAndInput = (id: string, input: FormData) =>
    this.restService.request<any, ActionResult>({
      method: 'POST',
      url: '/api/app/UpdateDeliveryBoy',
      params: { id },
      body:input
    },
    { apiName: this.apiName });

  updateDeliveryBoyDocumentsByInput = (input: DeliveryBoyDocuments) =>
    this.restService.request<any, ActionResult>({
      method: 'POST',
      url: '/api/app/UpdateDeliveryBoyDocuments',
    },
    { apiName: this.apiName });

  updateDeliveryBoyProfileByInput = (input: DeliveryBoyProfile) =>
    this.restService.request<any, ActionResult>({
      method: 'POST',
      url: '/api/app/UpdateDeliveryBoyProfile',
      body: input,
    },
    { apiName: this.apiName });
    
  constructor(private restService: RestService) {}
}
