import type { IFormFile } from '../../microsoft/asp-net-core/http/models';

export interface DeliveryBoyDocuments {
  deliveryBoyId?: string;
  identificationTypeId?: string;
  identificationNumber?: string;
  uploadFile: IFormFile;
  idFileName?: string;
  filePath?: string;
  identificationTypeId1?: string;
  identificationNumber1?: string;
  uploadFile1: IFormFile;
  idFileName1?: string;
  filePath1?: string;
}

export interface DeliveryBoyProfile {
  deliveryBoyId?: string;
  boyName?: string;
  boyPhone?: string;
  boyEmail?: string;
}
