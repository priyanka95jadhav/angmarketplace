import type { IFormFile } from '../../../microsoft/asp-net-core/http/models';
import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateDeliveryBoyDto {
  boyName?: string;
  boyPhone?: string;
  boyUsername?: string;
  boyEmail?: string;
  cityId?: string;
  identificationTypeId?: string;
  identificationNumber?: string;
  FileName?: string;
  address?: string;
  storeId: string[];
  filePath?: string;
  formFile: IFormFile;
  userId?: string;
  identificationTypeId1?: string;
  identificationNumber1?: string;
  idFileName1?: string;
  filePath1?: string;
  formFile1: IFormFile;
  isApproved: boolean;
}

export interface DeliveryBoyDto extends FullAuditedEntityDto<string> {
  boyName?: string;
  boyPhone?: string;
  boyUsername?: string;
  boyEmail?: string;
  cityId?: string;
  cityName?: string;
  identificationTypeId?: string;
  identificationTypeName?: string;
  identificationNumber?: string;
  FileName?: string;
  address?: string;
  storeId: string[];
  filePath?: string;
  userId?: string;
  identificationTypeId1?: string;
  identificationNumber1?: string;
  idFileName1?: string;
  filePath1?: string;
  isApproved: boolean;
}

export interface DeliveryboyList {
  deliveryBoyId?: string;
  boyName?: string;
}
