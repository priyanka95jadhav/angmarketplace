import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateDriverBankDetailDto {
  bankMasterId?: string;
  accountNo?: string;
  ifscCode?: string;
  location?: string;
  userId?: string;
}

export interface DriverBankDetailDto extends FullAuditedEntityDto<string> {
  bankMasterId?: string;
  accountNo?: string;
  ifscCode?: string;
  location?: string;
  userId?: string;
}
