import type { CreateUpdateDriverBankDetailDto, DriverBankDetailDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DriverBankDetailService {
  apiName = 'Default';

  create = (input: CreateUpdateDriverBankDetailDto) =>
    this.restService.request<any, DriverBankDetailDto>({
      method: 'POST',
      url: '/api/app/driver-bank-detail',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/driver-bank-detail/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, DriverBankDetailDto>({
      method: 'GET',
      url: `/api/app/driver-bank-detail/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<DriverBankDetailDto>>({
      method: 'GET',
      url: '/api/app/driver-bank-detail',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateDriverBankDetailDto) =>
    this.restService.request<any, DriverBankDetailDto>({
      method: 'PUT',
      url: `/api/app/driver-bank-detail/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
