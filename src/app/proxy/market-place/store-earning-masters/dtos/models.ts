import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateStoreEarningDto {
  storeId?: string;
  totalRevenue: number;
  alredyPaid: number;
  pendingBalance: number;
}

export interface StoreEarningDto extends FullAuditedEntityDto<string> {
  storeId?: string;
  totalRevenue: number;
  alredyPaid: number;
  pendingBalance: number;
}
