import type { CreateUpdateStoreEarningDto, StoreEarningDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class StoreEarningService {
  apiName = 'Default';

  create = (input: CreateUpdateStoreEarningDto) =>
    this.restService.request<any, StoreEarningDto>({
      method: 'POST',
      url: '/api/app/store-earning',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/store-earning/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, StoreEarningDto>({
      method: 'GET',
      url: `/api/app/store-earning/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<StoreEarningDto>>({
      method: 'GET',
      url: '/api/app/store-earning',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateStoreEarningDto) =>
    this.restService.request<any, StoreEarningDto>({
      method: 'PUT',
      url: `/api/app/store-earning/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
