import type { IFormFile } from '../../../microsoft/asp-net-core/http/models';
import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateSettingMasterDto {
  appName?: string;
  countryCode: number;
  phoneNumberLength: number;
  siteLogoName?: string;
  siteLogoFile: IFormFile;
  siteLogoPath?: string;
  faviconName?: string;
  faviconFile: IFormFile;
  faviconPath?: string;
  keywordsFooterText?: string;
  androidAppLink?: string;
  iosLink?: string;
}

export interface SettingMasterDto extends FullAuditedEntityDto<string> {
  appName?: string;
  countryCode: number;
  phoneNumberLength: number;
  siteLogoName?: string;
  siteLogoPath?: string;
  faviconName?: string;
  faviconPath?: string;
  keywordsFooterText?: string;
  androidAppLink?: string;
  iosLink?: string;
}
