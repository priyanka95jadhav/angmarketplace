import type { CreateUpdateSettingMasterDto, SettingMasterDto } from './dtos/models';
import type { Jsons, RegisterUsersDto, SettingMaster, VerifyUserDto } from './models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';
import type { ActionResult } from '../../microsoft/asp-net-core/mvc/models';

@Injectable({
  providedIn: 'root',
})
export class SettingMasterService {
  apiName = 'Default';

  create = (input: CreateUpdateSettingMasterDto) =>
    this.restService.request<any, SettingMasterDto>({
      method: 'POST',
      url: '/api/app/setting-master',
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/setting-master/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, SettingMasterDto>({
      method: 'GET',
      url: `/api/app/setting-master/${id}`,
    },
    { apiName: this.apiName });

  getDashBoardData = () =>
    this.restService.request<any, ActionResult>({
      method: 'GET',
      url: '/api/app/setting-master/dash-board-data',
    },
    { apiName: this.apiName });

  getIntroData = () =>
    this.restService.request<any, ActionResult>({
      method: 'GET',
      url: '/api/app/introdata',
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<SettingMasterDto>>({
      method: 'GET',
      url: '/api/app/setting-master',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  getSettingDetails = () =>
    this.restService.request<any, SettingMaster>({
      method: 'GET',
      url: '/api/app/settingdetails',
    },
    { apiName: this.apiName });

  registerEndUserByUserDto = (userDto: RegisterUsersDto) =>
    this.restService.request<any, Jsons>({
      method: 'POST',
      url: '/api/app/registerenduser',
    },
    { apiName: this.apiName });

   update = (id: string, input: FormData) =>
    this.restService.request<any, SettingMasterDto>({
      method: 'PUT',
      url: `/api/app/setting-master/${id}`,
      body:input
    },
    { apiName: this.apiName });

  verifyMobileByUserdetails = (userdetails: VerifyUserDto) =>
    this.restService.request<any, Jsons>({
      method: 'POST',
      url: '/api/app/verifymobile',
      body: userdetails,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
