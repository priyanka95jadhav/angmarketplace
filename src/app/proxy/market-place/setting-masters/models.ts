import type { FullAuditedEntity } from '../../volo/abp/domain/entities/auditing/models';

export interface Jsons {
  status: boolean;
  message?: string;
  data: object;
}

export interface RegisterUsersDto {
  userName?: string;
  mobileNumber?: string;
  email?: string;
  password?: string;
  confirmPassword?: string;
}

export interface SettingMaster extends FullAuditedEntity<string> {
  appName?: string;
  countryCode: number;
  phoneNumberLength: number;
  siteLogoName?: string;
  siteLogoPath?: string;
  faviconName?: string;
  faviconPath?: string;
  keywordsFooterText?: string;
  androidAppLink?: string;
  iosLink?: string;
}

export interface VerifyUserDto {
  userName?: string;
  countryCode?: string;
  mobileNumber?: string;
  email?: string;
  password?: string;
  confirmPassword?: string;
  otp?: string;
}
