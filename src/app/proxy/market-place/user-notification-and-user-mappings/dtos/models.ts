import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateUserNotificationAndUserMappingDto {
  userNotificationId?: string;
  userId?: string;
}

export interface UserNotificationAndUserMappingDto extends FullAuditedEntityDto<string> {
  userNotificationId?: string;
  userId?: string;
}
