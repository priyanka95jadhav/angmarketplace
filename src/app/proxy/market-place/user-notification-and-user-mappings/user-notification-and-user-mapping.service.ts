import type { CreateUpdateUserNotificationAndUserMappingDto, UserNotificationAndUserMappingDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UserNotificationAndUserMappingService {
  apiName = 'Default';

  create = (input: CreateUpdateUserNotificationAndUserMappingDto) =>
    this.restService.request<any, UserNotificationAndUserMappingDto>({
      method: 'POST',
      url: '/api/app/user-notification-and-user-mapping',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/user-notification-and-user-mapping/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, UserNotificationAndUserMappingDto>({
      method: 'GET',
      url: `/api/app/user-notification-and-user-mapping/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<UserNotificationAndUserMappingDto>>({
      method: 'GET',
      url: '/api/app/user-notification-and-user-mapping',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateUserNotificationAndUserMappingDto) =>
    this.restService.request<any, UserNotificationAndUserMappingDto>({
      method: 'PUT',
      url: `/api/app/user-notification-and-user-mapping/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
