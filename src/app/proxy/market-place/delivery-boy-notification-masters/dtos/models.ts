import type { IFormFile } from '../../../microsoft/asp-net-core/http/models';
import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateDeliveryBoyNotificationMasterDto {
  title?: string;
  message?: string;
  imageFileName?: string;
  imageFilePath?: string;
  deliveryBoyId: string[];
  deliveryBoyNotificationImageFile: IFormFile;
}

export interface DeliveryBoyNotificationMasterDto extends FullAuditedEntityDto<string> {
  title?: string;
  message?: string;
  imageFileName?: string;
  imageFilePath?: string;
  deliveryBoy: DeliveryBoydto[];
}

export interface DeliveryBoydto {
  deliveryBoyId?: string;
  deliveryBoyName?: string;
}
