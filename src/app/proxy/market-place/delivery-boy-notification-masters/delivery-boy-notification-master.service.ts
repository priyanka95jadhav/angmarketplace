import type { CreateUpdateDeliveryBoyNotificationMasterDto, DeliveryBoyNotificationMasterDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DeliveryBoyNotificationMasterService {
  apiName = 'Default';

  create = (input: FormData) =>
  this.restService.request<any, DeliveryBoyNotificationMasterDto>({
    method: 'POST',
    url: '/api/app/delivery-boy-notification-master',
    body:input
  },
  { apiName: this.apiName });

delete = (id: string) =>
  this.restService.request<any, void>({
    method: 'DELETE',
    url: `/api/app/delivery-boy-notification-master/${id}`,
  },
  { apiName: this.apiName });

get = (id: string) =>
  this.restService.request<any, DeliveryBoyNotificationMasterDto>({
    method: 'GET',
    url: `/api/app/delivery-boy-notification-master/${id}`,
  },
  { apiName: this.apiName });

getDetailsByIdById = (id: string) =>
  this.restService.request<any, CreateUpdateDeliveryBoyNotificationMasterDto>({
    method: 'GET',
    url: `/api/app/deliveryboy-notification-master/GetDetailsById/${id}`,
  },
  { apiName: this.apiName });

getList = (input: PagedAndSortedResultRequestDto) =>
  this.restService.request<any, PagedResultDto<DeliveryBoyNotificationMasterDto>>({
    method: 'GET',
    url: '/api/app/delivery-boy-notification-master',
    params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
  },
  { apiName: this.apiName });

update = (id: string, input: CreateUpdateDeliveryBoyNotificationMasterDto) =>
  this.restService.request<any, DeliveryBoyNotificationMasterDto>({
    method: 'PUT',
    url: `/api/app/delivery-boy-notification-master/${id}`,
    body: input,
  },
  { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
