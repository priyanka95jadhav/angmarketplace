import type { CreateUpdateStorePayoutRequestDto, StorePayoutRequestDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class StorePayoutRequestService {
  apiName = 'Default';

  create = (input: CreateUpdateStorePayoutRequestDto) =>
    this.restService.request<any, StorePayoutRequestDto>({
      method: 'POST',
      url: '/api/app/store-payout-request',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/store-payout-request/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, StorePayoutRequestDto>({
      method: 'GET',
      url: `/api/app/store-payout-request/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<StorePayoutRequestDto>>({
      method: 'GET',
      url: '/api/app/store-payout-request',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateStorePayoutRequestDto) =>
    this.restService.request<any, StorePayoutRequestDto>({
      method: 'PUT',
      url: `/api/app/store-payout-request/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
