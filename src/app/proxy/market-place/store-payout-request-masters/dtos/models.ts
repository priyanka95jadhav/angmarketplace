import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateStorePayoutRequestDto {
  storeTotalRevenue: number;
  storeBankAccountNo: number;
  accountIFSCCode?: string;
  alreadyPaid: number;
  pendingAmount: number;
  amount: number;
}

export interface StorePayoutRequestDto extends FullAuditedEntityDto<string> {
  storeTotalRevenue: number;
  storeBankAccountNo: number;
  accountIFSCCode?: string;
  alreadyPaid: number;
  pendingAmount: number;
  amount: number;
}
