import type { AreaDto, CreateUpdateAreaDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { ListResultDto, PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AreaService {
  apiName = 'Default';

  create = (input: CreateUpdateAreaDto) =>
    this.restService.request<any, AreaDto>({
      method: 'POST',
      url: '/api/app/area',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/area/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, AreaDto>({
      method: 'GET',
      url: `/api/app/area/${id}`,
    },
    { apiName: this.apiName });

  getAllArea = () =>
    this.restService.request<any, ListResultDto<AreaDto>>({
      method: 'GET',
      url: '/api/app/area/AllArea',
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<AreaDto>>({
      method: 'GET',
      url: '/api/app/area',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateAreaDto) =>
    this.restService.request<any, AreaDto>({
      method: 'PUT',
      url: `/api/app/area/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
