import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface AreaDto extends FullAuditedEntityDto<string> {
  areaAddress?: string;
  cityId?: string;
  cityName?: string;
}

export interface CreateUpdateAreaDto {
  areaAddress?: string;
  cityId?: string;
}
