import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateUnitMasterDto {
  unitName?: string;
  unitCode?: string;
}

export interface UnitMasterDto extends FullAuditedEntityDto<string> {
  unitName?: string;
  unitCode?: string;
}
