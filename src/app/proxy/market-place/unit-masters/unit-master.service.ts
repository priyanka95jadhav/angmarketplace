import type { CreateUpdateUnitMasterDto, UnitMasterDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UnitMasterService {
  apiName = 'Default';

  create = (input: CreateUpdateUnitMasterDto) =>
    this.restService.request<any, UnitMasterDto>({
      method: 'POST',
      url: '/api/app/unit-master',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/unit-master/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, UnitMasterDto>({
      method: 'GET',
      url: `/api/app/unit-master/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<UnitMasterDto>>({
      method: 'GET',
      url: '/api/app/unit-master',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateUnitMasterDto) =>
    this.restService.request<any, UnitMasterDto>({
      method: 'PUT',
      url: `/api/app/unit-master/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
