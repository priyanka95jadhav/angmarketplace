import type { CreateUpdateUserNotificationMasterDto, UserNotificationMasterDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class UserNotificationMasterService {
  apiName = 'Default';

  create = (input: FormData) =>
  this.restService.request<any, UserNotificationMasterDto>({
    method: 'POST',
    url: '/api/app/user-notification-master',
  },
  { apiName: this.apiName });

delete = (id: string) =>
  this.restService.request<any, void>({
    method: 'DELETE',
    url: `/api/app/user-notification-master/${id}`,
  },
  { apiName: this.apiName });

get = (id: string) =>
  this.restService.request<any, UserNotificationMasterDto>({
    method: 'GET',
    url: `/api/app/user-notification-master/${id}`,
  },
  { apiName: this.apiName });

getDetailsByIdById = (id: string) =>
  this.restService.request<any, CreateUpdateUserNotificationMasterDto>({
    method: 'GET',
    url: `/api/app/user-notification-master/GetDetailsById/${id}`,
  },
  { apiName: this.apiName });

getList = (input: PagedAndSortedResultRequestDto) =>
  this.restService.request<any, PagedResultDto<UserNotificationMasterDto>>({
    method: 'GET',
    url: '/api/app/user-notification-master',
    params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
  },
  { apiName: this.apiName });

update = (id: string, input: CreateUpdateUserNotificationMasterDto) =>
  this.restService.request<any, UserNotificationMasterDto>({
    method: 'PUT',
    url: `/api/app/user-notification-master/${id}`,
    body: input,
  },
  { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
