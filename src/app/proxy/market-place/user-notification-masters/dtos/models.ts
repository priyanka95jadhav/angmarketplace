import type { IFormFile } from '../../../microsoft/asp-net-core/http/models';
import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateUserNotificationMasterDto {
  title?: string;
  message?: string;
  imageFileName?: string;
  imageFilePath?: string;
  userId: string[];
  userNotificationImageFile: IFormFile;
}

export interface UserNotificationMasterDto extends FullAuditedEntityDto<string> {
  title?: string;
  message?: string;
  imageFileName?: string;
  imageFilePath?: string;
  user: UsersDto[];
}

export interface UsersDto {
  userId?: string;
  userName?: string;
}
