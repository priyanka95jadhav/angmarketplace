import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface BankList {
  bankId?: string;
  bankName?: string;
}

export interface BankMasterDto extends FullAuditedEntityDto<string> {
  bankName?: string;
}

export interface CreateUpdateBankMasterDto {
  bankName?: string;
}
