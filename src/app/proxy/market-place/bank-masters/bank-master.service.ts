import type { BankList, BankMasterDto, CreateUpdateBankMasterDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class BankMasterService {
  apiName = 'Default';

  create = (input: CreateUpdateBankMasterDto) =>
    this.restService.request<any, BankMasterDto>({
      method: 'POST',
      url: '/api/app/bank-master',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/bank-master/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, BankMasterDto>({
      method: 'GET',
      url: `/api/app/bank-master/${id}`,
    },
    { apiName: this.apiName });

  getAllBank = () =>
    this.restService.request<any, BankList[]>({
      method: 'GET',
      url: '/api/app/bank-master/bank',
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<BankMasterDto>>({
      method: 'GET',
      url: '/api/app/bank-master',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateBankMasterDto) =>
    this.restService.request<any, BankMasterDto>({
      method: 'PUT',
      url: `/api/app/bank-master/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
