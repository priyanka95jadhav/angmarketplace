import type { IFormFile } from '../../../microsoft/asp-net-core/http/models';
import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateStoreNotificationMasterDto {
  title?: string;
  message?: string;
  imageFileName?: string;
  imageFilePath?: string;
  storeId: string[];
  storeNotificationImageFile: IFormFile;
}

export interface StoreNotificationMasterDto extends FullAuditedEntityDto<string> {
  title?: string;
  message?: string;
  imageFileName?: string;
  imageFilePath?: string;
  store: StoresDto[];
}

export interface StoresDto {
  storeId?: string;
  storeName?: string;
}
