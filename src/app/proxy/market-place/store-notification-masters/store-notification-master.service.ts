import type { CreateUpdateStoreNotificationMasterDto, StoreNotificationMasterDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class StoreNotificationMasterService {
  apiName = 'Default';

  create = (input: FormData) =>
    this.restService.request<any, StoreNotificationMasterDto>({
      method: 'POST',
      url: '/api/app/store-notification-master',
      body:input

    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/store-notification-master/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, StoreNotificationMasterDto>({
      method: 'GET',
      url: `/api/app/store-notification-master/${id}`,
    },
    { apiName: this.apiName });

  getDetailsByIdById = (id: string) =>
    this.restService.request<any, CreateUpdateStoreNotificationMasterDto>({
      method: 'GET',
      url: `/api/app/store-notification-master/GetDetailsById/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<StoreNotificationMasterDto>>({
      method: 'GET',
      url: '/api/app/store-notification-master',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateStoreNotificationMasterDto) =>
    this.restService.request<any, StoreNotificationMasterDto>({
      method: 'PUT',
      url: `/api/app/store-notification-master/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
