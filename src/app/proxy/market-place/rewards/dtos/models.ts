import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateRewardDto {
  cartValue: number;
  rewardPoints: number;
}

export interface RewardDto extends FullAuditedEntityDto<string> {
  cartValue: number;
  rewardPoints: number;
}
