import type { CreateUpdateRewardDto, RewardDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class RewardService {
  apiName = 'Default';

  create = (input: CreateUpdateRewardDto) =>
    this.restService.request<any, RewardDto>({
      method: 'POST',
      url: '/api/app/reward',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/reward/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, RewardDto>({
      method: 'GET',
      url: `/api/app/reward/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<RewardDto>>({
      method: 'GET',
      url: '/api/app/reward',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateRewardDto) =>
    this.restService.request<any, RewardDto>({
      method: 'PUT',
      url: `/api/app/reward/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
