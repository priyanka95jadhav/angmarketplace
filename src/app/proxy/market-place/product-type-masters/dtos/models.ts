import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateProductTypeDto {
  productTypeName?: string;
}

export interface ProductTypeDto extends FullAuditedEntityDto<string> {
  productTypeName?: string;
}

export interface ProductTypeList {
  productTypeId?: string;
  productTypeName?: string;
}
