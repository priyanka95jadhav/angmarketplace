import type { CreateUpdateProductTypeDto, ProductTypeDto, ProductTypeList } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ProductTypeService {
  apiName = 'Default';

  allProductType = () =>
    this.restService.request<any, ProductTypeList[]>({
      method: 'GET',
      url: '/api/app/product-type/all-product-type',
    },
    { apiName: this.apiName });

  create = (input: CreateUpdateProductTypeDto) =>
    this.restService.request<any, ProductTypeDto>({
      method: 'POST',
      url: '/api/app/product-type',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/product-type/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, ProductTypeDto>({
      method: 'GET',
      url: `/api/app/product-type/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<ProductTypeDto>>({
      method: 'GET',
      url: '/api/app/product-type',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateProductTypeDto) =>
    this.restService.request<any, ProductTypeDto>({
      method: 'PUT',
      url: `/api/app/product-type/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
