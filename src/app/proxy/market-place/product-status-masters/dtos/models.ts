import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateProductStatusDto {
  productStatusName?: string;
}

export interface ProductStatusDto extends FullAuditedEntityDto<string> {
  productStatusName?: string;
}

export interface ProductStatusList {
  productStatusId?: string;
  productStatusName?: string;
}
