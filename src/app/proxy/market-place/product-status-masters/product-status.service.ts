import type { CreateUpdateProductStatusDto, ProductStatusDto, ProductStatusList } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ProductStatusService {
  apiName = 'Default';

  allProductStatus = () =>
    this.restService.request<any, ProductStatusList[]>({
      method: 'GET',
      url: '/api/app/product-status/all-product-status',
    },
    { apiName: this.apiName });

  create = (input: CreateUpdateProductStatusDto) =>
    this.restService.request<any, ProductStatusDto>({
      method: 'POST',
      url: '/api/app/product-status',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/product-status/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, ProductStatusDto>({
      method: 'GET',
      url: `/api/app/product-status/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<ProductStatusDto>>({
      method: 'GET',
      url: '/api/app/product-status',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateProductStatusDto) =>
    this.restService.request<any, ProductStatusDto>({
      method: 'PUT',
      url: `/api/app/product-status/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
