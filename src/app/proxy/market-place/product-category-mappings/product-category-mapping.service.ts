import type { CreateUpdateProductCategoryMappingDto, ProductCategoryMappingDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ProductCategoryMappingService {
  apiName = 'Default';

  create = (input: CreateUpdateProductCategoryMappingDto) =>
    this.restService.request<any, ProductCategoryMappingDto>({
      method: 'POST',
      url: '/api/app/product-category-mapping',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/product-category-mapping/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, ProductCategoryMappingDto>({
      method: 'GET',
      url: `/api/app/product-category-mapping/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<ProductCategoryMappingDto>>({
      method: 'GET',
      url: '/api/app/product-category-mapping',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateProductCategoryMappingDto) =>
    this.restService.request<any, ProductCategoryMappingDto>({
      method: 'PUT',
      url: `/api/app/product-category-mapping/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
