import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateProductCategoryMappingDto {
  productId?: string;
  categoryAttrId?: string;
  categoryAttrName?: string;
  categoryAttrValue?: string;
}

export interface ProductCategoryMappingDto extends FullAuditedEntityDto<string> {
  productId?: string;
  categoryAttrId?: string;
  categoryAttrName?: string;
  categoryAttrValue?: string;
}
