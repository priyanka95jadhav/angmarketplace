import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateOrderAndProductMappingDto {
  orderId?: string;
  productName?: string;
  productMainImagePath?: string;
  productAttributeName?: string;
  productQuantity: number;
  isCollected: boolean;
  mrp: number;
  productDiscount: number;
  price: number;
}

export interface OrderAndProductMappingDto extends FullAuditedEntityDto<string> {
  orderId?: string;
  productName?: string;
  productMainImagePath?: string;
  productAttributeName?: string;
  productQuantity: number;
  isCollected: boolean;
  mrp: number;
  productDiscount: number;
  price: number;
}
