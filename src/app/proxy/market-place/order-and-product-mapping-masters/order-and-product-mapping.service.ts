import type { CreateUpdateOrderAndProductMappingDto, OrderAndProductMappingDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class OrderAndProductMappingService {
  apiName = 'Default';

  create = (input: CreateUpdateOrderAndProductMappingDto) =>
    this.restService.request<any, OrderAndProductMappingDto>({
      method: 'POST',
      url: '/api/app/order-and-product-mapping',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/order-and-product-mapping/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, OrderAndProductMappingDto>({
      method: 'GET',
      url: `/api/app/order-and-product-mapping/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<OrderAndProductMappingDto>>({
      method: 'GET',
      url: '/api/app/order-and-product-mapping',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateOrderAndProductMappingDto) =>
    this.restService.request<any, OrderAndProductMappingDto>({
      method: 'PUT',
      url: `/api/app/order-and-product-mapping/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
