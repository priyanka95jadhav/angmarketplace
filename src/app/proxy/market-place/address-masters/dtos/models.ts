import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface AddressDto extends FullAuditedEntityDto<string> {
  firstName?: string;
  lastName?: string;
  customerAddress?: string;
  landMark?: string;
  pinCode: number;
  area?: string;
  cityId?: string;
  stateId?: string;
  phoneNo?: string;
  addressType?: string;
  isDefault: boolean;
  userId?: string;
}

export interface CreateUpdateAddressDto {
  firstName?: string;
  lastName?: string;
  customerAddress?: string;
  landMark?: string;
  pinCode: number;
  area?: string;
  cityId?: string;
  stateId?: string;
  phoneNo?: string;
  addressType?: string;
  isDefault: boolean;
  userId?: string;
}
