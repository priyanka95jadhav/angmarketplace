import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateRatingDto {
  ratings: number;
  comments?: string;
  productId?: string;
  storeId?: string;
  userId?: string;
}

export interface RatingDto extends FullAuditedEntityDto<string> {
  ratings: number;
  comments?: string;
  productId?: string;
  storeId?: string;
  userId?: string;
}
