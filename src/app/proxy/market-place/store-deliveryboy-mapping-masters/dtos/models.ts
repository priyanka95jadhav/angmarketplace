import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateStoreDeliveryboyMappingDto {
  deliveryboyId?: string;
  storeId?: string;
}

export interface StoreDeliveryboyMappingDto extends FullAuditedEntityDto<string> {
  deliveryboyId?: string;
  storeId?: string;
}
