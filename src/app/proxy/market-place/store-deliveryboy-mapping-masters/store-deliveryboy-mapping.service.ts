import type { CreateUpdateStoreDeliveryboyMappingDto, StoreDeliveryboyMappingDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class StoreDeliveryboyMappingService {
  apiName = 'Default';

  create = (input: CreateUpdateStoreDeliveryboyMappingDto) =>
    this.restService.request<any, StoreDeliveryboyMappingDto>({
      method: 'POST',
      url: '/api/app/store-deliveryboy-mapping',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/store-deliveryboy-mapping/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, StoreDeliveryboyMappingDto>({
      method: 'GET',
      url: `/api/app/store-deliveryboy-mapping/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<StoreDeliveryboyMappingDto>>({
      method: 'GET',
      url: '/api/app/store-deliveryboy-mapping',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateStoreDeliveryboyMappingDto) =>
    this.restService.request<any, StoreDeliveryboyMappingDto>({
      method: 'PUT',
      url: `/api/app/store-deliveryboy-mapping/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
