import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateStoreImageDto {
  storeImageFile?: string;
  storeId?: string;
  imagePath?: string;
}

export interface StoreImageDto extends FullAuditedEntityDto<string> {
  storeImageFile?: string;
  storeId?: string;
  imagePath?: string;
}
