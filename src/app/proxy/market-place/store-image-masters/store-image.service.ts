import type { CreateUpdateStoreImageDto, StoreImageDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class StoreImageService {
  apiName = 'Default';

  create = (input: CreateUpdateStoreImageDto) =>
    this.restService.request<any, StoreImageDto>({
      method: 'POST',
      url: '/api/app/store-image',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/store-image/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, StoreImageDto>({
      method: 'GET',
      url: `/api/app/store-image/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<StoreImageDto>>({
      method: 'GET',
      url: '/api/app/store-image',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateStoreImageDto) =>
    this.restService.request<any, StoreImageDto>({
      method: 'PUT',
      url: `/api/app/store-image/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
