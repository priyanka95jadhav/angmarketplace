import type { CreateUpdateProductAttributeDto, MainAttribute, ProductAttributeDto, ProductAttributeList } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { ListResultDto, PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ProductAttributeService {
  apiName = 'Default';

  allProductAttributes = () =>
    this.restService.request<any, ProductAttributeList[]>({
      method: 'GET',
      url: '/api/app/product-attribute/all-product-attributes',
    },
    { apiName: this.apiName });

  create = (input: CreateUpdateProductAttributeDto) =>
    this.restService.request<any, ProductAttributeDto>({
      method: 'POST',
      url: '/api/app/product-attribute',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/product-attribute/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, ProductAttributeDto>({
      method: 'GET',
      url: `/api/app/product-attribute/${id}`,
    },
    { apiName: this.apiName });

  getCategoryHierarchyList = () =>
    this.restService.request<any, MainAttribute[]>({
      method: 'GET',
      url: '/api/app/attribute/AllAttributeHierarchy',
    },
    { apiName: this.apiName });

  getCategoryList = (id: string) =>
    this.restService.request<any, ListResultDto<ProductAttributeDto>>({
      method: 'GET',
      url: `/api/app/product/product-attribute/By${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<ProductAttributeDto>>({
      method: 'GET',
      url: '/api/app/product-attribute',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateProductAttributeDto) =>
    this.restService.request<any, ProductAttributeDto>({
      method: 'PUT',
      url: `/api/app/product-attribute/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
