import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateProductAttributeDto {
  attributeName?: string;
  parentAttributeId?: string;
}

export interface MainAttribute {
  attributeId?: string;
  attributeName?: string;
  subAttributeList: SubAttribute[];
}

export interface ProductAttributeDto extends FullAuditedEntityDto<string> {
  attributeName?: string;
  parentAttributeId?: string;
}

export interface ProductAttributeList {
  productAttributeId?: string;
  productAttributeName?: string;
}

export interface SubAttribute {
  subAttributeId?: string;
  subAttributeName?: string;
}
