import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateCurrencyDto {
  currencyName?: string;
  currencySymbol?: string;
}

export interface CurrencyDto extends FullAuditedEntityDto<string> {
  currencyName?: string;
  currencySymbol?: string;
}
