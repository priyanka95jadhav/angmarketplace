import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateProductAndProductAttributeMappingDto {
  productId?: string;
  productAttributeId?: string;
  productStatusId?: string;
  productSubAttributeId?: string;
  productAttributeName?: string;
  productAttributeValue?: string;
  productDescription?: string;
  productMainImg?: string;
  productMainImgPath?: string;
  productSKU?: string;
  productPrice: number;
  productMRP: number;
  productMaxQty: number;
  productDiscount: number;
  isApproved: boolean;
  isActive: boolean;
}

export interface ProductAndProductAttributeMappingDto extends FullAuditedEntityDto<string> {
  productId?: string;
  productAttributeId?: string;
  productStatusId?: string;
  productSubAttributeId?: string;
  productAttributeName?: string;
  productAttributeValue?: string;
  productDescription?: string;
  productMainImg?: string;
  productMainImgPath?: string;
  productSKU?: string;
  productPrice: number;
  productMRP: number;
  productMaxQty: number;
  productDiscount: number;
  isApproved: boolean;
  isActive: boolean;
}
