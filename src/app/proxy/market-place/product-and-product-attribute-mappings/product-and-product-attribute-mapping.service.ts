import type { CreateUpdateProductAndProductAttributeMappingDto, ProductAndProductAttributeMappingDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ProductAndProductAttributeMappingService {
  apiName = 'Default';

  create = (input: CreateUpdateProductAndProductAttributeMappingDto) =>
    this.restService.request<any, ProductAndProductAttributeMappingDto>({
      method: 'POST',
      url: '/api/app/product-and-product-attribute-mapping',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/product-and-product-attribute-mapping/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, ProductAndProductAttributeMappingDto>({
      method: 'GET',
      url: `/api/app/product-and-product-attribute-mapping/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<ProductAndProductAttributeMappingDto>>({
      method: 'GET',
      url: '/api/app/product-and-product-attribute-mapping',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateProductAndProductAttributeMappingDto) =>
    this.restService.request<any, ProductAndProductAttributeMappingDto>({
      method: 'PUT',
      url: `/api/app/product-and-product-attribute-mapping/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
