import type { FullAuditedEntity } from '../../volo/abp/domain/entities/auditing/models';

export interface AddToCart extends FullAuditedEntity<string> {
  productQuantity: number;
  productId?: string;
}

export interface AddToCartAppService+CartDetails {
  proAttrId?: string;
  quantity: number;
}
