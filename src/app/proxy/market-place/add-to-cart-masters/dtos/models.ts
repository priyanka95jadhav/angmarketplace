import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface AddToCartDto extends FullAuditedEntityDto<string> {
  productQuantity: number;
  productId?: string;
}

export interface CreateUpdateAddToCartDto {
  productQuantity: number;
  productId?: string;
}
