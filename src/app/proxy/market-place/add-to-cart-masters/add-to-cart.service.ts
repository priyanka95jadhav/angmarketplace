import type { AddToCartDto, CreateUpdateAddToCartDto } from './dtos/models';
import type { AddToCart, AddToCartAppService+CartDetails } from './models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';
import type { ActionResult } from '../../microsoft/asp-net-core/mvc/models';

@Injectable({
  providedIn: 'root',
})
export class AddToCartService {
  apiName = 'Default';

  create = (input: CreateUpdateAddToCartDto) =>
    this.restService.request<any, AddToCartDto>({
      method: 'POST',
      url: '/api/app/add-to-cart',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/add-to-cart/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, AddToCartDto>({
      method: 'GET',
      url: `/api/app/add-to-cart/${id}`,
    },
    { apiName: this.apiName });

  getByUserIdByUser_id = (user_id: string) =>
    this.restService.request<any, AddToCart[]>({
      method: 'GET',
      url: '/api/app/add-to-cart/by-user-id',
      params: { user_id },
    },
    { apiName: this.apiName });

  getCartDetailsByCartDetails = (cartDetails: AddToCartAppService+CartDetails[]) =>
    this.restService.request<any, ActionResult>({
      method: 'GET',
      url: '/api/app/add-to-cart/cart-details',
      params: { cartDetails },
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<AddToCartDto>>({
      method: 'GET',
      url: '/api/app/add-to-cart',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateAddToCartDto) =>
    this.restService.request<any, AddToCartDto>({
      method: 'PUT',
      url: `/api/app/add-to-cart/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
