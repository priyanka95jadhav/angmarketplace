import type { IFormFile } from '../../../microsoft/asp-net-core/http/models';
import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateStoreMasterDto {
  name?: string;
  storeNo?: string;
  storeAddress?: string;
  storeLattitude: number;
  storeLongitude: number;
  adminshare?: string;
  storeRange: number;
  storeOrderPerTimeSlot: number;
  storeStartTime?: string;
  storeEndTime?: string;
  storeTimeSlotInterval: number;
  storeApporval: boolean;
  areaId?: string;
  storeTypeId?: string;
  tenantId?: string;
  storeOwnerAadhar?: string;
  storeOwnerPAN?: string;
}

export interface EditGetRoles {
  roleId?: string;
  roleName?: string;
}

export interface EditGetStoreDocumentsList {
  identificationTypeId?: string;
  storeDocId?: string;
  identificationTypeName?: string;
  identificationFileName?: string;
  identificationFilePath?: string;
  expiryDate?: string;
}

export interface EditGetStoreImagesList {
  storeImgId?: string;
  storeImageFileName?: string;
  storeImageFilePath?: string;
}

export interface EditGetStoreMasterDto {
  storeId?: string;
  tenantId?: string;
  name?: string;
  storeNo?: string;
  storeAddress?: string;
  storeLattitude: number;
  storeLongitude: number;
  adminshare?: string;
  storeRange: number;
  storeOrderPerTimeSlot: number;
  storeStartTime?: string;
  storeEndTime?: string;
  storeTimeSlotInterval: number;
  storeApporval: boolean;
  areaId?: string;
  areaName?: string;
  storeTypeId?: string;
  storeTypeName?: string;
  storeOwnerAadhar?: string;
  storeOwnerPAN?: string;
  storeDocslst: EditGetStoreDocumentsList[];
  storeImagelst: EditGetStoreImagesList[];
  storeUserlst: EditGetUserMasterDto[];
  rolesList: EditGetRoles[];
}

export interface EditGetUserMasterDto {
  tenantId?: string;
  userId?: string;
  userName?: string;
  userEmail?: string;
  userPass?: string;
  roleId?: string;
  storeId?: string;
}

export interface EditPostUserMasterDto {
  tenantId?: string;
  userId?: string;
  userName?: string;
  userEmail?: string;
  userPass?: string;
  roleId?: string;
  storeId?: string;
}

export interface EditStoreUploadMasterDto {
  storeOwnerName?: string;
  storeOwnerEmail?: string;
  storeOwnerPass?: string;
  tenantId?: string;
  storeId?: string;
  name?: string;
  storeNo?: string;
  storeAddress?: string;
  storeLattitude: number;
  storeLongitude: number;
  adminshare?: string;
  storeRange: number;
  storeOrderPerTimeSlot: number;
  storeStartTime?: string;
  storeEndTime?: string;
  storeTimeSlotInterval: number;
  storeApporval: boolean;
  areaId?: string;
  storeTypeId?: string;
  storeOwnerAadhar?: string;
  storeOwnerPAN?: string;
  identificationTypeId: string[];
  storeDocId: string[];
  identificationFile: IFormFile[];
  expiryDate: string[];
  storeImageFile: IFormFile[];
}

export interface GetStoreDocumentsList {
  identificationTypeId?: string;
  identificationTypeName?: string;
  identificationFileName?: string;
  expiryDate?: string;
}

export interface GetStoreImagesList {
  storeImageFileName?: string;
}

export interface StoreMasterDto extends FullAuditedEntityDto<string> {
  tenantId?: string;
  name?: string;
  storeNo?: string;
  storeAddress?: string;
  storeLattitude: number;
  storeLongitude: number;
  adminshare?: string;
  storeRange: number;
  storeOrderPerTimeSlot: number;
  storeStartTime?: string;
  storeEndTime?: string;
  storeTimeSlotInterval: number;
  storeApporval: boolean;
  areaId?: string;
  areaName?: string;
  storeTypeId?: string;
  storeTypeName?: string;
  storeOwnerAadhar?: string;
  storeOwnerPAN?: string;
  storeImagelst: GetStoreImagesList[];
  storeDocslst: GetStoreDocumentsList[];
}

export interface StoreUploadMasterDto {
  storeOwnerName?: string;
  storeOwnerEmail?: string;
  storeOwnerPass?: string;
  tenantId?: string;
  name?: string;
  storeNo?: string;
  storeAddress?: string;
  storeLattitude: number;
  storeLongitude: number;
  adminshare?: string;
  storeRange: number;
  storeOrderPerTimeSlot: number;
  storeStartTime?: string;
  storeEndTime?: string;
  storeTimeSlotInterval: number;
  storeApporval: boolean;
  areaId?: string;
  storeTypeId?: string;
  storeOwnerAadhar?: string;
  storeOwnerPAN?: string;
  storeId?: string;
  identificationTypeId: string[];
  identificationFile: IFormFile[];
  expiryDate: string[];
  storeImageFile: IFormFile[];
  userRoles: UserRoles[];
}

export interface StoresSelectList {
  storeId?: string;
  storeName?: string;
}

export interface UserMasterDto {
  tenantId?: string;
  userName?: string;
  userEmail?: string;
  userPass?: string;
  roleId?: string;
  storeId?: string;
}

export interface UserRoles {
  roleId?: string;
  roleName?: string;
}
