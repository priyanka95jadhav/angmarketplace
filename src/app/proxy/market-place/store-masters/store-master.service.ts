import type { CreateUpdateStoreMasterDto, EditGetStoreMasterDto, EditPostUserMasterDto, EditStoreUploadMasterDto, StoreMasterDto, StoreUploadMasterDto, StoresSelectList, UserMasterDto } from './dtos/models';
import type { StoresByLocationDto } from './models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';
import type { ActionResult } from '../../microsoft/asp-net-core/mvc/models';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class StoreMasterService {
  apiName = 'Default';

  allStores = () =>
    this.restService.request<any, StoresSelectList[]>({
      method: 'GET',
      url: '/api/app/store-master/all-stores',
    },
    { apiName: this.apiName });

  create = (input: CreateUpdateStoreMasterDto) =>
    this.restService.request<any, StoreMasterDto>({
      method: 'POST',
      url: '/api/app/store-master',
      body: input,
    },
    { apiName: this.apiName });

  createStoreWiseUserByUserMasterDto = (userMasterDto: String) =>
    this.restService.request<any, UserMasterDto[]>({
      method: 'POST',
      url: '/api/app/store-master/store-wise-user',
      body: userMasterDto,
      headers: new HttpHeaders().set('Content-Type', 'application/json').set('accept', 'text/plain'),
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/store-master/${id}`,
    },
    { apiName: this.apiName });

  editStoreDetailsByEditStoreMasterDto = (input: FormData) =>
    this.restService.request<any, EditStoreUploadMasterDto>({
      method: 'POST',
      url: '/api/app/store-master/edit-store-details',
      body:input
    },
    { apiName: this.apiName });

  editStoreWiseUserByUserMasterDto = (userMasterDto: String) =>
    this.restService.request<any, EditPostUserMasterDto[]>({
      method: 'POST',
      url: '/api/app/store-master/edit-store-wise-user',
      body: userMasterDto,
      headers: new HttpHeaders().set('Content-Type', 'application/json').set('accept', 'text/plain'),
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, StoreMasterDto>({
      method: 'GET',
      url: `/api/app/store-master/${id}`,
    },
    { apiName: this.apiName });

  getEditStoreByIdByStoreId = (storeId: string) =>
    this.restService.request<any, EditGetStoreMasterDto>({
      method: 'GET',
      url: `/api/app/store-master/edit-store-by-id/${storeId}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<StoreMasterDto>>({
      method: 'GET',
      url: '/api/app/store-master',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  getStoresByLocationByCoordinates = (coordinates: StoresByLocationDto) =>
    this.restService.request<any, ActionResult>({
      method: 'POST',
      url: '/api/app/store-master/get-stores-by-location',
      body: coordinates,
    },
    { apiName: this.apiName });

  update = (id: string, input: FormData) =>
    this.restService.request<any, StoreMasterDto>({
      method: 'PUT',
      url: `/api/app/store-master/${id}`,
      body: input,
      headers: new HttpHeaders().set('Content-Type', 'application/json').set('accept', 'text/plain'),
    },
    { apiName: this.apiName });

  uploadFileByStoreMasterDto = (input: FormData) =>
    this.restService.request<any, StoreUploadMasterDto>({
      method: 'POST',
      url: '/api/app/createstore',
      body:input
    },
    { apiName: this.apiName });
    
  constructor(private restService: RestService) {}
}
