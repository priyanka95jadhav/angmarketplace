import type { PagedAndSortedResultRequestDto } from '@abp/ng.core';

export interface StoresByLocationDto extends PagedAndSortedResultRequestDto {
  latitude?: string;
  longitude?: string;
}
