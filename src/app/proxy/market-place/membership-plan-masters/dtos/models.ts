import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateMembershipPlanDto {
  planName?: string;
  planDays: number;
  planPrice: number;
  rewardPoint: number;
  isFreeDelivery: boolean;
  isInstantDelivery: boolean;
  imageName?: string;
  description?: string;
}

export interface MembershipPlanDto extends FullAuditedEntityDto<string> {
  planName?: string;
  planDays: number;
  planPrice: number;
  rewardPoint: number;
  isFreeDelivery: boolean;
  isInstantDelivery: boolean;
  imageName?: string;
  description?: string;
}
