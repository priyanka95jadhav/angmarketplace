import type { CreateUpdateMembershipPlanDto, MembershipPlanDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class MembershipPlanService {
  apiName = 'Default';

  create = (input: CreateUpdateMembershipPlanDto) =>
    this.restService.request<any, MembershipPlanDto>({
      method: 'POST',
      url: '/api/app/membership-plan',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/membership-plan/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, MembershipPlanDto>({
      method: 'GET',
      url: `/api/app/membership-plan/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<MembershipPlanDto>>({
      method: 'GET',
      url: '/api/app/membership-plan',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateMembershipPlanDto) =>
    this.restService.request<any, MembershipPlanDto>({
      method: 'PUT',
      url: `/api/app/membership-plan/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
