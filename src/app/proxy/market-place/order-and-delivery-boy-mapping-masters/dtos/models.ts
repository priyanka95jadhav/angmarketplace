import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateOrderAndDeliveryBoyMappingDto {
  orderId?: string;
  deliveryBoyId?: string;
}

export interface OrderAndDeliveryBoyMappingDto extends FullAuditedEntityDto<string> {
  orderId?: string;
  deliveryBoyId?: string;
}
