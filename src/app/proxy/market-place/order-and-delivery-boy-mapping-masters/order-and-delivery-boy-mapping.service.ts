import type { CreateUpdateOrderAndDeliveryBoyMappingDto, OrderAndDeliveryBoyMappingDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class OrderAndDeliveryBoyMappingService {
  apiName = 'Default';

  create = (input: CreateUpdateOrderAndDeliveryBoyMappingDto) =>
    this.restService.request<any, OrderAndDeliveryBoyMappingDto>({
      method: 'POST',
      url: '/api/app/order-and-delivery-boy-mapping',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/order-and-delivery-boy-mapping/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, OrderAndDeliveryBoyMappingDto>({
      method: 'GET',
      url: `/api/app/order-and-delivery-boy-mapping/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<OrderAndDeliveryBoyMappingDto>>({
      method: 'GET',
      url: '/api/app/order-and-delivery-boy-mapping',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateOrderAndDeliveryBoyMappingDto) =>
    this.restService.request<any, OrderAndDeliveryBoyMappingDto>({
      method: 'PUT',
      url: `/api/app/order-and-delivery-boy-mapping/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
