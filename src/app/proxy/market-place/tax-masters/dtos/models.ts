import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateTaxDto {
  taxName?: string;
  taxPercentage: number;
}

export interface TaxDto extends FullAuditedEntityDto<string> {
  taxName?: string;
  taxPercentage: number;
}
