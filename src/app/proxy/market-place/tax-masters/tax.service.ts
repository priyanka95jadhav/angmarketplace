import type { CreateUpdateTaxDto, TaxDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class TaxService {
  apiName = 'Default';

  create = (input: CreateUpdateTaxDto) =>
    this.restService.request<any, TaxDto>({
      method: 'POST',
      url: '/api/app/tax',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/tax/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, TaxDto>({
      method: 'GET',
      url: `/api/app/tax/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<TaxDto>>({
      method: 'GET',
      url: '/api/app/tax',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateTaxDto) =>
    this.restService.request<any, TaxDto>({
      method: 'PUT',
      url: `/api/app/tax/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
