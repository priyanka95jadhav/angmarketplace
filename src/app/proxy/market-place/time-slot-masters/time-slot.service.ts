import type { CreateUpdateTimeSlotDto, TimeSlotDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';
import type { ActionResult } from '../../microsoft/asp-net-core/mvc/models';
import type { StoreAndTimeSlotMappingDto } from '../store-and-time-slot-mapping-masters/dtos/models';

@Injectable({
  providedIn: 'root',
})
export class TimeSlotService {
  apiName = 'Default';

  activeOrDeactiveTimeSlotByInput = (input: StoreAndTimeSlotMappingDto[]) =>
    this.restService.request<any, ActionResult>({
      method: 'POST',
      url: '/api/app/time-slot/active-or-deactive-time-slot',
      body: input,
    },
    { apiName: this.apiName });

  addStoreAndTimeSlotMappingByInput = (input: StoreAndTimeSlotMappingDto[]) =>
    this.restService.request<any, ActionResult>({
      method: 'POST',
      url: '/api/app/time-slot/store-and-time-slot-mapping',
      body: input,
    },
    { apiName: this.apiName });

  allTimeSlotList = () =>
    this.restService.request<any, ActionResult>({
      method: 'GET',
      url: '/api/app/time-slot/all-time-slot-list',
    },
    { apiName: this.apiName });

  create = (input: CreateUpdateTimeSlotDto) =>
    this.restService.request<any, TimeSlotDto>({
      method: 'POST',
      url: '/api/app/time-slot',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/time-slot/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, TimeSlotDto>({
      method: 'GET',
      url: `/api/app/time-slot/${id}`,
    },
    { apiName: this.apiName });

  getAvailableTimeSlotByStoreId = (storeId: string) =>
    this.restService.request<any, ActionResult>({
      method: 'GET',
      url: `/api/app/time-slot/available-time-slot/${storeId}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<TimeSlotDto>>({
      method: 'GET',
      url: '/api/app/time-slot',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  timeSlotListByStoreIdByStoreId = (storeId: string) =>
    this.restService.request<any, ActionResult>({
      method: 'GET',
      url: `/api/app/time-slot/time-slot-list-by-store-id/${storeId}`,
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateTimeSlotDto) =>
    this.restService.request<any, TimeSlotDto>({
      method: 'PUT',
      url: `/api/app/time-slot/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
