import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateTimeSlotDto {
  startTime?: string;
  endTime?: string;
  timeStart?: string;
  timeEnd?: string;
}

export interface TimeSlotDto extends FullAuditedEntityDto<string> {
  startTime?: string;
  endTime?: string;
  timeStart?: string;
  timeEnd?: string;
}
