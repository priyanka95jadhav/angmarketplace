import type { CancellationReasonDto, CreateUpdateCancellationReasonDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CancellationReasonService {
  apiName = 'Default';

  create = (input: CreateUpdateCancellationReasonDto) =>
    this.restService.request<any, CancellationReasonDto>({
      method: 'POST',
      url: '/api/app/cancellation-reason',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/cancellation-reason/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, CancellationReasonDto>({
      method: 'GET',
      url: `/api/app/cancellation-reason/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<CancellationReasonDto>>({
      method: 'GET',
      url: '/api/app/cancellation-reason',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateCancellationReasonDto) =>
    this.restService.request<any, CancellationReasonDto>({
      method: 'PUT',
      url: `/api/app/cancellation-reason/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
