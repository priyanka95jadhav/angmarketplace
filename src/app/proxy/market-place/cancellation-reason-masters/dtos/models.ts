import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CancellationReasonDto extends FullAuditedEntityDto<string> {
  reason?: string;
}

export interface CreateUpdateCancellationReasonDto {
  reason?: string;
}
