import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateTaxTypeDto {
  taxTypeName?: string;
}

export interface TaxTypeDto extends FullAuditedEntityDto<string> {
  taxTypeName?: string;
}
