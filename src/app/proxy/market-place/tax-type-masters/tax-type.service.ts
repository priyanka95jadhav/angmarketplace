import type { CreateUpdateTaxTypeDto, TaxTypeDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { ListResultDto, PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class TaxTypeService {
  apiName = 'Default';

  create = (input: CreateUpdateTaxTypeDto) =>
    this.restService.request<any, TaxTypeDto>({
      method: 'POST',
      url: '/api/app/tax-type',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/tax-type/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, TaxTypeDto>({
      method: 'GET',
      url: `/api/app/tax-type/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<TaxTypeDto>>({
      method: 'GET',
      url: '/api/app/tax-type',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  getTaxType = () =>
    this.restService.request<any, ListResultDto<TaxTypeDto>>({
      method: 'GET',
      url: '/api/app/tax-type/tax-type',
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateTaxTypeDto) =>
    this.restService.request<any, TaxTypeDto>({
      method: 'PUT',
      url: `/api/app/tax-type/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
