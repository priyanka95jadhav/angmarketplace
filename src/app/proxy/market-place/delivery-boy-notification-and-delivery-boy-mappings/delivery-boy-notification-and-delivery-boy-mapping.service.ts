import type { CreateUpdateDeliveryBoyNotificationAndDeliveryBoyMappingDto, DeliveryBoyNotificationAndDeliveryBoyMappingDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DeliveryBoyNotificationAndDeliveryBoyMappingService {
  apiName = 'Default';

  create = (input: CreateUpdateDeliveryBoyNotificationAndDeliveryBoyMappingDto) =>
    this.restService.request<any, DeliveryBoyNotificationAndDeliveryBoyMappingDto>({
      method: 'POST',
      url: '/api/app/delivery-boy-notification-and-delivery-boy-mapping',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/delivery-boy-notification-and-delivery-boy-mapping/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, DeliveryBoyNotificationAndDeliveryBoyMappingDto>({
      method: 'GET',
      url: `/api/app/delivery-boy-notification-and-delivery-boy-mapping/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<DeliveryBoyNotificationAndDeliveryBoyMappingDto>>({
      method: 'GET',
      url: '/api/app/delivery-boy-notification-and-delivery-boy-mapping',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateDeliveryBoyNotificationAndDeliveryBoyMappingDto) =>
    this.restService.request<any, DeliveryBoyNotificationAndDeliveryBoyMappingDto>({
      method: 'PUT',
      url: `/api/app/delivery-boy-notification-and-delivery-boy-mapping/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
