import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateDeliveryBoyNotificationAndDeliveryBoyMappingDto {
  deliveryBoyNotificationId?: string;
  deliveryBoyId?: string;
}

export interface DeliveryBoyNotificationAndDeliveryBoyMappingDto extends FullAuditedEntityDto<string> {
  deliveryBoyNotificationId?: string;
  deliveryBoyId?: string;
}
