import type { CreateUpdateDeviceMasterDto, DeviceMasterDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DeviceMasterService {
  apiName = 'Default';

  create = (input: CreateUpdateDeviceMasterDto) =>
    this.restService.request<any, DeviceMasterDto>({
      method: 'POST',
      url: '/api/app/device-master',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/device-master/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, DeviceMasterDto>({
      method: 'GET',
      url: `/api/app/device-master/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<DeviceMasterDto>>({
      method: 'GET',
      url: '/api/app/device-master',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateDeviceMasterDto) =>
    this.restService.request<any, DeviceMasterDto>({
      method: 'PUT',
      url: `/api/app/device-master/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
