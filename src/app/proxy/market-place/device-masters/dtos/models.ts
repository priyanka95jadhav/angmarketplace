import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateDeviceMasterDto {
  deviceId?: string;
  userId?: string;
}

export interface DeviceMasterDto extends FullAuditedEntityDto<string> {
  deviceId?: string;
  userId?: string;
}
