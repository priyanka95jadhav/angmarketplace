import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateIdentificationTypeDto {
  identificationTypeName?: string;
}

export interface IdentificationTypeDto extends FullAuditedEntityDto<string> {
  identificationTypeName?: string;
}
