import type { CreateUpdateIdentificationTypeDto, IdentificationTypeDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { ListResultDto, PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class IdentificationTypeService {
  apiName = 'Default';

  allIdentificationType = () =>
    this.restService.request<any, ListResultDto<IdentificationTypeDto>>({
      method: 'GET',
      url: '/api/app/identification-type/all-identification-type',
    },
    { apiName: this.apiName });

  create = (input: CreateUpdateIdentificationTypeDto) =>
    this.restService.request<any, IdentificationTypeDto>({
      method: 'POST',
      url: '/api/app/identification-type',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/identification-type/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, IdentificationTypeDto>({
      method: 'GET',
      url: `/api/app/identification-type/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<IdentificationTypeDto>>({
      method: 'GET',
      url: '/api/app/identification-type',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateIdentificationTypeDto) =>
    this.restService.request<any, IdentificationTypeDto>({
      method: 'PUT',
      url: `/api/app/identification-type/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
