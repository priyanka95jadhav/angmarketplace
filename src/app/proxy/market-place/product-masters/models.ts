import type { PagedAndSortedResultRequestDto } from '@abp/ng.core';

export interface ProductSearch {
  productId?: string;
  brand?: string;
  productName?: string;
  productMainImg?: string;
  productMainImgPath?: string;
  productMRP: number;
  productPrice: number;
  rating: number;
}

export interface ProductSearchResult extends PagedAndSortedResultRequestDto {
  sreachText?: string;
}
