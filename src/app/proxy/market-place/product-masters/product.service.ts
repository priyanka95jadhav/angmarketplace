import type { CreateUpdateProductDto, GetEditProductUploadDto, ProductDto, ProductFilterByCatagory, ProductFilterByTags, ProductList, ProductUploadDto, UpdateProductUploadDto } from './dtos/models';
import type { ProductSearch, ProductSearchResult } from './models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';
import type { ActionResult } from '../../microsoft/asp-net-core/mvc/models';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  apiName = 'Default';

  allProducts = () =>
  this.restService.request<any, ProductList[]>({
    method: 'GET',
    url: '/api/app/product/all-products',
  },
  { apiName: this.apiName });

create = (input: CreateUpdateProductDto) =>
  this.restService.request<any, ProductDto>({
    method: 'POST',
    url: '/api/app/product',
    body: input,
  },
  { apiName: this.apiName });

delete = (id: string) =>
  this.restService.request<any, void>({
    method: 'DELETE',
    url: `/api/app/product/${id}`,
  },
  { apiName: this.apiName });

get = (id: string) =>
  this.restService.request<any, ProductDto>({
    method: 'GET',
    url: `/api/app/product/${id}`,
  },
  { apiName: this.apiName });

getEditProductByIdByProductId = (productId: string) =>
  this.restService.request<any, GetEditProductUploadDto>({
    method: 'GET',
    url: `/api/app/product/edit-product-by-id/${productId}`,
  },
  { apiName: this.apiName });

getList = (input: PagedAndSortedResultRequestDto) =>
  this.restService.request<any, PagedResultDto<ProductDto>>({
    method: 'GET',
    url: '/api/app/product',
    params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
  },
  { apiName: this.apiName });

getProductLstByCategoryIdByFilter = (filter: ProductFilterByCatagory) =>
  this.restService.request<any, PagedResultDto<GetEditProductUploadDto>>({
    method: 'GET',
    url: '/api/app/product/product-lst-by-category-id',
    params: { categoryId: filter.categoryId, sorting: filter.sorting, skipCount: filter.skipCount, maxResultCount: filter.maxResultCount },
  },
  { apiName: this.apiName });

getProductLstByTagsByFilter = (filter: ProductFilterByTags) =>
  this.restService.request<any, PagedResultDto<GetEditProductUploadDto>>({
    method: 'GET',
    url: '/api/app/product/product-lst-by-tags',
    params: { tagsName: filter.tagsName, sorting: filter.sorting, skipCount: filter.skipCount, maxResultCount: filter.maxResultCount },
  },
  { apiName: this.apiName });

insertProductByInput = (input: FormData) =>
  this.restService.request<any, ProductUploadDto>({
    method: 'POST',
    url: '/api/app/product/createproduct',
    body:input,
    // headers: new HttpHeaders().set('Content-Type', 'multipart/form-data'),
  },
  { apiName: this.apiName });

update = (id: string, input: CreateUpdateProductDto) =>
  this.restService.request<any, ProductDto>({
    method: 'PUT',
    url: `/api/app/product/${id}`,
    body: input,
  },
  { apiName: this.apiName });

updateProductDetailsByIdAndEditProductUploadDto = (Id: string, editProductUploadDto: FormData) =>
  this.restService.request<any, UpdateProductUploadDto>({
    method: 'PUT',
    url: `/api/app/product/product-details/${Id}`,
    body:editProductUploadDto
  },
  { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
