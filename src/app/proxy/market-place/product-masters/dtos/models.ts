import type { IFormFile } from '../../../microsoft/asp-net-core/http/models';
import type { FullAuditedEntityDto, PagedAndSortedResultRequestDto } from '@abp/ng.core';

export interface CreateUpdateProductDto {
  productName?: string;
  brandName?: string;
  countryOfOrigin?: string;
  productDisclaimer?: string;
  productTypeId?: string;
  storeId?: string;
}

export interface GetEditProductAndProductAttribute {
  id?: string;
  productId?: string;
  productAttributeId?: string;
  productSubAttributeId?: string;
  productStatusId?: string;
  productAttributeName?: string;
  productAttributeValue?: string;
  productDescription?: string;
  productMainImgFile: IFormFile;
  productMainImgFileName?: string;
  productMainImgFilePath?: string;
  productSKU?: string;
  productPrice: number;
  productMRP: number;
  productMaxQty: number;
  productDiscount: number;
  isApproved: boolean;
  isActive: boolean;
  productSideImages: GetEditProductImg[];
}

export interface GetEditProductImg {
  id?: string;
  imageName?: string;
  images: IFormFile;
  imagePath?: string;
}

export interface GetEditProductUploadDto {
  id?: string;
  productName?: string;
  brandName?: string;
  countryOfOrigin?: string;
  productDisclaimer?: string;
  productTypeId?: string;
  productTypeName?: string;
  storeId?: string;
  storeName?: string;
  categoryId?: string;
  subCategoryId?: string;
  subSubCategoryId?: string;
  productAndProductAttributes: GetEditProductAndProductAttribute[];
  productTags: string[];
}

export interface ProductAndProductAttribute {
  productId?: string;
  productAttributeId?: string;
  productSubAttributeId?: string;
  productStatusId?: string;
  productAttributeName?: string;
  productAttributeValue?: string;
  productDescription?: string;
  productMainImgFile: IFormFile;
  productMainImgFilePath?: string;
  productSKU?: string;
  productPrice: number;
  productMRP: number;
  productMaxQty: number;
  productDiscount: number;
  isApproved: boolean;
  isActive: boolean;
  productSideImages: ProductImg[];
}

export interface ProductDto extends FullAuditedEntityDto<string> {
  productName?: string;
  brandName?: string;
  countryOfOrigin?: string;
  productDisclaimer?: string;
  productTypeId?: string;
  storeId?: string;
  productTypeName?: string;
  storeName?: string;
}

export interface ProductFilterByCatagory extends PagedAndSortedResultRequestDto {
  categoryId?: string;
}

export interface ProductFilterByTags extends PagedAndSortedResultRequestDto {
  tagsName?: string;
}

export interface ProductImg {
  iamgeName?: string;
  images: IFormFile;
}

export interface ProductList {
  productId?: string;
  productName?: string;
}

export interface ProductUploadDto {
  productName?: string;
  brandName?: string;
  countryOfOrigin?: string;
  productDisclaimer?: string;
  productTypeId?: string;
  storeId?: string;
  categoryId?: string;
  subCategoryId?: string;
  subSubCategoryId?: string;
  productAndProductAttributes: ProductAndProductAttribute[];
  productTags: string[];
}

export interface UpdateProductAndProductAttribute {
  id?: string;
  productId?: string;
  productAttributeId?: string;
  productSubAttributeId?: string;
  productStatusId?: string;
  productAttributeName?: string;
  productAttributeValue?: string;
  productDescription?: string;
  productMainImgFile: IFormFile;
  productMainImg?: string;
  productMainImgPath?: string;
  productSKU?: string;
  productPrice: number;
  productMRP: number;
  productMaxQty: number;
  productDiscount: number;
  isApproved: boolean;
  isActive: boolean;
  productSideImages: UpdateProductImg[];
}

export interface UpdateProductImg {
  id?: string;
  imageName?: string;
  images: IFormFile;
  imagePath?: string;
}

export interface UpdateProductUploadDto {
  productId?: string;
  productName?: string;
  brandName?: string;
  countryOfOrigin?: string;
  productDisclaimer?: string;
  productTypeId?: string;
  productTypeName?: string;
  storeId?: string;
  storeName?: string;
  categoryId?: string;
  subCategoryId?: string;
  subSubCategoryId?: string;
  productAndProductAttributes: UpdateProductAndProductAttribute[];
  productTags: string[];
}
