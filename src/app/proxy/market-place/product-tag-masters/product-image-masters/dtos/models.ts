import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateProductImageDto {
  productAttributeMappingId?: string;
  imageName?: string;
  imagePath?: string;
}

export interface ProductImageDto extends FullAuditedEntityDto<string> {
  productAttributeMappingId?: string;
  imageName?: string;
  imagePath?: string;
}
