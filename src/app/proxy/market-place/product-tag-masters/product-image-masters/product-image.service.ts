import type { CreateUpdateProductImageDto, ProductImageDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ProductImageService {
  apiName = 'Default';

  create = (input: CreateUpdateProductImageDto) =>
    this.restService.request<any, ProductImageDto>({
      method: 'POST',
      url: '/api/app/product-image',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/product-image/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, ProductImageDto>({
      method: 'GET',
      url: `/api/app/product-image/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<ProductImageDto>>({
      method: 'GET',
      url: '/api/app/product-image',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateProductImageDto) =>
    this.restService.request<any, ProductImageDto>({
      method: 'PUT',
      url: `/api/app/product-image/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
