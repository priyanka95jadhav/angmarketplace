import type { CreateUpdateProductTagDto, ProductTagDto, ProductTagList } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ProductTagService {
  apiName = 'Default';

  allProductTag = () =>
    this.restService.request<any, ProductTagList[]>({
      method: 'GET',
      url: '/api/app/product-tag/all-product-tag',
    },
    { apiName: this.apiName });

  create = (input: CreateUpdateProductTagDto) =>
    this.restService.request<any, ProductTagDto>({
      method: 'POST',
      url: '/api/app/product-tag',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/product-tag/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, ProductTagDto>({
      method: 'GET',
      url: `/api/app/product-tag/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<ProductTagDto>>({
      method: 'GET',
      url: '/api/app/product-tag',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  getSimilarTagsByTag = (tag: string) =>
    this.restService.request<any, string[]>({
      method: 'GET',
      url: '/api/app/product-tag/similar-tags',
      params: { tag },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateProductTagDto) =>
    this.restService.request<any, ProductTagDto>({
      method: 'PUT',
      url: `/api/app/product-tag/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
