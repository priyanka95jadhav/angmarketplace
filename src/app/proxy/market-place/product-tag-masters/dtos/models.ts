import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateProductTagDto {
  productTagName?: string;
  productId?: string;
}

export interface ProductTagDto extends FullAuditedEntityDto<string> {
  productTagName?: string;
  productId?: string;
}

export interface ProductTagList {
  productTagId?: string;
  productTagName?: string;
}
