import type { CreateUpdateStoreAndTimeSlotMappingDto, StoreAndTimeSlotMappingDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class StoreAndTimeSlotMappingService {
  apiName = 'Default';

  create = (input: CreateUpdateStoreAndTimeSlotMappingDto) =>
    this.restService.request<any, StoreAndTimeSlotMappingDto>({
      method: 'POST',
      url: '/api/app/store-and-time-slot-mapping',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/store-and-time-slot-mapping/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, StoreAndTimeSlotMappingDto>({
      method: 'GET',
      url: `/api/app/store-and-time-slot-mapping/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<StoreAndTimeSlotMappingDto>>({
      method: 'GET',
      url: '/api/app/store-and-time-slot-mapping',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateStoreAndTimeSlotMappingDto) =>
    this.restService.request<any, StoreAndTimeSlotMappingDto>({
      method: 'PUT',
      url: `/api/app/store-and-time-slot-mapping/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
