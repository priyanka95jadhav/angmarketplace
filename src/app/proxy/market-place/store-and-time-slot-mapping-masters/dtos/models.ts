import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateStoreAndTimeSlotMappingDto {
  storeId?: string;
  timeSlot?: string;
  isActive: boolean;
}

export interface StoreAndTimeSlotMappingDto extends FullAuditedEntityDto<string> {
  storeId?: string;
  timeSlot?: string;
  isActive: boolean;
}
