import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateOrderStatusDto {
  orderStatusName?: string;
}

export interface OrderStatusDto extends FullAuditedEntityDto<string> {
  orderStatusName?: string;
}

export interface OrderStatusList {
  orderStatusId?: string;
  orderStatusName?: string;
}
