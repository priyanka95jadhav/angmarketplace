import type { CreateUpdateOrderStatusDto, OrderStatusDto, OrderStatusList } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class OrderStatusService {
  apiName = 'Default';

  allOrderStatus = () =>
    this.restService.request<any, OrderStatusList[]>({
      method: 'GET',
      url: '/api/app/order-status/all-order-status',
    },
    { apiName: this.apiName });

  create = (input: CreateUpdateOrderStatusDto) =>
    this.restService.request<any, OrderStatusDto>({
      method: 'POST',
      url: '/api/app/order-status',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/order-status/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, OrderStatusDto>({
      method: 'GET',
      url: `/api/app/order-status/${id}`,
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<OrderStatusDto>>({
      method: 'GET',
      url: '/api/app/order-status',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateOrderStatusDto) =>
    this.restService.request<any, OrderStatusDto>({
      method: 'PUT',
      url: `/api/app/order-status/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
