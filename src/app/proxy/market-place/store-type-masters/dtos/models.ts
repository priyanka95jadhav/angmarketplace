import type { FullAuditedEntityDto } from '@abp/ng.core';

export interface CreateUpdateStoreTypeDto {
  storeTypeName?: string;
}

export interface StoreTypeDto extends FullAuditedEntityDto<string> {
  storeTypeName?: string;
}
