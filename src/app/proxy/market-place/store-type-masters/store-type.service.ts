import type { CreateUpdateStoreTypeDto, StoreTypeDto } from './dtos/models';
import { RestService } from '@abp/ng.core';
import type { ListResultDto, PagedAndSortedResultRequestDto, PagedResultDto } from '@abp/ng.core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class StoreTypeService {
  apiName = 'Default';

  create = (input: CreateUpdateStoreTypeDto) =>
    this.restService.request<any, StoreTypeDto>({
      method: 'POST',
      url: '/api/app/store-type',
      body: input,
    },
    { apiName: this.apiName });

  delete = (id: string) =>
    this.restService.request<any, void>({
      method: 'DELETE',
      url: `/api/app/store-type/${id}`,
    },
    { apiName: this.apiName });

  get = (id: string) =>
    this.restService.request<any, StoreTypeDto>({
      method: 'GET',
      url: `/api/app/store-type/${id}`,
    },
    { apiName: this.apiName });

  getAllStoreType = () =>
    this.restService.request<any, ListResultDto<StoreTypeDto>>({
      method: 'GET',
      url: '/api/app/storetype/AllStoretype',
    },
    { apiName: this.apiName });

  getList = (input: PagedAndSortedResultRequestDto) =>
    this.restService.request<any, PagedResultDto<StoreTypeDto>>({
      method: 'GET',
      url: '/api/app/store-type',
      params: { skipCount: input.skipCount, maxResultCount: input.maxResultCount, sorting: input.sorting },
    },
    { apiName: this.apiName });

  update = (id: string, input: CreateUpdateStoreTypeDto) =>
    this.restService.request<any, StoreTypeDto>({
      method: 'PUT',
      url: `/api/app/store-type/${id}`,
      body: input,
    },
    { apiName: this.apiName });

  constructor(private restService: RestService) {}
}
