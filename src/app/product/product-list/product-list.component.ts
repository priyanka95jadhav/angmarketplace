import { Component, OnInit } from '@angular/core';
import { ListService, PagedResultDto } from '@abp/ng.core';
import { ProductService } from '@proxy/market-place/product-masters';
import { ProductUploadDto } from '@proxy/market-place/product-masters/dtos';
import {Confirmation, ConfirmationService } from '@abp/ng.theme.shared';
import { NotificationService } from 'src/app/shared/notification.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
  providers: [ListService],
})
export class ProductListComponent implements OnInit {

  productlist = { items: [], totalCount: 0 } as PagedResultDto<ProductUploadDto>;
  products: any;

  constructor(public readonly list: ListService , private productService:ProductService, private confirmation: ConfirmationService,private notifyService: NotificationService, ) { }

  ngOnInit(): void {
    this.getProducts()
    
  }

  getProducts(){
    const productStreamCreator = (query) => this.productService.getList(query);

    this.list.hookToQuery(productStreamCreator).subscribe((response) => {
      this.products = response;
      // console.log(response);
    });
  }


  deleteProduct(id:any){
    this.confirmation.warn('::Are You Sure To Delete', '::AreYouSure').subscribe((status) => {
      if (status === Confirmation.Status.confirm) {
        this.productService.delete(id).subscribe(() => this.list.get());
        this.notifyService.showSuccess('Product deleted Successfully', '');
      }
    });
  }


}
