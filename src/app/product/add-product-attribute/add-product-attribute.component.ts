import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductAttributeService } from '@proxy/market-place/product-attribute-masters';
import { NotificationService } from 'src/app/shared/notification.service';

@Component({
  selector: 'app-add-product-attribute',
  templateUrl: './add-product-attribute.component.html',
  styleUrls: ['./add-product-attribute.component.scss']
})
export class AddProductAttributeComponent implements OnInit {

  productattributeForm!: FormGroup;
  submitted = false;
  allProductAttribute: any;
  selectedFiles: any;
  formFile: File;
  id: string;
  isAddMode: boolean;
  constructor(
    private formBuilder: FormBuilder,
    private notifyService: NotificationService,
    private productattributeService: ProductAttributeService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.isAddMode = !this.id;
    this. productattributeForm = this.formBuilder.group({
      attributeName: ['', Validators.required],
      parentAttributeId: new FormControl(''),
      
    });

    this.getAllProductAttribute();

    if (!this.isAddMode) {
      this.productattributeService.get(this.id)
          .pipe()
          .subscribe(x => this. productattributeForm.patchValue(x)
          );
          
  }
  }
  get productattributeval() {
    return this. productattributeForm.controls;
  }

  getAllProductAttribute() {
    this.productattributeService.getCategoryHierarchyList().subscribe((data: any) => {
      this.allProductAttribute = data;
      console.log(this.allProductAttribute);
    });
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this. productattributeForm.invalid) {
      return;
    }

    if (this.isAddMode) {
      this.saveCategory();
    } else {
     this.updateProductAttribute();
    }
  }

  private saveCategory() {
    this.submitted = true;
    if (this. productattributeForm.invalid) {
      return;
    }

    this.productattributeService
      .create(this. productattributeForm.value)
      .pipe()
      .subscribe({
        next: e => {
          this.notifyService.showSuccess('Product Attribute added Successfully.', '');
          // this.router.navigate(['/product-attribute'], { relativeTo: this.route });

          this.submitted = false;
          this. productattributeForm.reset();
          this.getAllProductAttribute();
        },
        error: error => {
          this.notifyService.showError('Your request is not valid.', '');
          console.log(error.message, 'error');
        },
      });
  }

  private updateProductAttribute() {
  
    this.productattributeService
      .update(this.id, this.productattributeForm.value)
      .pipe()
      .subscribe({
        next: () => {
          this.notifyService.showSuccess('Product Attribute Updated Successfully.', '');
          // this.getAllCategory()
          this.router.navigate(['/product-attribute'], { relativeTo: this.route });
        },
        error: error => {
          this.notifyService.showError(error, '');
          this.submitted = false;
        },
      });
  }
}
