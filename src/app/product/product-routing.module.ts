import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddProductAttributeComponent } from './add-product-attribute/add-product-attribute.component';
import { AddProductComponent } from './add-product/add-product.component';
import { ProductAttributeComponent } from './product-attribute/product-attribute.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductComponent } from './product.component';

const routes: Routes = [
  { path: '', component: ProductComponent },
  { path: 'product-list', component: ProductListComponent},
  { path: 'product/add', component: AddProductComponent},
  { path: 'product/:id', component: AddProductComponent},
  { path: 'product-attribute', component: ProductAttributeComponent },
  { path: 'product-attribute/add', component: AddProductAttributeComponent },
  { path: 'product-attribute/:id', component: AddProductAttributeComponent }
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule { }
