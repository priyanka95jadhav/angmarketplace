import { Component, OnInit } from '@angular/core';
import { ListService, PagedResultDto } from '@abp/ng.core';
import { NotificationService } from 'src/app/shared/notification.service';
import { ProductAttributeService } from '@proxy/market-place/product-attribute-masters';
import { ProductAttributeDto } from '@proxy/market-place/product-attribute-masters/dtos';


@Component({
  selector: 'app-product-attribute',
  templateUrl: './product-attribute.component.html',
  styleUrls: ['./product-attribute.component.scss'],
  providers: [ListService],
})
export class ProductAttributeComponent implements OnInit {

  productattributelist = { items: [], totalCount: 0 } as PagedResultDto<ProductAttributeDto>;
  allProductAttribute: any;

  constructor(public readonly list: ListService, private productattributeService: ProductAttributeService,private notifyService:NotificationService ) { }

  ngOnInit(): void {
    // get Category
    this.getProductAttribute()
  }

  getProductAttribute(){
    const getAllProductAttribute= (query) => this.productattributeService.getList(query);

    this.list.hookToQuery(getAllProductAttribute).subscribe((response) => {
      this.allProductAttribute = response;
      console.log(response);
    });
  }
// Delete Category
  deleteProductAttribute(id:any){
    if(confirm("Are you sure to delete ")) {
    this.productattributeService.delete(id).subscribe(() => {
      this.notifyService.showSuccess('Product Attribute Deleted Successfully.', '');
      this.getProductAttribute()
    });
  }
  }

  // editProductAttribute(id: any){
  //   this.productattributeService.update(id).subscribe(() => {
  //     this.notifyService.showSuccess('Product AttributeUpdated Successfully.', '');
  //   });
  // }

}
