import { ListService } from '@abp/ng.core';
import { Confirmation, ConfirmationService } from '@abp/ng.theme.shared';
import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryAttributeService } from '@proxy/market-place/category-attribute-masters';
import { ProductAttributeService } from '@proxy/market-place/product-attribute-masters';
import { ProductService } from '@proxy/market-place/product-masters';
import { ProductStatusService } from '@proxy/market-place/product-status-masters';
import { ProductTypeService } from '@proxy/market-place/product-type-masters';
import { StoreMasterService } from '@proxy/market-place/store-masters';
import { NotificationService } from 'src/app/shared/notification.service';
import { ProductImageService } from '@proxy/market-place/product-tag-masters/product-image-masters';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss'],
  providers: [ListService],
})
export class AddProductComponent implements OnInit {
  productAddForm: FormGroup;
  productAttributeForm: FormGroup;

  submitted = false;
  producttype: any;
  store: any;
  productattribute: any;
  categorytype: any;
  SelCategorytype: any;
  subsubCategoryList: any;
  subcategoryList: any;
  pAttributeId: any;
  productattributeList: any;
  productstatusList: any;
  forsuCateventVal: any;
  forsubsubCateventVal: any;
  imageSrc: string[] = [];
  images: File[][]=[];
  mainImg: File[] = [];
  subsubCategoryId: string;
  id: string;
  isAddMode: boolean;
  product_tags: any;
  SubAtt: any = [];
  AttVal: any = [];
  AttributeName:any = [];
  mrp: any = [];
  discount: any = [];
  TotalDiscountPrice: any = [];
  apiURL = 'https://marketplace.projectnimbus.co.in/ProductImage/ProductSideImg';
  MainImgURL = 'https://marketplace.projectnimbus.co.in/ProductImage/ProductMainImg';
  servermainImg: any= [];
  serverImgs: any;
  productsubattributeList: any = [];
  editData: any = null;
  prAttx: any[] = [];
  imagesFile: any = [];
  constructor(
    private fb: FormBuilder,
    private productService: ProductService,
    private producttypeService: ProductTypeService,
    private storeMasterService: StoreMasterService,
    private productattributeService: ProductAttributeService,
    private notifyService: NotificationService,
    private categoryService: CategoryAttributeService,
    private productStatus: ProductStatusService,
    private route: ActivatedRoute,
    private router: Router,
    private confirmation: ConfirmationService,
    private ProductImageService: ProductImageService
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.isAddMode = !this.id;
    this.productAddForm = this.fb.group({
      productName: ['', Validators.required],
      brandName: ['', Validators.required],
      countryOfOrigin: ['', Validators.required],
      productDisclaimer: ['', Validators.required],
      productTypeId: [''],
      storeId: [''],
      categoryId: [''],
      subCategoryId: [''],
      subSubCategoryId: [''],
      ProductTags: new FormArray([
        new FormGroup({
          productTagslist: new FormControl(''),
        }),
      ]),

      ProductAndProductAttributes: new FormArray([
        new FormGroup({
          productId: new FormControl(''),
          id: new FormControl(''),
          productAttributeId: new FormControl('', [Validators.required]),
          productSubAttributeId: new FormControl('', [Validators.required]),
          productStatusId: new FormControl('', [Validators.required]),
          productAttributeName: new FormControl('', [Validators.required]),
          productAttributeValue: new FormControl('', [Validators.required]),
          productDescription: new FormControl('', [Validators.required]),
          productMainImgFile: new FormControl('', [Validators.required]),
          productSKU: new FormControl('', [Validators.required]),
          productPrice: new FormControl('', [Validators.required]),
          productMRP: new FormControl('', [Validators.required]),
          productMaxQty: new FormControl('', [Validators.required]),
          productDiscount: new FormControl('', [Validators.required]),
          isApproved: new FormControl('', [Validators.required]),
          isActive: new FormControl('', [Validators.required]),
          ProductSideImages: this.fb.array([]),
        }),
      ]),
    });
    this.getProductType();
    this.getStore();
    this.getProductAttribute();
    this.getCategory();
    this.getProductStatus();
    if (!this.isAddMode) {
      this.productService
        .getEditProductByIdByProductId(this.id)
        .pipe()
        .subscribe((x: any) => {
          // console.log(x,"product");

          this.editData = x;

          for (let index = 0; index < x.productAndProductAttributes.length; index++) {
            const element = x.productAndProductAttributes[index].productSideImages;
            this.serverImgs = element;
            
          }
          // console.log(x.productAndProductAttributes,"serverImgs");

          for (let index = 0; index < x.productAndProductAttributes.length; index++) {
            const element = x.productAndProductAttributes[index].productMainImgFileName;
            const sImg = this.servermainImg.push(element);
          }
// console.log(x,"ppp");productPrice

          this.productAddForm.patchValue(x);   
          this.featchSubCategory(x.categoryId);
          this.featchSubSubCategory(x.subCategoryId);
          // this.calculateDiscount()
          for (let index = 0; index < x.productAndProductAttributes.length; index++) {
            if (index > 0) {
              this.addproduct();
            }
          }
          this.productAddForm.controls['ProductAndProductAttributes'].patchValue(
            this.prAttx = x.productAndProductAttributes
          );
          for (let index = 0; index < this.prAttx.length; index++) {
            const element = this.prAttx[index].productAttributeId;
            this.fetchProductAttr(element,index);
          }
          for (let index = 0; index < this.prAttx.length; index++) {
            const disc = this.prAttx[index].productPrice;
            this.calculateDiscount(index,disc)
            // console.log(index,disc);
          }
          for (let index = 0; index < x.productTags.length; index++) {
            const element = x.productTags;
            this.product_tags = Array(element.join(' '));
            this.productAddForm.controls['ProductTags'].patchValue(this.product_tags);
          }
        });
    }
  }

  addproduct() {
    const control = <FormArray>this.productAddForm.controls['ProductAndProductAttributes'];
    control.push(
      new FormGroup({
        productId: new FormControl(''),
        id: new FormControl('00000000-0000-0000-0000-000000000000', []),
        productAttributeId: new FormControl('', [Validators.required]),
        productSubAttributeId: new FormControl('', [Validators.required]),
        productStatusId: new FormControl('', [Validators.required]),
        productAttributeName: new FormControl('', [Validators.required]),
        productAttributeValue: new FormControl('', [Validators.required]),
        productDescription: new FormControl('', [Validators.required]),
        productMainImgFile: new FormControl('', [Validators.required]),
        productSKU: new FormControl('', [Validators.required]),
        productPrice: new FormControl('', [Validators.required]),
        productMRP: new FormControl('', [Validators.required]),
        productMaxQty: new FormControl('', [Validators.required]),
        productDiscount: new FormControl('', [Validators.required]),
        isApproved: new FormControl('', [Validators.required]),
        isActive: new FormControl('', [Validators.required]),
        ProductSideImages: this.fb.array([]),
      })
    );
  }

  sideimg(): FormArray {
    return this.productAddForm.get('ProductAndProductAttributes') as FormArray;
  }

  productSideImgs(index: number): FormArray {
    return this.sideimg().at(index).get('ProductSideImages') as FormArray;
  }

  addImges(): FormGroup {
    return this.fb.group({
      Images: '',
    });
  }

  addsideImg(index: number) {
    if (this.productSideImgs(index).length < 4) {
      this.productSideImgs(index).push(this.addImges());
      if(this.images.length < index+1){
        this.images.push([]);
      }
      
    }
  }

  removesideImage(index: number, sideimgIndex: number) {
    if (this.productSideImgs(index).length > 0) {
      this.productSideImgs(index).removeAt(sideimgIndex);
      this.images.splice(index,sideimgIndex)
    }
  }

  removeproduct(index) {
    const control = <FormArray>this.productAddForm.controls['ProductAndProductAttributes'];
    if (control.length > 1) {
      control.removeAt(index);
    }
  }

  addNewTags() {
    const add = this.productAddForm.get('ProductTags') as FormArray;
    add.push(
      new FormGroup({
        productTagslist: new FormControl(''),
      })
    );
  }

  deleteTags(index: number) {
    const remove = <FormArray>this.productAddForm.controls['ProductTags'];
    if (remove.length > 1) {
      remove.removeAt(index);
    }
  }

  removeSelectedFile(index) {
    console.log(index,"img");
    this.confirmation.warn('::Are You Sure To Delete Image', '::AreYouSure').subscribe(status => {
      if (status === Confirmation.Status.confirm) {
        this.ProductImageService.delete(index).subscribe(() => {
          this.serverImgs.splice(index, 1);
          this.notifyService.showSuccess(' Deleted Successfully.', '');
        });
      }
    });
    this.imageSrc.splice(index, 1);
    this.images.splice(index, 1);
  }

  get p(): { [key: string]: AbstractControl } {
    return this.productAddForm.controls;
  }

  // get f(): { [key: string]: AbstractControl } {
  //   return this.productAddForm.controls;
  // }

  getProductType() {
    this.producttypeService.allProductType().subscribe((data: any) => {
      data.forEach(element => {
        this.producttype = data;
        // console.log(this.producttype,"000000000")
      });
    });
  }

  getCategory() {
    this.categoryService.getCategoryHierarchyList().subscribe((data: any) => {
      data.forEach(element => {
        this.categorytype = data;
        // console.log(this.categorytype, 'category');
        // this.productAddForm.controls['categoryId'].patchValue(this.editData.categoryId);
      });
    });
  }

  forsubCategory(e: any) {
    this.featchSubCategory(e.target?.value);
  }

  featchSubCategory(id: string) {
    this.categoryService.getCategoryList(id).subscribe((data: any) => {
      // console.log(e.target.value,"e",this.SelCategorytype,"category type")
      this.subcategoryList = data.items;
    });
  }

  featchSubSubCategory(id: string) {
    this.categoryService.getCategoryList(id).subscribe((data: any) => {
      // console.log(e.target.value,"e",this.SelCategorytype,"category type")
      this.subsubCategoryList = data.items;
    });
  }

  forsubsubCategory(e: any) {
    this.featchSubSubCategory(e.target.value);
  }

  fetchProductAttr(id: string, index) {
    this.productattributeService.getCategoryList(id).subscribe((data: any) => {
      this.productsubattributeList[index] = data['items'];
      // console.log(index);
    });
  }

  changeProductAttribute(e: any, index) {
    this.fetchProductAttr(e.target.value, index);
  }

  getProductAttribute() {
    this.productattributeService.getCategoryHierarchyList().subscribe((data: any) => {
      this.productattribute = data;
      // console.log(this.productattribute,"==== productattribute")
    });
  }

  getProductStatus() {
    this.productStatus.allProductStatus().subscribe((data: any) => {
      this.productstatusList = data;
      // console.log(this.productstatusList,"==== productstatusList")
    });
  }

  onKeySubAttribute(e,i) {
    this.SubAtt[i] = e.target.options[e.target.options.selectedIndex].text;
    // console.log(this.SubAtt, 'SubAttribute');
  }

  onKeySAttrValue(e,i) {
    // console.log(e.target.value, 'onKeySAttrValue');

    this.AttVal[i] = e.target.value;
  }

  attrValueChange(e,i) {
    // console.log(e,"eee");
    this.AttVal[i] = e
    // if(this.SubAtt && this.AttVal){
    this.AttributeName[i] = this.AttVal[i] + ' ' + this.SubAtt[i];
    // console.log(this.AttributeName);
    // }
  }

  onKeyMRP(e,i) {
    this.mrp[i] = parseInt(e.target.value);
    // console.log(e.target.value);
  }

  onKeyPrice(e,i) {
    this.discount[i] = e.target.value;
  }

  fetchDiscount(e: any, i) {
    // console.log(e,"eee");
    this.discount[i] = e
    if (this.mrp[i] < this.discount[i]) {
      // alert('MRP amount should be greater than Price');
      // return;
    }
   else {
      this.TotalDiscountPrice[i] = this.mrp[i] - (parseInt(this.mrp[i]) * parseInt(this.discount[i])) / 100 ;
      // discounted_price = original_price - (original_price * discount / 100)
    }

    // console.log(this.TotalDiscountPrice[i],"TotalDiscountPrice");
  }

  calculateDiscount(e,i) {
    this.fetchDiscount(e,i)
  }

  getStore() {
    this.storeMasterService.allStores().subscribe((data: any) => {
      data.forEach(element => {
        this.store = data;
      });
    });
  }

  uploadImages(event, i,j) {
    // console.log(i,j,event,"i,j");
    // this.images.push([i, "value2"]);

    var filename = event.target.files[0].name;


//var items = [ [1], [2] ];
// var add = [ 5 ];
// items.splice(0, 1, add);


// console.log(items);


    // this.images = [];
    // this.imagesFile.push(filename);
    this.images[i].push(event.target.files[0]);
    // this.images.push(this.imagesFile);  
    // console.log("sfsdf",this.imagesFile); 
    console.log("sdaasd",this.images);
   
  }

  mainImage(event, i) {
    const file: File = event.target.files[0];
    if (file) {
      this.mainImg[i] = file;
      console.log(this.mainImg[i]);
    }
  }

  getFormData(object) {
    const formData = new FormData();
    Object.keys(object).forEach(key => formData.append(key, object[key]));
    return formData;
  }

  onProductSubmit() {
    this.submitted = true;
    if (!this.isAddMode) {

    // let productAttributs = this.productAddForm.controls['productAddForm'];
    // productAttributs.get('productMainImgFile').clearValidators();
    // productAttributs.get('productMainImgFile').setErrors({ 'required': null, 'emptyLines': null });
    // productAttributs.get('productMainImgFile').updateValueAndValidity();
     const array =  <FormArray>this.productAddForm.get('ProductAndProductAttributes');
     array.controls.forEach(group => group.get('productMainImgFile').clearValidators());
     array.controls.forEach(group => group.get('productMainImgFile').setErrors({ 'required': null, 'emptyLines': null }));
     array.controls.forEach(group => group.get('productMainImgFile').updateValueAndValidity());
    

    }
    // stop here if form is invalid
    if (this.productAddForm.invalid) {
      return;
    }

    if (this.isAddMode) {
      this.submitProduct();
    } else {
      this.updateProduct();
    }
  }

  submitProduct() {
    this.submitted = true;

    // if (this.mrp < this.discount) {
    //   alert('MRP amount should be greater than Price');
    //   return;
    // }

    if (this.productAddForm.invalid) {
      return;
    }

    let productAttributs = this.productAddForm.value['ProductAndProductAttributes'];
    let Tags = this.productAddForm.value['ProductTags'];
    let postMultipartformData: FormData = this.getFormData(this.productAddForm.value);

    postMultipartformData.delete('ProductAndProductAttributes');
    postMultipartformData.delete('ProductSideImages');
    postMultipartformData.delete('productMainImgFile');
    postMultipartformData.delete('ProductTags');

    postMultipartformData.append('subCategoryId', this.SelCategorytype);
    Tags.forEach((element, index) => {
      postMultipartformData.append(`ProductTags[${index}]`, element.productTagslist);
    });

   

    productAttributs.forEach((element, i) => {
      // postMultipartformData.append(`ProductAndProductAttributes[${i}].productId`, '');
      postMultipartformData.append(
        `ProductAndProductAttributes[${i}].productAttributeId`,
        element.productAttributeId
      );
      postMultipartformData.append(
        `ProductAndProductAttributes[${i}].productSubAttributeId`,
        element.productSubAttributeId
      );
      postMultipartformData.append(
        `ProductAndProductAttributes[${i}].productStatusId`,
        element.productStatusId
      );
      postMultipartformData.append(
        `ProductAndProductAttributes[${i}].productAttributeName`,
        element.productAttributeName
      );
      postMultipartformData.append(
        `ProductAndProductAttributes[${i}].productAttributeValue`,
        element.productAttributeValue
      );
      postMultipartformData.append(
        `ProductAndProductAttributes[${i}].productDescription`,
        element.productDescription
      );

      
      this.mainImg.forEach((element, index) => {
        postMultipartformData.append(
          `ProductAndProductAttributes[${index}].productMainImgFile`,
          element
        );
      });
      postMultipartformData.append(
        `ProductAndProductAttributes[${i}].productSKU`,
        element.productSKU
      );
      postMultipartformData.append(
        `ProductAndProductAttributes[${i}].productPrice`,
        element.productPrice
      );
      postMultipartformData.append(
        `ProductAndProductAttributes[${i}].productMRP`,
        element.productMRP
      );
      postMultipartformData.append(
        `ProductAndProductAttributes[${i}].productMaxQty`,
        element.productMaxQty
      );
      postMultipartformData.append(
        `ProductAndProductAttributes[${i}].productDiscount`,
        element.productDiscount
      );
      postMultipartformData.append(
        `ProductAndProductAttributes[${i}].isApproved`,
        element.isApproved
      );
      postMultipartformData.append(`ProductAndProductAttributes[${i}].isActive`, element.isActive);
      this.images.some((element, i) => {
        element.forEach((img, j) => {
          postMultipartformData.append(
            `ProductAndProductAttributes[${i}].ProductSideImages[${j}].Images`,
            this.images[i][j]
          );
          console.log(this.images[i][j], "tevbsbf");
        });
        return;
        // postMultipartformData.append(
        //   `ProductAndProductAttributes[${i}].ProductSideImages[${j}].ImageName`,
        //   ''
        // );
      });
    });

    this.productService
      .insertProductByInput(postMultipartformData)
      .pipe()
      .subscribe({
        next: e => {
          this.notifyService.showSuccess('Product added Successfully.', '');
          this.router.navigate(['/product-list'], { relativeTo: this.route });

          this.submitted = false;
          this.productAddForm.reset();
          // this.getAllProductAttribute();
        },
        error: error => {
          this.notifyService.showError('Your request is not valid.', '');
          // console.log(error.message, 'error');
        },
      });
  }

  updateProduct() {

    let productAttributs = this.productAddForm.value['ProductAndProductAttributes'];
    let Tags = this.productAddForm.value['ProductTags'];
    let postMultipartformData: FormData = this.getFormData(this.productAddForm.value);

    postMultipartformData.delete('ProductAndProductAttributes');
    postMultipartformData.delete('ProductSideImages');
    postMultipartformData.delete('productMainImgFile');
    postMultipartformData.delete('ProductTags');

    postMultipartformData.append('subCategoryId', this.SelCategorytype);
    Tags.forEach((element, index) => {
      if (Tags!=''){
        postMultipartformData.append(`ProductTags[${index}]`, element.productTagslist);
      }
      else{
        postMultipartformData.append(`ProductTags[${index}]`, '');
      }
      
    });

    productAttributs.forEach((element, i) => {
      // postMultipartformData.append(`ProductAndProductAttributes[${i}].productId`, '');
      // element.productMainImgFile.at(i).controls.productMainImgFile.clearValidators();
      postMultipartformData.append(`ProductAndProductAttributes[${i}].id`, element.id);

      postMultipartformData.append(
        `ProductAndProductAttributes[${i}].productAttributeId`,
        element.productAttributeId
      );
      postMultipartformData.append(
        `ProductAndProductAttributes[${i}].productSubAttributeId`,
        element.productSubAttributeId
      );
      postMultipartformData.append(
        `ProductAndProductAttributes[${i}].productStatusId`,
        element.productStatusId
      );
      postMultipartformData.append(
        `ProductAndProductAttributes[${i}].productAttributeName`,
        element.productAttributeName
      );
      postMultipartformData.append(
        `ProductAndProductAttributes[${i}].productAttributeValue`,
        element.productAttributeValue
      );
      postMultipartformData.append(
        `ProductAndProductAttributes[${i}].productDescription`,
        element.productDescription
      );
      this.mainImg.forEach((element, index) => {
        postMultipartformData.append(
          `ProductAndProductAttributes[${index}].productMainImgFile`,
          element
        );
      });
      postMultipartformData.append(
        `ProductAndProductAttributes[${i}].productSKU`,
        element.productSKU
      );
      postMultipartformData.append(
        `ProductAndProductAttributes[${i}].productPrice`,
        element.productPrice
      );
      postMultipartformData.append(
        `ProductAndProductAttributes[${i}].productMRP`,
        element.productMRP
      );
      postMultipartformData.append(
        `ProductAndProductAttributes[${i}].productMaxQty`,
        element.productMaxQty
      );
      postMultipartformData.append(
        `ProductAndProductAttributes[${i}].productDiscount`,
        element.productDiscount
      );
      postMultipartformData.append(
        `ProductAndProductAttributes[${i}].isApproved`,
        element.isApproved
      );
      postMultipartformData.append(`ProductAndProductAttributes[${i}].isActive`, element.isActive);
      console.log(this.images[i],"upload");
      
      this.images.some((element, i) => {
        element.forEach((img, j) => {
          postMultipartformData.append(
            `ProductAndProductAttributes[${i}].ProductSideImages[${j}].Images`,
            this.images[i][j]
          );
          console.log(this.images[i][j], "tevbsbf");
        });
        return;
        // postMultipartformData.append(
        //   `ProductAndProductAttributes[${i}].ProductSideImages[${j}].ImageName`,
        //   element[j].name
        // );
      });
    });

    this.productService
      .updateProductDetailsByIdAndEditProductUploadDto(this.id, postMultipartformData)
      .pipe()
      .subscribe({
        next: e => {
          this.notifyService.showSuccess('Product updated Successfully.', '');
          this.submitted = false;
          this.productAddForm.reset();
          this.router.navigate(['/product-list'], { relativeTo: this.route });
        },
        error: error => {
          this.notifyService.showError('Your request is not valid.', '');
          // console.log(error.message, 'error');
        },
      });
  }
}
