import { Component, OnInit, ElementRef, NgZone, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { StoreMasterService } from '@proxy/market-place/store-masters/store-master.service';
import { StoreUploadMasterDto } from '@proxy/market-place/store-masters/dtos/models';
import { IdentificationTypeService } from '@proxy/market-place/identification-type-masters';
import { StoreTypeService } from '@proxy/market-place/store-type-masters/store-type.service';
import { StoreTypeDto } from '@proxy/market-place/store-type-masters/dtos';
import { AreaService } from '@proxy/market-place/area-masters/area.service';
import { ListService, PagedResultDto } from '@abp/ng.core';
import { MapsAPILoader, MouseEvent as AGMMouseEvent } from '@agm/core';
import { MarketPlaceService } from '@proxy/market-place/controllers/market-place.service';
import { NotificationService } from 'src/app/shared/notification.service';

import { ActivatedRoute, Router } from '@angular/router';
import { IdentityRoleService } from '@abp/ng.identity/proxy';

import { StoreImageService } from '@proxy/market-place/store-image-masters/store-image.service'
import { Confirmation, ConfirmationService } from '@abp/ng.theme.shared';

@Component({
  selector: 'app-update-store',
  templateUrl: './update-store.component.html',
  styleUrls: ['./update-store.component.scss']
})
export class UpdateStoreComponent implements OnInit {

  ownerDetails!: FormGroup;
  userDetails!: FormGroup;
  file_stepDetails!: FormGroup;
  personal_step = false;
  fileupload_step = false;
  file_step = false;
  user_step = false;
  step = 1;
  submitted = false;
  images: File[] = [];
  documents: File[] = [];
  imageSrc: string[] = [];
  fileName: File;
  rows: any = [];
  latitude!: number;
  longitude!: number;
  uploadstore: StoreUploadMasterDto;
  identificationtype: any;
  storetype: any;
  area: any;
  address: string;
  private geoCoder;

  zoom: number;
  selectedFiles: any;
  currentFileUpload: any;
  tenantId: string;
  storeId: string;
  user_roles: any[];
  @ViewChild('agmSearch',{ static: false }) searchElementRef: ElementRef;
  id: any;
  isAddMode: boolean;
  docId: void;
  apiURL = "https://marketplace.projectnimbus.co.in/StoreImages"
  serverImgs: any;
  user: any;
  time:any;
  // public searchElementRef: ElementRef;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private storeService: StoreMasterService,
    private store_types: StoreTypeService,
    private identificationtypeService: IdentificationTypeService,
    private areaService: AreaService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private notifyService: NotificationService,
    private roles:IdentityRoleService,
    private StoreImageService:StoreImageService,
    private confirmation: ConfirmationService,
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.isAddMode = !this.id;
    if (!this.isAddMode) {
      this.storeService.getEditStoreByIdByStoreId(this.id)
          .pipe()
          .subscribe((x : any) =>{
            this.ownerDetails.patchValue(x)
            let t = x.tenantId;
            let s = x.storeId;
            this.tenantId = t;
            this.storeId = s;
            this.time = x.storeStartTime;
            // console.log(this.time,'======');

            // this.ownerDetails.setValue({ 
            //   storeStartTime: x.storeStartTime.format('hh:mm')})

            for (let index = 0; index < x.storeImagelst.length; index++) {
              let img = x.storeImagelst[index].storeImageFileName
              this.serverImgs = x.storeImagelst
            }
            console.log(x,"------")
            for (let index = 0; index < x.storeDocslst.length; index++) {
              let expiryDate = x.storeDocslst[index].expiryDate
              var newDate= new Date(expiryDate)
              
              var mnth = ("0" + (newDate.getMonth() + 1)).slice(-2),
              day = ("0" + newDate.getDate()).slice(-2);
              var date = [day, mnth,newDate.getFullYear()].join("-");
              console.log(date,"date")
              // this.ownerDetails.controls['documentList'].value.expirydate.patchValue(expiryDate)
              if(index > 0){
                this.addDocument()
              }
            }
            
            this.ownerDetails.controls['documentList'].patchValue(x.storeDocslst)
            // this.ownerDetails.controls['documentList'].get('expirydate').patchValue(date)

            for (let index = 0; index < x.storeUserlst.length; index++) {
              let storeUser = x.storeUserlst[index].roleId
              this.user = x.storeUserlst[index].userId
              // console.log(this.user,"*****")
              if(index > 0){
                this.addUser()
              }
            }

            let abc = this.userDetails.controls['userArray'].patchValue(x.storeUserlst)
            
            
            for (let index = 0; index < x.rolesList.length; index++) {
              let role = x.rolesList
              this.user_roles = role
              // console.log(this.user_roles,"*****") 
            }
          }
          );
  }

    this.getDocumentTypelist();
    this.getstoretypes();
    this.getArea();
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
      this.geoCoder = new google.maps.Geocoder();
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
        });
      });
    });

    this.ownerDetails = this.formBuilder.group({
      storeOwnerName: ['', Validators.required],
      storeOwnerEmail: ['', Validators.required],
      storeOwnerPass: ['', Validators.required],
      name: ['', Validators.required],
      storeNo: ['', Validators.required],
      storeAddress: ['', Validators.required],
      storeLattitude: ['', Validators.required],
      storeLongitude: ['', Validators.required],
      adminshare: ['', Validators.required],
      storeRange: ['', Validators.required],
      storeOrderPerTimeSlot: ['', Validators.required],
      storeStartTime: ['', Validators.required],
      storeEndTime: ['', Validators.required],
      storeTimeSlotInterval: ['', Validators.required],
      storeApporval: [''],
      areaId: ['', Validators.required],
      storeTypeId: ['', Validators.required],
      storeOwnerPAN: ['', [Validators.required, Validators.pattern(/[A-Z]{5}[0-9]{4}[A-Z]{1}/)]],
      storeOwnerAadhar: [
        '',
        [ Validators.pattern(/^[2-9]{1}[0-9]{3}[0-9]{4}[0-9]{4}$/)],
      ],
      storeImageFile: this.formBuilder.array([]),

      tenantId: new FormControl('', [Validators.required]),
      storeId: new FormControl('', [Validators.required]),

      documentList: new FormArray([
        new FormGroup({
          identificationTypeId: new FormControl('', [Validators.required]),
          identificationFile: new FormControl(null, []),
          expirydate: new FormControl('', [Validators.required]),
          storeDocId:new FormControl('', [Validators.required]),
        }),
      ]),
    });

    this.userDetails = this.formBuilder.group({
      userArray: new FormArray([
        new FormGroup({
          tenantId: new FormControl('', [Validators.required]),
          userName: new FormControl('', [Validators.required]),
          userEmail: new FormControl('', [Validators.required]),
          userPass: new FormControl('', [Validators.required]),
          userId: new FormControl('', [Validators.required]),
          roleId: new FormControl('', [Validators.required]),
          storeId: new FormControl('', [Validators.required]),
        }),
      ]),
    });
  }

  addDocument() {
    (<FormArray>this.ownerDetails.controls['documentList']).push(
      new FormGroup({
        identificationTypeId: new FormControl('', [Validators.required]),
        identificationFile: new FormControl(null, []),
        expirydate: new FormControl('', [Validators.required]),
        storeDocId:new FormControl('', [Validators.required]),
      })
    );
  }
  removeDocument(index) {
    const control = <FormArray>this.ownerDetails.controls['documentList'];
    if (control.length > 1) {
      control.removeAt(index);
    }

  }

  addUser() {
    const control = <FormArray>this.userDetails.controls['userArray'];
    control.push(
      new FormGroup({
        tenantId: new FormControl('', [Validators.required]),
        userName: new FormControl('', [Validators.required]),
        userEmail: new FormControl('', [Validators.required]),
        userPass: new FormControl('', [Validators.required]),
        userId: new FormControl(null),
        roleId: new FormControl('', [Validators.required]),
        storeId: new FormControl('', [Validators.required]),
      })
    );
  }

  removeUser(index) {
    const control = <FormArray>this.userDetails.controls['userArray'];
    if (control.length > 1) {
      control.removeAt(index);
    }
  }

  get s() {
    return this.ownerDetails.controls;
  }


  get uservalidation() {
    return this.userDetails.controls['userArray'];
  }


  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition(position => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 8;
        this.getAddress(this.latitude, this.longitude);
      });
    }
  }

  markerDragEnd($event: AGMMouseEvent) {
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
  }

  
  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ location: { lat: latitude, lng: longitude } }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.address = results[0].formatted_address;
        } else {
          window.alert('No results found');
        }
      } else {
        console.log('Geocoder failed due to: ' + status);
      }
    });
  }
  onMapClicked(event: any) {
    console.table(event.coords);
    this.latitude = event.coords.lat;
    this.longitude = event.coords.lng;
  }

  imgChange(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        var reader = new FileReader();

        reader.onload = (event: any) => {
          // console.log(event.target.result);
          this.imageSrc.push(event.target.result);
        }

        reader.readAsDataURL(event.target.files[i]);
      }
    }


    for (let index = 0; index < event.target.files.length; index++) {
      this.images.push(event.target.files[index]);

    }
  }

  fileupload(event, i) {
    const file: File = event.target.files[0];
    if (file) {
      this.documents[i] = file;
    }
  }

  getFormData(object) {
    const formData = new FormData();
    Object.keys(object).forEach(key => formData.append(key, object[key]));
    return formData;
  }

  removeSelectedFile(index) {
    this.imageSrc.splice(index, 1);
    this.images.splice(index, 1);
  }

  getArea() {
    this.areaService.getAllArea().subscribe((data: any) => {
      this.area = data.items;
      // console.log(data,"+++++++++");
    });
  }

  getDocumentTypelist() {
    this.identificationtypeService.allIdentificationType().subscribe((data: any) => {
      this.identificationtype = data.items;
      // console.log(this.identificationtype);
    });
  }

  getstoretypes() {
    this.store_types.getAllStoreType().subscribe((response: any) => {
      this.storetype = response.items;
      // console.log(this.storetype);
    });
  }

  deleteImg(id){
    // console.log(id)
    this.confirmation.warn('::Are You Sure To Delete Image', '::AreYouSure').subscribe((status) => {
      if (status === Confirmation.Status.confirm) {
        this.StoreImageService.delete(id).subscribe(() =>{
          this.serverImgs.splice(id, 1);
          this.notifyService.showSuccess(' Deleted Successfully.', '');
        });
      }
    });

  }


  updateStore() {
    let documetList = this.ownerDetails.value['documentList'];
    const array =  <FormArray>this.ownerDetails.get('documentList');
    
    array.controls.forEach(group => group.get('identificationFile').clearValidators());
    array.controls.forEach(group => group.get('identificationFile').setErrors({ 'required': null, 'emptyLines': null }));
    array.controls.forEach(group => group.get('identificationFile').updateValueAndValidity());

    this.ownerDetails.value.storeLongitude = this.longitude;
    this.ownerDetails.value.storeLattitude = this.latitude;

    let postMultipartformData: FormData = this.getFormData(this.ownerDetails.value);

    postMultipartformData.delete('storeImageFile');
    postMultipartformData.delete('documentList');

      this.images.forEach(element => {
        postMultipartformData.append('storeImageFile', element);
      });
    

    this.documents.forEach(element => {
      postMultipartformData.append('identificationFile', element);
    });

    documetList.forEach(element => {
      postMultipartformData.append('identificationTypeId', element.identificationTypeId);
      postMultipartformData.append('expiryDate', element.expirydate);
      postMultipartformData.append('storeDocId', element.storeDocId);
      
    });

  
    this.storeService
      .editStoreDetailsByEditStoreMasterDto(postMultipartformData)
      .pipe()
      .subscribe({
        next: () => {
          this.notifyService.showSuccess('store Updated Successfully.', '');
          // this.getAllCategory()
          this.router.navigate(['/store'], { relativeTo: this.route });
        },
        error: error => {
          this.notifyService.showError("Something went wrong", '');
          this.submitted = false;
        },
      });
  }

  updateUser() {
    // console.log( 'error');
    this.submitted = true;
    // if (this.userDetails.invalid) {
    //   return;
    // } 
    // console.log(this.userDetails.value['userArray'],"++++++");
    
    this.storeService
      .editStoreWiseUserByUserMasterDto(JSON.stringify(this.userDetails.value['userArray']))
      .pipe()
      .subscribe({
        next: e => {
          this.notifyService.showSuccess('User Details updated Successfully', '');
          this.router.navigate(['/store'], { relativeTo: this.route });
        },
        error: error => {
          this.notifyService.showError(
            '',
            error
          );
          console.log(error.message, 'error');
        },
      });
  }


}
