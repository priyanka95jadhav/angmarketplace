import { Component, OnInit, ElementRef, NgZone, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { StoreMasterService } from '@proxy/market-place/store-masters/store-master.service';
import { StoreUploadMasterDto } from '@proxy/market-place/store-masters/dtos/models';
import { IdentificationTypeService } from '@proxy/market-place/identification-type-masters';
import { StoreTypeService } from '@proxy/market-place/store-type-masters/store-type.service';
import { StoreTypeDto } from '@proxy/market-place/store-type-masters/dtos';
import { AreaService } from '@proxy/market-place/area-masters/area.service';
import { ListService, PagedResultDto } from '@abp/ng.core';
import { MapsAPILoader, MouseEvent as AGMMouseEvent } from '@agm/core';
import { MarketPlaceService } from '@proxy/market-place/controllers/market-place.service';
import { NotificationService } from 'src/app/shared/notification.service';

import { ActivatedRoute, Router } from '@angular/router';
import { IdentityRoleService } from '@abp/ng.identity/proxy';


@Component({
  selector: 'app-store-add',
  templateUrl: './store-add.component.html',
  styleUrls: ['./store-add.component.scss'],
  providers: [ListService],
})
export class StoreAddComponent implements OnInit {
  ownerDetails!: FormGroup;
  userDetails!: FormGroup;
  file_stepDetails!: FormGroup;
  personal_step = false;
  fileupload_step = false;
  file_step = false;
  user_step = false;
  step = 1;
  submitted = false;
  images: File[] = [];
  documents: File[] = [];
  imageSrc: string[] = [];
  fileName: File;
  rows: any = [];
  latitude!: number;
  longitude!: number;
  uploadstore: StoreUploadMasterDto;
  identificationtype: any;
  storetype: any;
  area: any;
  address: string;
  private geoCoder;

  zoom: number;
  selectedFiles: any;
  currentFileUpload: any;
  tenantId: string;
  storeId: string;
  user_roles: any[];
  @ViewChild('agmSearch',{ static: false }) searchElementRef: ElementRef;
  id: any;
  isAddMode: boolean;
 

  // public searchElementRef: ElementRef;
  constructor(
    public readonly list: ListService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private storeService: StoreMasterService,
    private store_types: StoreTypeService,
    private identificationtypeService: IdentificationTypeService,
    private areaService: AreaService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private notifyService: NotificationService,
    private roles:IdentityRoleService
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.isAddMode = !this.id;

    console.log(this.tenantId,this.storeId)
    this.getDocumentTypelist();
    this.getstoretypes();
    this.getArea();
 //load Places Autocomplete
 this.mapsAPILoader.load().then(() => {
  this.setCurrentLocation();
  this.geoCoder = new google.maps.Geocoder;

  let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
    types: ["address"]
  });
  autocomplete.addListener("place_changed", () => {
    this.ngZone.run(() => {
      //get the place result
      let place: google.maps.places.PlaceResult = autocomplete.getPlace();

      //verify result
      if (place.geometry === undefined || place.geometry === null) {
        return;
      }

      //set latitude, longitude and zoom
      this.latitude = place.geometry.location.lat();
      this.longitude = place.geometry.location.lng();
      this.zoom = 12;
    });
  });
});

    this.ownerDetails = this.formBuilder.group({
      storeOwnerName: ['', Validators.required],
      storeOwnerEmail: ['', Validators.required],
      storeOwnerPass: ['', Validators.required],
      name: ['', Validators.required],
      storeNo: ['', Validators.required],
      storeAddress: ['', Validators.required],
      storeLattitude: ['', Validators.required],
      storeLongitude: ['', Validators.required],
      adminshare: ['', Validators.required],
      storeRange: ['', Validators.required],
      storeOrderPerTimeSlot: ['', Validators.required],
      storeStartTime: ['', Validators.required],
      storeEndTime: ['', Validators.required],
      storeTimeSlotInterval: ['', Validators.required],
      storeApporval: [''],
      areaId: ['', Validators.required],
      storeTypeId: ['', Validators.required],
      storeOwnerPAN: ['', [Validators.required, Validators.pattern(/[A-Z]{5}[0-9]{4}[A-Z]{1}/)]],
      storeOwnerAadhar: [
        '',
        [Validators.required, Validators.pattern(/^[2-9]{1}[0-9]{3}[0-9]{4}[0-9]{4}$/)],
      ],
      storeImageFile: this.formBuilder.array([]),

      documentList: new FormArray([
        new FormGroup({
          identificationTypeId: new FormControl('', [Validators.required]),
          identificationFile: new FormControl('', [Validators.required]),
          expirydate: new FormControl('', [Validators.required]),
        }),
      ]),
    });

    this.userDetails = this.formBuilder.group({
      userArray: new FormArray([
        new FormGroup({
          tenantId: new FormControl('', [Validators.required]),
          userName: new FormControl('', [Validators.required]),
          userEmail: new FormControl('', [Validators.required]),
          userPass: new FormControl('', [Validators.required]),
          roleId: new FormControl('', [Validators.required]),
          storeId: new FormControl('', [Validators.required]),
        }),
      ]),
    });

    if (!this.isAddMode) {
      this.storeService.getEditStoreByIdByStoreId(this.id)
          .pipe()
          .subscribe((x : any) =>{
            this.ownerDetails.patchValue(x)
            // this.IdOfUser = x.userId 
            // this.getStoreId = x.storeId
            console.log( x,"3435")
          }
          );
  }

  }

  addDocument() {
    (<FormArray>this.ownerDetails.controls['documentList']).push(
      new FormGroup({
        identificationTypeId: new FormControl('', [Validators.required]),
        identificationFile: new FormControl('', [Validators.required]),
        expirydate: new FormControl('', [Validators.required]),
      })
    );
  }
  removeDocument(index) {
    const control = <FormArray>this.ownerDetails.controls['documentList'];
    if (control.length > 1) {
      control.removeAt(index);
    }

  }

  addUser() {
    const control = <FormArray>this.userDetails.controls['userArray'];
    control.push(
      new FormGroup({
        tenantId: new FormControl('', [Validators.required]),
        userName: new FormControl('', [Validators.required]),
        userEmail: new FormControl('', [Validators.required]),
        userPass: new FormControl('', [Validators.required]),
        roleId: new FormControl('', [Validators.required]),
        storeId: new FormControl('', [Validators.required]),
      })
    );
  }

  removeUser(index) {
    const control = <FormArray>this.userDetails.controls['userArray'];
    if (control.length > 1) {
      control.removeAt(index);
    }
  }

  get s() {
    return this.ownerDetails.controls;
  }


  get uservalidation() {
    return this.userDetails.controls['userArray'];
  }

  next() {
    if (this.step == 1) {
      if (this.ownerDetails.controls['storeOwnerName'].invalid,this.ownerDetails.controls['storeOwnerEmail'].invalid,this.ownerDetails.controls['storeOwnerPass'].invalid) {
        return ;
      }
      this.personal_step = true;
      this.step++;
    } else if (this.step == 2) {
      if (this.ownerDetails.controls['name'].invalid,this.ownerDetails.controls['storeNo'].invalid,this.ownerDetails.controls['storeAddress'].invalid,
      this.ownerDetails.controls['storeLattitude'].invalid,this.ownerDetails.controls['storeLongitude'].invalid,this.ownerDetails.controls['adminshare'].invalid,
      this.ownerDetails.controls['storeRange'].invalid,this.ownerDetails.controls['storeOrderPerTimeSlot'].invalid,this.ownerDetails.controls['areaId'].invalid,
      this.ownerDetails.controls['storeStartTime'].invalid,this.ownerDetails.controls['storeEndTime'].invalid,this.ownerDetails.controls['storeTimeSlotInterval'].invalid,
      this.ownerDetails.controls['storeTypeId'].invalid,this.ownerDetails.controls['storeOwnerPAN'].invalid,this.ownerDetails.controls['storeOwnerAadhar'].invalid) {
        return;
      }
      this.file_step = true;
      this.step++;
    }

   if  (this.step == 3) {
      if (this.ownerDetails.controls['documentList'].invalid) {
        return;
      }
      this.user_step = true;
      this.step++;
    }
  }


  previous() {
    this.step--;

    if (this.step == 1) {
      this.fileupload_step = false;
    }
    if (this.step == 2) {
      this.file_step = false;
    }
    if (this.step == 3) {
      this.user_step = false;
    }
  }
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition(position => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 8;
        this.getAddress(this.latitude, this.longitude);
      });
    }
  }

  markerDragEnd($event: AGMMouseEvent) {
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ location: { lat: latitude, lng: longitude } }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.address = results[0].formatted_address;
        } else {
          window.alert('No results found');
        }
      } else {
        console.log('Geocoder failed due to: ' + status);
      }
    });
  }
  onMapClicked(event: any) {
    console.table(event.coords);
    this.latitude = event.coords.lat;
    this.longitude = event.coords.lng;
  }

  imgChange(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        var reader = new FileReader();

        reader.onload = (event: any) => {
          // console.log(event.target.result);
          this.imageSrc.push(event.target.result);
        }

        reader.readAsDataURL(event.target.files[i]);
      }
    }


    for (let index = 0; index < event.target.files.length; index++) {
      this.images.push(event.target.files[index]);

    }
  }

  fileupload(event, i) {
    const file: File = event.target.files[0];
    if (file) {
      this.documents[i] = file;
    }
  }

  getFormData(object) {
    const formData = new FormData();
    Object.keys(object).forEach(key => formData.append(key, object[key]));
    return formData;
  }

  removeSelectedFile(index) {
    this.imageSrc.splice(index, 1);
    this.images.splice(index, 1);
  }

  getArea() {
    this.areaService.getAllArea().subscribe((data: any) => {
      this.area = data.items;
      // console.log(data,"+++++++++");
    });
  }

  getDocumentTypelist() {
    this.identificationtypeService.allIdentificationType().subscribe((data: any) => {
      this.identificationtype = data.items;
      console.log(this.identificationtype);
    });
  }

  getstoretypes() {
    const getstoretypes = query => this.store_types.getAllStoreType();

    this.list.hookToQuery(getstoretypes).subscribe(response => {
      this.storetype = response.items;
      // console.log(response,"*******");
    });
  }

  savestore() {
    this.submitted = true;
    // if (this.ownerDetails.invalid) {
    //   return;
    // }
    let documetList = this.ownerDetails.value['documentList'];
    let DocumetControlList = this.ownerDetails.get('documentList')['controls'];
    this.ownerDetails.value.storeLongitude = this.longitude;
    this.ownerDetails.value.storeLattitude = this.latitude;

    let postMultipartformData: FormData = this.getFormData(this.ownerDetails.value);

    postMultipartformData.delete('storeImageFile');
    postMultipartformData.delete('documentList');
    this.images.forEach(element => {
      postMultipartformData.append('storeImageFile', element);
    });

    this.documents.forEach(element => {
      postMultipartformData.append('identificationFile', element);
    });

    documetList.forEach(element => {
      postMultipartformData.append('identificationTypeId', element.identificationTypeId);
      postMultipartformData.append('expiryDate', element.expirydate);
    });

    this.storeService
      .uploadFileByStoreMasterDto(postMultipartformData)
      .pipe().subscribe((response) => {
        this.tenantId = response.tenantId;
        this.storeId = response.storeId;
        this.user_roles = response.userRoles
        this.ownerDetails.reset();
        this.notifyService.showSuccess('Store Details added Successfully', '');
        this.user_step = true;
        // this.last_step();
        this.submitted = false;
        
     });
  }

  saveuser() {
    this.submitted = true;
    if (this.userDetails.invalid) {
      return;
    } 
    this.storeService
      .createStoreWiseUserByUserMasterDto(JSON.stringify(this.userDetails.value['userArray']))
      .pipe()
      .subscribe({
        next: e => {
          this.notifyService.showSuccess('User Details added Successfully', '');
          this.ownerDetails.reset();
          this.router.navigate(['/store'], { relativeTo: this.route });
        },
        error: error => {
          this.notifyService.showError(
            '',
            error
          );
          console.log(error.message, 'error');
        },
      });
  }

  private updateStore() {
    let documetList = this.ownerDetails.value['documentList'];
    let DocumetControlList = this.ownerDetails.get('documentList')['controls'];
    this.ownerDetails.value.storeLongitude = this.longitude;
    this.ownerDetails.value.storeLattitude = this.latitude;

    let postMultipartformData: FormData = this.getFormData(this.ownerDetails.value);

    postMultipartformData.delete('storeImageFile');
    postMultipartformData.delete('documentList');
    this.images.forEach(element => {
      postMultipartformData.append('storeImageFile', element);
    });

    this.documents.forEach(element => {
      postMultipartformData.append('identificationFile', element);
    });

    documetList.forEach(element => {
      postMultipartformData.append('identificationTypeId', element.identificationTypeId);
      postMultipartformData.append('expiryDate', element.expirydate);
    });

  
    this.storeService
      .update(this.id,postMultipartformData)
      .pipe()
      .subscribe({
        next: () => {
          this.notifyService.showSuccess('store Updated Successfully.', '');
          // this.getAllCategory()
          this.router.navigate(['/store'], { relativeTo: this.route });
        },
        error: error => {
          this.notifyService.showError("Something went wrong", '');
          this.submitted = false;
        },
      });
  }

}
