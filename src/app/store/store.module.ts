import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StoreRoutingModule } from './store-routing.module';
import { StoreComponent } from './store.component';
import { StoreDocumentMasterComponent } from './store-document-master/store-document-master.component';
import { StoreEarningComponent } from './store-earning/store-earning.component';
import { StoreImageComponent } from './store-image/store-image.component';
import { StorePayoutRequestComponent } from './store-payout-request/store-payout-request.component';
import { StorePayoutValidationComponent } from './store-payout-validation/store-payout-validation.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MastersRoutingModule } from '../masters/masters-routing.module';
import { SharedModule } from '../shared/shared.module';
import { MastersModule } from '../masters/masters.module';
import { StoreListComponent } from './store-list/store-list.component';
import { StoreAddComponent } from './store-add/store-add.component';
import { AgmCoreModule } from '@agm/core';
import { UpdateStoreComponent } from './update-store/update-store.component';


@NgModule({
  declarations: [
    StoreComponent,
    StoreDocumentMasterComponent,
    StoreEarningComponent,
    StoreImageComponent,
    StorePayoutRequestComponent,
    StorePayoutValidationComponent,
    StoreListComponent,
    StoreAddComponent,
    UpdateStoreComponent
   
  ],
  imports: [
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC_gWDX_jS5qCUyutiEoSe5lTPsd_7WM_c&address=india',
      libraries: ["places"]
    }),
    SharedModule,
    MastersRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MastersModule,
    StoreRoutingModule
  ]
})
export class StoreModule { }
