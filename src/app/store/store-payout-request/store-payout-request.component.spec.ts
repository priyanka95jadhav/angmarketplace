import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StorePayoutRequestComponent } from './store-payout-request.component';

describe('StorePayoutRequestComponent', () => {
  let component: StorePayoutRequestComponent;
  let fixture: ComponentFixture<StorePayoutRequestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StorePayoutRequestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StorePayoutRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
