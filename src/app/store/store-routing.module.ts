import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StoreAddComponent } from './store-add/store-add.component';
import { StoreDocumentMasterComponent } from './store-document-master/store-document-master.component';
import { StoreEarningComponent } from './store-earning/store-earning.component';
import { StoreImageComponent } from './store-image/store-image.component';
import { StoreListComponent } from './store-list/store-list.component';
import { StorePayoutRequestComponent } from './store-payout-request/store-payout-request.component';
import { StorePayoutValidationComponent } from './store-payout-validation/store-payout-validation.component';
import { StoreComponent } from './store.component';
import { UpdateStoreComponent } from './update-store/update-store.component';

const routes: Routes = [
  { path: '', component: StoreComponent },
  { path: 'store', component: StoreListComponent},
  { path: 'store/add', component: StoreAddComponent},
  { path: 'store/edit/:id', component: UpdateStoreComponent}

  // {path: 'storedocument', component: StoreDocumentMasterComponent},
  // { path: 'storeearning', component: StoreEarningComponent},
  // {path: 'storeimage', component: StoreImageComponent},
  // {path: 'storepayoutrequest', component: StorePayoutRequestComponent},
  // {path: 'storepayoutvalidation', component: StorePayoutValidationComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StoreRoutingModule { }
