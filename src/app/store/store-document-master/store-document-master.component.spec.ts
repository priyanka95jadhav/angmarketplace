import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreDocumentMasterComponent } from './store-document-master.component';

describe('StoreDocumentMasterComponent', () => {
  let component: StoreDocumentMasterComponent;
  let fixture: ComponentFixture<StoreDocumentMasterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StoreDocumentMasterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreDocumentMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
