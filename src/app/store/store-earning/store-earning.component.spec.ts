import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreEarningComponent } from './store-earning.component';

describe('StoreEarningComponent', () => {
  let component: StoreEarningComponent;
  let fixture: ComponentFixture<StoreEarningComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StoreEarningComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreEarningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
