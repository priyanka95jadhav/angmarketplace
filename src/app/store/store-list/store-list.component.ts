import { Component, OnInit } from '@angular/core';
import { ListService, PagedResultDto } from '@abp/ng.core';
import { StoreUploadMasterDto } from '@proxy/market-place/store-masters/dtos';
import { StoreMasterService } from '@proxy/market-place/store-masters';
import { NotificationService } from 'src/app/shared/notification.service';
import { Confirmation, ConfirmationService } from '@abp/ng.theme.shared';

@Component({
  selector: 'app-store-list',
  templateUrl: './store-list.component.html',
  styleUrls: ['./store-list.component.scss'],
  providers: [ListService],
  
})
export class StoreListComponent implements OnInit {

  storelist = { items: [], totalCount: 0 } as PagedResultDto<StoreUploadMasterDto>;
  stores: any;

  constructor(public readonly list: ListService , private storeService: StoreMasterService,private notifyService: NotificationService, private confirmation: ConfirmationService,) { }

  ngOnInit(): void {
    this.getStores()
    
  }

  getStores(){
    const storeStreamCreator = (query) => this.storeService.getList(query);

    this.list.hookToQuery(storeStreamCreator).subscribe((response) => {
      this.stores = response;
      // console.log(response);
    });
  }

  

}

