import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StorePayoutValidationComponent } from './store-payout-validation.component';

describe('StorePayoutValidationComponent', () => {
  let component: StorePayoutValidationComponent;
  let fixture: ComponentFixture<StorePayoutValidationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StorePayoutValidationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StorePayoutValidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
