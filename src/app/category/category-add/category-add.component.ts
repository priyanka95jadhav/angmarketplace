import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryAttributeService } from '@proxy/market-place/category-attribute-masters';
import { NotificationService } from 'src/app/shared/notification.service';

@Component({
  selector: 'app-category-add',
  templateUrl: './category-add.component.html',
  styleUrls: ['./category-add.component.scss'],
})
export class CategoryAddComponent implements OnInit {
  categoryForm!: FormGroup;
  submitted = false;
  allCategory: any;
  selectedFiles: any;
  formFile: File;
  id: string;
  isAddMode: boolean;
  constructor(
    private formBuilder: FormBuilder,
    private notifyService: NotificationService,
    private categoryService: CategoryAttributeService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.isAddMode = !this.id;
    this.categoryForm = this.formBuilder.group({
      categoryName: ['', Validators.required],
      parentCategoryId: new FormControl(''),
      categoryImageFile: new FormControl(''),
    });

    this.getAllCategory();

    if (!this.isAddMode) {
      this.categoryService.get(this.id)
          .pipe()
          .subscribe(x => this.categoryForm.patchValue(x)
          );
          
          // this.categoryForm.value.categoryName.patchValue(x.categoryName)
          // this.categoryForm.value.categoryImageFile.patchValue(x.categoryImagePath)
          // this.categoryForm.value.parentCategoryId.patchValue(x.parentCategoryId)
  }
  }
  get categoryval() {
    return this.categoryForm.controls;
  }

  getAllCategory() {
    this.categoryService.getCategoryHierarchyList().subscribe((data: any) => {
      this.allCategory = data;
      console.log(this.allCategory);
    });
  }

  categoryImg(event) {
    const file: File = event.target.files[0];

    if (file) {
      const formData = new FormData();
      this.selectedFiles = event.target.files;
      this.formFile = this.selectedFiles.item(0);
    }
  }

  getFormData(object) {
    const formData = new FormData();
    Object.keys(object).forEach(key => formData.append(key, object[key]));
    return formData;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.categoryForm.invalid) {
      return;
    }

    if (this.isAddMode) {
      this.saveCategory();
    } else {
      this.updateCategory();
    }
  }

  private saveCategory() {
    this.submitted = true;
    if (this.categoryForm.invalid) {
      return;
    }

    let postMultipartformData: FormData = this.getFormData(this.categoryForm.value);
    postMultipartformData.delete('categoryImageFile');
    postMultipartformData.append('categoryImageFile', this.formFile);
    
    // postMultipartformData.forEach(element => {
    //   console.log(element,"============"); 
    // })

    this.categoryService
      .create(postMultipartformData)
      .pipe()
      .subscribe({
        next: e => {
          this.notifyService.showSuccess('Category added Successfully.', '');
          // this.router.navigate(['/category'], { relativeTo: this.route });

          
          this.submitted = false;
          this.categoryForm.reset();
          this.getAllCategory();
        },
        error: error => {
          this.notifyService.showError('Your request is not valid.', '');
          console.log(error.message, 'error');
        },
      });
  }

  private updateCategory() {
    let postMultipartformData: FormData = this.getFormData(this.categoryForm.value);
    postMultipartformData.delete('categoryImageFile');
    postMultipartformData.append('categoryImageFile', this.formFile);
    this.categoryService
      .update(this.id, postMultipartformData)
      .pipe()
      .subscribe({
        next: () => {
          this.notifyService.showSuccess('Category Updated Successfully.', '');
          // this.getAllCategory()
          this.router.navigate(['/category'], { relativeTo: this.route });
        },
        error: error => {
          this.notifyService.showError(error, '');
          this.submitted = false;
        },
      });
  }
}
