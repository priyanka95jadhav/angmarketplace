import { Component, OnInit } from '@angular/core';
import { ListService, PagedResultDto } from '@abp/ng.core';
import { CategoryAttributeService } from '@proxy/market-place/category-attribute-masters';
import { CategoryAttributeDto } from '@proxy/market-place/category-attribute-masters/dtos';
import { NotificationService } from 'src/app/shared/notification.service';
import { Confirmation,ConfirmationService } from '@abp/ng.theme.shared';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'],
  providers: [ListService],
})
export class CategoryComponent implements OnInit {

  categorylist = { items: [], totalCount: 0 } as PagedResultDto<CategoryAttributeDto>;
  allCategory: any;

  constructor(public readonly list: ListService, private categoryService: CategoryAttributeService,private confirmation: ConfirmationService,private notifyService:NotificationService ) { }

  ngOnInit(): void {
    // get Category
    this.getCategory()
  }

  getCategory(){
    const getAllCategory = (query) => this.categoryService.getList(query);

    this.list.hookToQuery(getAllCategory).subscribe((response) => {
      this.allCategory = response;
      console.log(response);
    });
  }
// Delete Category
  deleteCategory(id:any){
    this.confirmation.warn('::Are You Sure To Delete', '::AreYouSure').subscribe((status) => {
      if (status === Confirmation.Status.confirm) {
        this.categoryService.delete(id).subscribe(() => this.list.get());
        this.notifyService.showSuccess('Category deleted Successfully', '');
      }
    });
  }
  }

  // editCategory(id: any){
  //   this.categoryService.update(id).subscribe(() => {
  //     this.notifyService.showSuccess('Category Updated Successfully.', '');
  //   });
  // }


