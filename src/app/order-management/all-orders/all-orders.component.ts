import { Component, OnInit } from '@angular/core';
import { ListService, PagedResultDto } from '@abp/ng.core';
import { OrderService } from '@proxy/market-place/order-masters';
import { OrderDto } from '@proxy/market-place/order-masters/dtos';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OrderAndDeliveryBoyMappingService } from '@proxy/market-place/order-and-delivery-boy-mapping-masters';
import { OrderAndDeliveryBoyMappingDto } from '@proxy/market-place/order-and-delivery-boy-mapping-masters/dtos';
import { NotificationService } from 'src/app/shared/notification.service';
import { DeliveryBoyService } from '@proxy/market-place/delivery-boy-masters';

@Component({
  selector: 'app-all-orders',
  templateUrl: './all-orders.component.html',
  styleUrls: ['./all-orders.component.scss'],
  providers: [ListService],
})
export class AllOrdersComponent implements OnInit {
  orderlist = { items: [], totalCount: 0 } as PagedResultDto<OrderDto>;
  assign = {} as OrderAndDeliveryBoyMappingDto;
  assignOrderform: FormGroup;
  isModalOpen = false;
  submitted: boolean = false;
  orderId: any;
  boyDetails: any;
  IsassignOrder: boolean;

  constructor(
    private fb: FormBuilder,
    private notifyService: NotificationService,
    public readonly list: ListService,
    private orderService: OrderService,
    private assignOrder: OrderAndDeliveryBoyMappingService,
    private deliveryboyService:DeliveryBoyService,
  ) {}

  ngOnInit(): void {
    const orderStreamCreator = query => this.orderService.getList(query);

    this.list.hookToQuery(orderStreamCreator).subscribe(response => {
      this.orderlist = response;
      var list = response.items
      list.forEach(element => {
        this.IsassignOrder = element.isAssigned
        // console.log(this.isAssigned, 'element');
      });
      // console.log(response, 'orders');
    });

    this.getDeliveryBoy();
  }

  get f(): { [key: string]: AbstractControl } {
    return this.assignOrderform.controls;
  }

  getDeliveryBoy() {
    this.deliveryboyService.allDeliveryBoy().subscribe((data:any) =>{   
      
       this.boyDetails = data;
      //  data.items.forEach(element => {    
      //  console.log(data);
    
      //  });
     })
  
  }

  assignorder(id: string) {
    // this.orderService.get(id).subscribe((assign) => {
    // this.assign = assign;
    this.buildForm();
    this.isModalOpen = true;
    this.orderId = id;
    // console.log(id, '88');

    // });
  }

  assignOrderToDeliveryBoy() {
    this.assign = {} as OrderAndDeliveryBoyMappingDto;
    this.buildForm();
    this.isModalOpen = true;
  }

  buildForm() {
    this.assignOrderform = this.fb.group({
      orderId: [this.assign.orderId || ''],
      deliveryBoyId: [this.assign.deliveryBoyId || '', Validators.required],
    });
  }

  save() {
    // this.buildForm();
    this.submitted = true;

    if (this.assignOrderform.invalid) {
      return;
    }
    this.assignOrderform.value.orderId = this.orderId;
    this.assignOrder.create(this.assignOrderform.value).subscribe(() => {
      this.notifyService.showSuccess('Order Assigned To Delivery Boy Successfully.', '');
    });

    //request.subscribe(() => {
    this.isModalOpen = false;
    this.assignOrderform.reset();
    this.list.get();
    //});
  }
}
