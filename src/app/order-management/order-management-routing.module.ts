import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllOrdersComponent } from './all-orders/all-orders.component';
import { OrderManagementComponent } from './order-management.component';

const routes: Routes = [
  { path: '', component: OrderManagementComponent },
  { path: 'all-orders', component: AllOrdersComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrderManagementRoutingModule { }
