import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryBoyIncentiveComponent } from './delivery-boy-incentive.component';

describe('DeliveryBoyIncentiveComponent', () => {
  let component: DeliveryBoyIncentiveComponent;
  let fixture: ComponentFixture<DeliveryBoyIncentiveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeliveryBoyIncentiveComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryBoyIncentiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
