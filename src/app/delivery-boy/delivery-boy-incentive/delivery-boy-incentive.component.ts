import { Component, OnInit } from '@angular/core';
import { ListService, PagedResultDto } from '@abp/ng.core';
import { DeliveryBoyService } from '@proxy/market-place/delivery-boy-masters';
import { DeliveryBoyDto } from '@proxy/market-place/delivery-boy-masters/dtos';

@Component({
  selector: 'app-delivery-boy-incentive',
  templateUrl: './delivery-boy-incentive.component.html',
  styleUrls: ['./delivery-boy-incentive.component.scss'],
  providers: [ListService],
})
export class DeliveryBoyIncentiveComponent implements OnInit {

  deliveryboylist = { items: [], totalCount: 0 } as PagedResultDto<DeliveryBoyDto>;

  constructor(public readonly list: ListService , private deliveryboyService: DeliveryBoyService) { }

  ngOnInit(): void {
    const deliveryboyStreamCreator = (query) => this.deliveryboyService.getList(query);

    this.list.hookToQuery(deliveryboyStreamCreator).subscribe((response) => {
      this.deliveryboylist = response;
      console.log(response);
    });
  }

}
