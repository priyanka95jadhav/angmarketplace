import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeliveryBoyAddComponent } from './delivery-boy-add/delivery-boy-add.component';
import { DeliveryBoyIncentiveComponent } from './delivery-boy-incentive/delivery-boy-incentive.component';
import { DeliveryBoyListComponent } from './delivery-boy-list/delivery-boy-list.component';
import { DeliveryBoyComponent } from './delivery-boy.component';

const routes: Routes = [
  { path: '', component: DeliveryBoyComponent },
  { path: 'add', component:DeliveryBoyAddComponent},
  { path: 'deliveryboy', component: DeliveryBoyListComponent},
  { path: 'deliveryboy-incentive', component: DeliveryBoyIncentiveComponent},
  { path: 'add/:id', component: DeliveryBoyAddComponent }
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeliveryBoyRoutingModule { }
