import { Component, OnInit } from '@angular/core';
import { ListService, PagedResultDto } from '@abp/ng.core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CityService } from '@proxy/market-place/cities';
import { CityDto } from '@proxy/market-place/cities/dtos';
import { DeliveryBoyService } from '@proxy/market-place/delivery-boy-masters';
import { DeliveryBoyDto } from '@proxy/market-place/delivery-boy-masters/dtos';
import { IdentificationTypeService } from '@proxy/market-place/identification-type-masters';
import { IdentificationTypeDto } from '@proxy/market-place/identification-type-masters/dtos';
import { StoreMasterService } from '@proxy/market-place/store-masters';
import { StoreMasterDto } from '@proxy/market-place/store-masters/dtos';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ConfigStateService } from '@abp/ng.core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService } from '@abp/ng.theme.shared';
import { NotificationService } from 'src/app/shared/notification.service';




@Component({
  selector: 'app-delivery-boy-add',
  templateUrl: './delivery-boy-add.component.html',
  styleUrls: ['./delivery-boy-add.component.scss'],
  providers: [ListService],
})
export class DeliveryBoyAddComponent implements OnInit {

  deliveryboyaddform: FormGroup;
  city: CityDto;
  // identificationType:IdentificationTypeDto;
  identificationtype: any;
  identificationtype1: any;
  store: StoreMasterDto;
  storeName: any;
  // storeName = [];
  dropdownList: any;
  storeId: string[] = [];
  dropdownSettings = {};
  selectedFiles: any;
  identifacationFile1: any;
  formFile: File;
  formFile1: File;
  storearray: any = [];
  storeid: any;
  storename: any;
  // currentUser: any;
  //list: any;
  id: string;
  isAddMode: boolean;

  submitted = false;
  router: any;
  alldeliveryboy: any;
  IdOfUser: any;
  getStoreId: any = [];

  constructor(private fb: FormBuilder,
    private deliveryboyService: DeliveryBoyService,
    private cityService: CityService,
    private identificationtypeService: IdentificationTypeService,
    private storeMasterService: StoreMasterService,
    private config: ConfigStateService,
    private route: ActivatedRoute,
    private _route: Router,
    private _toaster: ToasterService,
    private notifyService: NotificationService,
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.isAddMode = !this.id;

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'storeId',
      textField: 'storeName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      // itemsShowLimit: 3,
      allowSearchFilter: true
    };

    // this.currentUser = this.config.getOne("currentUser");
    // console.log(this.currentUser.id,'hh');
    // this.currentUser = this.config.getOne("currentUser");
    // console.log(this.currentUser.userName,'aa');
    // this.currentUser = this.config.getOne("currentUser");
    // console.log(this.currentUser.email,'fg');



    this.deliveryboyaddform = this.fb.group({
          boyName: ['', [
            Validators.required,
            // Validators.minLength(6),
            // Validators.maxLength(20),
            // Validators.pattern('^[A-Z].*$'),
          ]
        ],
          boyPhone: ['', [
            Validators.required,
            Validators.pattern("^[0-9]{10}$")
          ]
        ],
          boyUsername: ['',[
            Validators.required,
            // Validators.minLength(6),
            // Validators.maxLength(20),
            // Validators.pattern('^[A-Z].*$'),
          ]
        ],
          boyEmail: ['',[
            Validators.required, 
            Validators.email,
            Validators.pattern('[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}')
          ]
        ],
          cityId: ['', Validators.required],
          identificationTypeId: ['', Validators.required],
          identificationNumber: ['', Validators.required],
          formFile: ['',],
          identificationTypeId1: ['', Validators.required],
          identificationNumber1: ['', Validators.required],
          formFile1: ['',],
          address: ['', Validators.required],
          storeId: ['', Validators.required],
          userId: ['', Validators.required],
        });



    this.getCity();
    this.getIdentificationType();
    this.getIdentificationType1();

    this.getStore();
    //this.getAllDeliveryBoy();

    if (!this.isAddMode) {
      this.deliveryboyService.get(this.id)
          .pipe()
          .subscribe((x : any) =>{
            this.deliveryboyaddform.patchValue(x)
            this.IdOfUser = x.userId 
            this.getStoreId = x.storeId
            console.log( this.getStoreId,"3435")
          }
          );
  }
  }


  
  get f(): { [key: string]: AbstractControl } {
    return this.deliveryboyaddform.controls;
  }

  // getAllDeliveryBoy() {
  //   this.deliveryboyService.allDeliveryBoy().subscribe((data: any) => {
  //     this.alldeliveryboy = data;
  //     console.log(this.alldeliveryboy);
  //   });
  // }

  getCity() {
    this.cityService.getCity().subscribe((data: any) => {
      this.city = data.data;
      console.log(this.city,"JHGJH")
      // data.items.forEach(element => {
      //   this.city = element;
      //   console.log(this.city,"JHGJH")
      // });
    })
  }

  getIdentificationType() {
    this.identificationtypeService.allIdentificationType().subscribe((data: any) => {
      data.items.forEach(element => {
        this.identificationtype = data.items;
         console.log(this.identificationtype)
      });
    })
  }


  getIdentificationType1() {
    this.identificationtypeService.allIdentificationType().subscribe((data: any) => {
      data.items.forEach(element => {
        this.identificationtype1 = data.items;
         console.log(this.identificationtype1)
      });
    })
  }


  getStore() {
      this.storeMasterService.allStores().subscribe((data:any) =>{
        this.storeName = data;
         data.forEach(element => {
          this.storeid = data.storeId;
          this.storename = data.storeName;
          //console.log(this.storeName,'hhjgjjh')
         });
      })

  }



  imgChange(event) {

    // this.fileName = event.target.files;
    // for (let i = 0; i < event.target.files; i++) {
    //   this.fileName = event.target.files.item(i);
    // }
    const file: File = event.target.files[0];

    if (file) {
      const formData = new FormData();
      this.selectedFiles = event.target.files;
      this.formFile = this.selectedFiles.item(0);

      // console.log(this.formFile);
      formData.append('file', this.formFile);
      formData.forEach((value, key) => {
        console.log(key + ' ' + value, 'hghg');
      });
    }
  }

  imgChange1(event) {

    // this.fileName = event.target.files;
    // for (let i = 0; i < event.target.files; i++) {
    //   this.fileName = event.target.files.item(i);
    // }
    const file: File = event.target.files[0];

    if (file) {
      const formData = new FormData();
      this.identifacationFile1 = event.target.files;
      this.formFile1 = this.identifacationFile1.item(0);

      // console.log(this.formFile);
      formData.append('file1', this.formFile1);
      formData.forEach((value, key) => {
        console.log(key + ' ' + value, 'hghg');
      });
    }
  }

  onItemSelect($event) {

    var store = this.deliveryboyaddform.value.storeId.length

    console.log($event, 'jkhkh');

    this.storearray.push((store))


  }
  onSelectAll(event) {
    var store = this.deliveryboyaddform.value.storeId.length;
    for (let i = 0; i < store; i++) {
      this.storearray = event[i]
      // this.storeId.push(event.target.result);
      console.log(event, 'jkhkh');
    }
    // console.log(this.storearray,"vvvvvvvvv===========")
  }



  changeCity(e) {
    console.log(e.target.value)

  }
  changeIdentificationType(e) {
    console.log(e.target.value)

  }

  changeIdentificationType1(e) {
    console.log(e.target.value)

  }


  changeStore(e) {
    console.log(e.target.value)

  }

  getFormData(object) {
    const formData = new FormData();
    Object.keys(object).forEach(key => formData.append(key, object[key]));
    return formData;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    // if (this.deliveryboyaddform.invalid) {
    //   return;
    // }

    if (this.isAddMode) {
      this.save();
    } else {
      this.updateDeliveryboy();
    }
  }

  save() {

    this.submitted = true;

    // if (this.deliveryboyaddform.invalid)
    //  {
    //    return;
    //  }
   

    var stors: string[] = [];
    this.deliveryboyaddform.value.formFile = this.formFile;
    this.deliveryboyaddform.value.formFile1 = this.formFile1;

    let formData = this.deliveryboyaddform.value;
    this.deliveryboyaddform.value['storeId']
    formData.storeId.forEach(element => {
      stors.push(element.storeId);
    });
    let postMultipartformData: FormData = this.getFormData(formData);
    postMultipartformData.delete('storeId');
    stors.forEach(element => {
      postMultipartformData.append('storeId', element);
    });

  this.deliveryboyService.addDeliveryBoyByInput(postMultipartformData)
  .pipe()
  .subscribe({
    next: e => {
      if(e.status ==false){
       
        this.notifyService.showError(e.message, '');
      }
      else{
        this.notifyService.showSuccess('Deliveryboy added Successfully.', '');
    
        this.deliveryboyaddform.reset();
        this._route.navigate(["/deliveryboy"]);
      }
      
    },
    error: error => {
      this.notifyService.showError('Your request is not valid.', '');
      console.log(error.status ==false, 'error');
    },
  });


}

private updateDeliveryboy() {
  var stors: string[] = [];
  this.deliveryboyaddform.value.formFile = this.formFile;
  this.deliveryboyaddform.value.formFile1 = this.formFile1;

  this.deliveryboyaddform.value.userId = this.IdOfUser

  let formData = this.deliveryboyaddform.value;
  // this.deliveryboyaddform.value['storeId'] = this.getStoreId

  formData.storeId.forEach(element => {
    stors.push(element.storeId);
  });
  let postMultipartformData: FormData = this.getFormData(formData);
  postMultipartformData.delete('storeId');
  this.getStoreId.forEach(element => {
    postMultipartformData.append('storeId', element);
  });

  this.deliveryboyService
    .updateDeliveryBoyByIdAndInput(this.id,postMultipartformData)
    .pipe()
    .subscribe({
      next: () => {
        this.notifyService.showSuccess('deliveryboy Updated Successfully.', '');
        // this.getAllCategory()
        this._route.navigate(["/deliveryboy"]);
      },
      error: error => {
        this.notifyService.showError("Something went wrong", '');
        this.submitted = false;
      },
    });
}

}

