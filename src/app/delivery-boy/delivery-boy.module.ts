import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeliveryBoyRoutingModule } from './delivery-boy-routing.module';
import { DeliveryBoyComponent } from './delivery-boy.component';
import { DeliveryBoyListComponent } from './delivery-boy-list/delivery-boy-list.component';
import { DeliveryBoyAddComponent } from './delivery-boy-add/delivery-boy-add.component';
import { DeliveryBoyIncentiveComponent } from './delivery-boy-incentive/delivery-boy-incentive.component';
import { SharedModule } from '../shared/shared.module';
import { MastersRoutingModule } from '../masters/masters-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MastersModule } from '../masters/masters.module';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';


@NgModule({
  declarations: [
    DeliveryBoyComponent,
    DeliveryBoyListComponent,
    DeliveryBoyAddComponent,
    DeliveryBoyIncentiveComponent
  ],
  imports: [
    CommonModule,
    DeliveryBoyRoutingModule,
    SharedModule,
    MastersRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MastersModule,
    DeliveryBoyRoutingModule,
    NgMultiSelectDropDownModule.forRoot()
  ]
})
export class DeliveryBoyModule { }
