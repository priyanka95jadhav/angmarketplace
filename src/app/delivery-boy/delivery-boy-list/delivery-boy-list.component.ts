import { Component, OnInit } from '@angular/core';
import { ListService, PagedResultDto } from '@abp/ng.core';
import { DeliveryBoyService } from '@proxy/market-place/delivery-boy-masters';
import { DeliveryBoyDto } from '@proxy/market-place/delivery-boy-masters/dtos';
import { NotificationService } from 'src/app/shared/notification.service';
import { Confirmation, ConfirmationService } from '@abp/ng.theme.shared';

@Component({
  selector: 'app-delivery-boy-list',
  templateUrl: './delivery-boy-list.component.html',
  styleUrls: ['./delivery-boy-list.component.scss'],
  providers: [ListService],
})
export class DeliveryBoyListComponent implements OnInit {

  deliveryboylist = { items: [], totalCount: 0 } as PagedResultDto<DeliveryBoyDto>;
  alldeliveryboy: PagedResultDto<DeliveryBoyDto>;

  constructor(public readonly list: ListService , 
              private deliveryboyService: DeliveryBoyService,
              private confirmation: ConfirmationService,
              private notifyService:NotificationService
              ) { }

  ngOnInit(): void {
    const deliveryboyStreamCreator = (query) => this.deliveryboyService.getList(query);

    this.list.hookToQuery(deliveryboyStreamCreator).subscribe((response) => {
      this.deliveryboylist = response;
      console.log(response);
    });
  }

  // ngOnInit(): void {
 
  //   this.getDeliveryBoy()
  // }

  
  // getDeliveryBoy(){
  //   const getAllDeliveryBoy = (query) => this.deliveryboyService.getList(query);

  //   this.list.hookToQuery(getAllDeliveryBoy).subscribe((response) => {
  //     this.alldeliveryboy = response;
  //     console.log(response);
  //   });
  // }

   // Add a delete method
  deleteDeliveryBoy(id: string) {
  this.confirmation.warn('::Are You Sure To Delete', '::AreYouSure').subscribe((status) => {
    if (status === Confirmation.Status.confirm) {
      this.deliveryboyService.delete(id).subscribe(() =>{
        this.notifyService.showSuccess(' Deleted Successfully.', '');
        this.list.get()
      });
    }
  });
}


}
