import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MastersRoutingModule } from './masters-routing.module';
import { MastersComponent } from './masters.component';
import { SharedModule } from '../shared/shared.module';
import { UnitComponent } from './unit/unit.component';
import { CitiesComponent } from './cities/cities.component';
import { StateComponent } from './state/state.component';
import { RewardsComponent } from './rewards/rewards.component';
import { RedeemValuesComponent } from './redeem-values/redeem-values.component';
import { OrderStatusComponent } from './order-status/order-status.component';
import { ProductTypeComponent } from './product-type/product-type.component';
import { StoreTypeComponent } from './store-type/store-type.component';
import { TaxesComponent } from './taxes/taxes.component';
import { TaxTypeComponent } from './tax-type/tax-type.component';
import { IdentificationComponent } from './identification/identification.component';
import { CurrencyComponent } from './currency/currency.component';
import { CancellingReasonsComponent } from './cancelling-reasons/cancelling-reasons.component';
import { AreaSocietyComponent } from './area-society/area-society.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductStatusComponent } from './product-status/product-status.component';
import { TimeSlotComponent } from './time-slot/time-slot.component';



@NgModule({
  declarations: [
    MastersComponent,
    UnitComponent,
    CitiesComponent,
    StateComponent,
    RewardsComponent,
    RedeemValuesComponent,
    OrderStatusComponent,
    ProductTypeComponent,
    StoreTypeComponent,
    TaxesComponent,
    TaxTypeComponent,
    IdentificationComponent,
    CurrencyComponent,
    CancellingReasonsComponent,
    AreaSocietyComponent,
    ProductStatusComponent,
    TimeSlotComponent,
  
  ],
  imports: [
    // CommonModule,
    SharedModule,
    MastersRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class MastersModule { }
