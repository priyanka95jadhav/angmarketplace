import { ListService, PagedResultDto } from '@abp/ng.core';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StateService } from '@proxy/market-place/states';
import { StateDto } from '@proxy/market-place/states/dtos';
import { ConfirmationService, Confirmation } from '@abp/ng.theme.shared';
import { NotificationService } from 'src/app/shared/notification.service';

@Component({
  selector: 'app-state',
  templateUrl: './state.component.html',
  styleUrls: ['./state.component.scss'],
  providers: [ListService],
})
export class StateComponent implements OnInit {

  stateform: FormGroup; 
  isModalOpen = false; // add this lines

  statelist = { items: [], totalCount: 0 } as PagedResultDto<StateDto>;
  
  selectedState = {} as StateDto; // declare selectedBook
  submitted: boolean = false;

  constructor( private fb: FormBuilder , 
    public readonly list: ListService , 
    private stateService: StateService ,
    private confirmation: ConfirmationService,
    private notifyService: NotificationService,
    ) { }


  ngOnInit(): void {
    const stateStreamCreator = (query) => this.stateService.getList(query);

    this.list.hookToQuery(stateStreamCreator).subscribe((response) => {
      this.statelist = response;
      console.log(response);
    });
  }

  get f(): { [key: string]: AbstractControl } {
    return this.stateform.controls;
  }


  createState() {
    this.selectedState = {} as StateDto; // reset the selected book
    this.buildForm();
    this.isModalOpen = true;
  }

  // Add editBook method
  editState(id: string) {
    this.stateService.get(id).subscribe((state) => {
      this.selectedState = state;
      this.buildForm();
      this.isModalOpen = true;
      
      
    });
  }

  // Add a delete method
delete(id: string) {
  this.confirmation.warn('::Are You Sure To Delete', '::AreYouSure').subscribe((status) => {
    if (status === Confirmation.Status.confirm) {
      this.stateService.delete(id).subscribe(() => this.list.get());
     this.notifyService.showSuccess('state deleted Successfully', '');

    }
  });
}

  buildForm() {
    this.stateform = this.fb.group({
      stateName: [this.selectedState.stateName || '', Validators.required],
      stateCode: [this.selectedState.stateCode || '', Validators.required],
    });
  }

  // change the save method
  save() {
    this.submitted = true;
    if (this.stateform.invalid) {
      return;
    }
  console.log(this.selectedState.id, 'stateupdate');

    const request = this.selectedState.id
      ? this.stateService.update(this.selectedState.id, this.stateform.value).subscribe(() =>{
        this.notifyService.showSuccess('state updated Successfully.', '');
      })
      : this.stateService.create(this.stateform.value).subscribe(() => {
        this.notifyService.showSuccess('state added Successfully.', '');
      })

   // request.subscribe(() => {
      this.isModalOpen = false;
      this.stateform.reset();
      this.list.get();
   // });
  }
 

}
