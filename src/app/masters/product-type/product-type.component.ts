import { ListService, PagedResultDto } from '@abp/ng.core';
import { Confirmation, ConfirmationService } from '@abp/ng.theme.shared';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductTypeService } from '@proxy/market-place/product-type-masters';
import { ProductTypeDto } from '@proxy/market-place/product-type-masters/dtos';
import { NotificationService } from 'src/app/shared/notification.service';

@Component({
  selector: 'app-product-type',
  templateUrl: './product-type.component.html',
  styleUrls: ['./product-type.component.scss'],
  providers: [ListService],
})
export class ProductTypeComponent implements OnInit {
  producttypelist = { items: [], totalCount: 0 } as PagedResultDto<ProductTypeDto>;
  producttypeform: FormGroup; // add this line
  isModalOpen = false; // add this line
  // taxTypeName: any;
  selectedProductType = {} as ProductTypeDto;
  submitted: boolean=false;
  constructor(public readonly list: ListService,
     private producttypeService: ProductTypeService,
     private fb: FormBuilder,
     private confirmation: ConfirmationService,
     private notifyService: NotificationService,
     ) { }

  ngOnInit(): void {
    const  producttypeStreamCreator = (query) => this.producttypeService.getList(query);

    this.list.hookToQuery( producttypeStreamCreator).subscribe((response) => {
      this.producttypelist = response;
    });
  }

  get f(): { [key: string]: AbstractControl } {
    return this.producttypeform.controls;
  }


  // add new method
  createproducttype() {
    this.selectedProductType = {} as ProductTypeDto;
    this.buildForm();
    this.isModalOpen = true;
  }

  // Add editBook method
  editproducttype(id: string) {
    this.producttypeService.get(id).subscribe((producttype) => {
      this.selectedProductType = producttype;
      this.buildForm();
      this.isModalOpen = true;
      
      
    });
  }

  // Add a delete method
delete(id: string) {
  this.confirmation.warn('::Are You Sure To Delete', '::AreYouSure').subscribe((status) => {
    if (status === Confirmation.Status.confirm) {
      this.producttypeService.delete(id).subscribe(() => this.list.get());
      this.notifyService.showSuccess('Product type deleted Successfully', '');

    }
  });
}

   // add buildForm method
   buildForm() {
    this.producttypeform = this.fb.group({
      productTypeName: [this.selectedProductType.productTypeName ||'', Validators.required],
      
    });
  }

  // add save method
  // save() {
  //   if (this.taxtypeform.invalid) {
  //     return;
  //   }

  //   console.log(this.taxtypeform.value,'hjhj')

  //   this.taxtypeService.create(this.taxtypeform.value).subscribe(() => {
  //     this.isModalOpen = false;
  //     this.taxtypeform.reset();
  //     this.list.get();
  //   });
  // }

   // change the save method
   save() {
    this.submitted = true;
    if (this.producttypeform.invalid) {
      return;
    }
  console.log(this.selectedProductType.id, 'stateupdate');

    const request = this.selectedProductType.id
      ? this.producttypeService.update(this.selectedProductType.id, this.producttypeform.value).subscribe(() =>{
        this.notifyService.showSuccess('product type updated Successfully.', '');
      })
      : this.producttypeService.create(this.producttypeform.value).subscribe(() => {
        this.notifyService.showSuccess('product type added Successfully.', '');
      })

   // request.subscribe(() => {
      this.isModalOpen = false;
      this.producttypeform.reset();
      this.list.get();
   // });
  }
 

}
