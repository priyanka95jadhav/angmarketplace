import { ListService, PagedResultDto } from '@abp/ng.core';
import { Confirmation, ConfirmationService } from '@abp/ng.theme.shared';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OrderStatusService } from '@proxy/market-place/order-status-masters';
import { OrderStatusDto } from '@proxy/market-place/order-status-masters/dtos';
import { NotificationService } from 'src/app/shared/notification.service';

@Component({
  selector: 'app-order-status',
  templateUrl: './order-status.component.html',
  styleUrls: ['./order-status.component.scss'],
  providers: [ListService],
})
export class OrderStatusComponent implements OnInit {
  orderstatuslist = { items: [], totalCount: 0 } as PagedResultDto<OrderStatusDto>;
  orderstatusform: FormGroup; // add this line
  isModalOpen = false; // add this line
  // taxTypeName: any;
  selectedOrderStatus = {} as OrderStatusDto;
  submitted: boolean = false;

  constructor(public readonly list: ListService,
     private orderstatusService: OrderStatusService,
     private fb: FormBuilder,
     private confirmation: ConfirmationService,
    private notifyService: NotificationService,
    ) { }

  ngOnInit(): void {
    const orderstatusStreamCreator = (query) => this.orderstatusService.getList(query);

    this.list.hookToQuery(orderstatusStreamCreator).subscribe((response) => {
      this.orderstatuslist = response;
    });
  }

  get f(): { [key: string]: AbstractControl } {
    return this.orderstatusform.controls;
  }

   // add new method
   createorderstatus() {
    this.selectedOrderStatus = {} as OrderStatusDto;
    this.buildForm();
    this.isModalOpen = true;
  }

  // Add editBook method
  editorderstatus(id: string) {
    this.orderstatusService.get(id).subscribe((orderstatus) => {
      this.selectedOrderStatus = orderstatus;
      this.buildForm();
      this.isModalOpen = true;
      
      
    });
  }

  // Add a delete method
delete(id: string) {
  this.confirmation.warn('::Are You Sure To Delete', '::AreYouSure').subscribe((status) => {
    if (status === Confirmation.Status.confirm) {
      this.orderstatusService.delete(id).subscribe(() => this.list.get());
      this.notifyService.showSuccess('Order status deleted Successfully', '');

    }
  });
}

   // add buildForm method
   buildForm() {
    this.orderstatusform = this.fb.group({
      orderStatusName: [this.selectedOrderStatus.orderStatusName ||'', Validators.required],
      
    });
  }

  // add save method
  // save() {
  //   if (this.taxtypeform.invalid) {
  //     return;
  //   }

  //   console.log(this.taxtypeform.value,'hjhj')

  //   this.taxtypeService.create(this.taxtypeform.value).subscribe(() => {
  //     this.isModalOpen = false;
  //     this.taxtypeform.reset();
  //     this.list.get();
  //   });
  // }

   // change the save method
   save() {
    this.submitted = true;
    if (this.orderstatusform.invalid) {
      return;
    }
  console.log(this.selectedOrderStatus.id, 'stateupdate');

    const request = this.selectedOrderStatus.id
      ? this.orderstatusService.update(this.selectedOrderStatus.id, this.orderstatusform.value).subscribe(() =>{
        this.notifyService.showSuccess('order status updated Successfully.', '');
      })
      : this.orderstatusService.create(this.orderstatusform.value).subscribe(() => {
        this.notifyService.showSuccess('order status added Successfully.', '');
      })

  //  request.subscribe(() => {
      this.isModalOpen = false;
      this.orderstatusform.reset();
      this.list.get();
   // });
  }
 

  
 

}
