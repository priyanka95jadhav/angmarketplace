import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CancellingReasonsComponent } from './cancelling-reasons.component';

describe('CancellingReasonsComponent', () => {
  let component: CancellingReasonsComponent;
  let fixture: ComponentFixture<CancellingReasonsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CancellingReasonsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CancellingReasonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
