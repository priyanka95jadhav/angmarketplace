import { ListService, PagedResultDto } from '@abp/ng.core';
import { Confirmation, ConfirmationService } from '@abp/ng.theme.shared';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CancellationReasonService } from '@proxy/market-place/cancellation-reason-masters';
import { CancellationReasonDto } from '@proxy/market-place/cancellation-reason-masters/dtos';
import { NotificationService } from 'src/app/shared/notification.service';

@Component({
  selector: 'app-cancelling-reasons',
  templateUrl: './cancelling-reasons.component.html',
  styleUrls: ['./cancelling-reasons.component.scss'],
  providers: [ListService],
})
export class CancellingReasonsComponent implements OnInit {
  cancelform: FormGroup; 
  isModalOpen = false; // add this line
  cancelreasonslist = { items: [], totalCount: 0 } as PagedResultDto<CancellationReasonDto>;
  selectedReason = {} as CancellationReasonDto;
  submitted: boolean = false;
  
 

  constructor( private fb: FormBuilder,
    public readonly list: ListService,
    private cancellationreasonService: CancellationReasonService,
    private confirmation: ConfirmationService,
    private notifyService: NotificationService,
    ) { }

  ngOnInit(): void {

    const cancelreasonsStreamCreator = (query) => this.cancellationreasonService.getList(query);

    this.list.hookToQuery(cancelreasonsStreamCreator).subscribe((response) => {
      this.cancelreasonslist= response;
    });

  }

  get f(): { [key: string]: AbstractControl } {
    return this.cancelform.controls;
  }

  createreason(){
    this.selectedReason = {} as CancellationReasonDto;
    this.buildForm();
    this.isModalOpen = true;
  }

   // Add editBook method
   editreason(id: string) {
    this.cancellationreasonService.get(id).subscribe((reason) => {
      this.selectedReason = reason;
      this.buildForm();
      this.isModalOpen = true;
      
      
    });
  }

  // Add a delete method
delete(id: string) {
  this.confirmation.warn('::Are You Sure To Delete', '::AreYouSure').subscribe((status) => {
    if (status === Confirmation.Status.confirm) {
      this.cancellationreasonService.delete(id).subscribe(() => this.list.get());
      this.notifyService.showSuccess('Cancel Reason deleted Successfully', '');
    }
  });
}

    // add buildForm method
    buildForm() {
      this.cancelform = this.fb.group({
        reason: [this.selectedReason.reason ||'', Validators.required],
        
        
      });
    }

    // add save method
  save() {
    this.submitted = true;
    if (this.cancelform.invalid) {
      return;
    }
    
    // this.cancellationreasonService.create(this.cancelform.value).subscribe(() => {
    //   this.isModalOpen = false;
    //   this.cancelform.reset();
    //   this.list.get();
    // });

    const request = this.selectedReason.id
    ? this.cancellationreasonService.update(this.selectedReason.id, this.cancelform.value).subscribe(() =>{
      this.notifyService.showSuccess('cancelling reason updated Successfully.', '');
    })
    : this.cancellationreasonService.create(this.cancelform.value).subscribe(() => {
      this.notifyService.showSuccess('cancelling reason added Successfully.', '');
    })


  // request.subscribe(() => {
    this.isModalOpen = false;
    this.cancelform.reset();
    this.list.get();
    
  // });

 
  }

}
