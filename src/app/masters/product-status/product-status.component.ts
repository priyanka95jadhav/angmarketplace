import { ListService, PagedResultDto } from '@abp/ng.core';
import { Confirmation, ConfirmationService } from '@abp/ng.theme.shared';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductStatusService } from '@proxy/market-place/product-status-masters';
import { ProductStatusDto } from '@proxy/market-place/product-status-masters/dtos';
import { NotificationService } from 'src/app/shared/notification.service';

@Component({
  selector: 'app-product-status',
  templateUrl: './product-status.component.html',
  styleUrls: ['./product-status.component.scss'],
  providers: [ListService],
  
})
export class ProductStatusComponent implements OnInit {
  productstatuslist = { items: [], totalCount: 0 } as PagedResultDto<ProductStatusDto>;
  productstatusform: FormGroup; // add this line
  isModalOpen = false; // add this line
  // taxTypeName: any;
  selectedProductStatus = {} as ProductStatusDto;
  submitted: boolean = false;

  constructor(public readonly list: ListService,
       private productstatusService: ProductStatusService,
       private fb: FormBuilder,
       private confirmation: ConfirmationService,
       private notifyService: NotificationService,

       ) { }

  ngOnInit(): void {
    const productstatusStreamCreator = (query) => this.productstatusService.getList(query);

    this.list.hookToQuery(productstatusStreamCreator).subscribe((response) => {
      this.productstatuslist = response;
    });
  }

  get f(): { [key: string]: AbstractControl } {
    return this.productstatusform.controls;
  }


    // add new method
    createproductstatus() {
      this.selectedProductStatus = {} as ProductStatusDto;
      this.buildForm();
      this.isModalOpen = true;
    }
  
    // Add editBook method
    editproductstatus(id: string) {
      this.productstatusService.get(id).subscribe((productstatus) => {
        this.selectedProductStatus = productstatus;
        this.buildForm();
        this.isModalOpen = true;
        
        
      });
    }
  
    // Add a delete method
  delete(id: string) {
    this.confirmation.warn('::Are You Sure To Delete', '::AreYouSure').subscribe((status) => {
      if (status === Confirmation.Status.confirm) {
        this.productstatusService.delete(id).subscribe(() => this.list.get());
       this.notifyService.showSuccess('Product status deleted Successfully', '');

      }
    });
  }
  
     // add buildForm method
     buildForm() {
      this.productstatusform = this.fb.group({
        productStatusName: [this.selectedProductStatus.productStatusName ||'', Validators.required],
        
      });
    }
  
    // add save method
    // save() {
    //   if (this.taxtypeform.invalid) {
    //     return;
    //   }
  
    //   console.log(this.taxtypeform.value,'hjhj')
  
    //   this.taxtypeService.create(this.taxtypeform.value).subscribe(() => {
    //     this.isModalOpen = false;
    //     this.taxtypeform.reset();
    //     this.list.get();
    //   });
    // }
  
     // change the save method
     save() {
      this.submitted = true;
      if (this.productstatusform.invalid) {
        return;
      }
    console.log(this.selectedProductStatus.id, 'stateupdate');
  
      const request = this.selectedProductStatus.id
        ? this.productstatusService.update(this.selectedProductStatus.id, this.productstatusform.value).subscribe(() =>{
          this.notifyService.showSuccess('product status updated Successfully.', '');
        })
        : this.productstatusService.create(this.productstatusform.value).subscribe(() => {
        this.notifyService.showSuccess('product status added Successfully.', '');
      })
  
     // request.subscribe(() => {
        this.isModalOpen = false;
        this.productstatusform.reset();
        this.list.get();
     // });
    }
     

}
