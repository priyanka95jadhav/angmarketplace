import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AreaSocietyComponent } from './area-society/area-society.component';
import { CancellingReasonsComponent } from './cancelling-reasons/cancelling-reasons.component';
import { CitiesComponent } from './cities/cities.component';
import { CurrencyComponent } from './currency/currency.component';
import { IdentificationComponent } from './identification/identification.component';
import { MastersComponent } from './masters.component';
import { OrderStatusComponent } from './order-status/order-status.component';
import { ProductStatusComponent } from './product-status/product-status.component';
import { ProductTypeComponent } from './product-type/product-type.component';
import { RedeemValuesComponent } from './redeem-values/redeem-values.component';
import { RewardsComponent } from './rewards/rewards.component';
import { StateComponent } from './state/state.component';
import { StoreTypeComponent } from './store-type/store-type.component';
import { TaxTypeComponent } from './tax-type/tax-type.component';
import { TaxesComponent } from './taxes/taxes.component';
import { TimeSlotComponent } from './time-slot/time-slot.component';
import { UnitComponent } from './unit/unit.component';

const routes: Routes = [
  { path: '', component: MastersComponent },
  { path: 'unit',component: UnitComponent},
  { path: 'cities',component: CitiesComponent},
  { path: 'state',component: StateComponent},
  { path: 'rewards',component: RewardsComponent},
  { path: 'redeem-values',component: RedeemValuesComponent},
  { path: 'order-status',component: OrderStatusComponent},
  { path: 'product-type',component: ProductTypeComponent},
  { path: 'store-type',component: StoreTypeComponent},
  { path: 'taxes',component: TaxesComponent},
  { path: 'tax-type',component: TaxTypeComponent},
  { path: 'identification',component: IdentificationComponent},
  { path: 'currency',component: CurrencyComponent},
  { path: 'cancelling-reasons',component: CancellingReasonsComponent},
  { path: 'area-society',component: AreaSocietyComponent},
  { path: 'product-status',component: ProductStatusComponent},
  { path: 'time-slot',component: TimeSlotComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MastersRoutingModule {}
