import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ListService, PagedResultDto } from '@abp/ng.core';
import { Confirmation, ConfirmationService } from '@abp/ng.theme.shared';
import { NotificationService } from 'src/app/shared/notification.service';
import { TimeSlotDto } from '@proxy/market-place/time-slot-masters/dtos';
import { TimeSlotService } from '@proxy/market-place/time-slot-masters/time-slot.service';

@Component({
  selector: 'app-time-slot',
  templateUrl: './time-slot.component.html',
  styleUrls: ['./time-slot.component.scss'],
  providers: [ListService],
})
export class TimeSlotComponent implements OnInit {
  timeslotform: FormGroup;
  isModalOpen = false; // add this lines

  timeslotlist = { items: [], totalCount: 0 } as PagedResultDto<TimeSlotDto>;

  selectedTimeSlot = {} as TimeSlotDto; // declare selectedBook
  submitted: boolean = false;
  now:any;

  constructor(
    private fb: FormBuilder,
    public readonly list: ListService,
    private timeslotService: TimeSlotService,
    private confirmation: ConfirmationService,
    private notifyService: NotificationService
  ) {}

  ngOnInit(): void {
    const timeslotStreamCreator = query => this.timeslotService.getList(query);

    this.list.hookToQuery(timeslotStreamCreator).subscribe(response => {
      this.timeslotlist = response;
      console.log(response);
    });
  }

  get f(): { [key: string]: AbstractControl } {
    return this.timeslotform.controls;
  }

  createTimeSlot() {
    this.selectedTimeSlot = {} as TimeSlotDto; // reset the selected book
    this.buildForm();
    this.isModalOpen = true;
  }

  // Add editBook method
  editTimeslot(id: string) {
    this.timeslotService.get(id).subscribe(timeslot => {
      this.selectedTimeSlot = timeslot;
      this.buildForm();
      this.isModalOpen = true;
    });
  }

  // Add a delete method
  delete(id: string) {
    this.confirmation.warn('::Are You Sure To Delete', '::AreYouSure').subscribe(status => {
      if (status === Confirmation.Status.confirm) {
        this.timeslotService.delete(id).subscribe(() => this.list.get());
        this.notifyService.showSuccess('time slot deleted Successfully', '');
      }
    });
  }

  buildForm() {
    this.timeslotform = this.fb.group({
      startTime: [this.selectedTimeSlot.startTime || '', Validators.required],
      endTime: [this.selectedTimeSlot.endTime || '', Validators.required],
    });
  }

  // change the save method
  save() {
    this.submitted = true;

    if (this.timeslotform.invalid) {
      return;
    }
    
    // console.log(this.selectedTimeSlot.id, 'sss');
    var today:any = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); 
    var yyyy = today.getFullYear();
    
    today = yyyy + '-' + mm + '-' + dd;

    this.timeslotform.value.startTime = today + 'T' +  this.timeslotform.value.startTime;
    this.timeslotform.value.endTime = today + 'T' +  this.timeslotform.value.endTime;

    const request = this.selectedTimeSlot.id
      ? this.timeslotService
          .update(this.selectedTimeSlot.id, this.timeslotform.value)
          .subscribe(() => {
            this.notifyService.showSuccess('time slot updated Successfully.', '');
          })
      : this.timeslotService.create(this.timeslotform.value).subscribe(() => {
          this.notifyService.showSuccess('time slot added Successfully.', '');
        });

    //request.subscribe(() => {
    this.isModalOpen = false;
    this.timeslotform.reset();
    this.list.get();
    //});
  }
}
