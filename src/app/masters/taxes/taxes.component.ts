import { ListService, PagedResultDto } from '@abp/ng.core';
import { Confirmation, ConfirmationService } from '@abp/ng.theme.shared';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TaxService } from '@proxy/market-place/tax-masters';
import { TaxDto } from '@proxy/market-place/tax-masters/dtos';
import { TaxTypeService } from '@proxy/market-place/tax-type-masters';
import { TaxTypeDto } from '@proxy/market-place/tax-type-masters/dtos';
import { NotificationService } from 'src/app/shared/notification.service';


@Component({
  selector: 'app-taxes',
  templateUrl: './taxes.component.html',
  styleUrls: ['./taxes.component.scss'],
  providers: [ListService],
  
})
export class TaxesComponent implements OnInit {

  taxlist = { items: [], totalCount: 0 } as PagedResultDto<TaxDto>;
  isModalOpen = false;
  taxform: FormGroup;
  taxtype = {} as TaxTypeDto;
  taxtypelist: any;
  selectedTax: TaxDto;
  submitted: boolean=false;
  

  constructor( private fb: FormBuilder ,
     public readonly list: ListService ,
     private taxService: TaxService , 
     private taxtypeService : TaxTypeService ,
     private confirmation: ConfirmationService,
     private notifyService: NotificationService,
     ) { }

  ngOnInit(): void {
    const taxStreamCreator = (query) => this.taxService.getList(query);

    this.list.hookToQuery(taxStreamCreator).subscribe((response) => {
      this.taxlist = response;
      console.log(response);
    });

   
  }

  get f(): { [key: string]: AbstractControl } {
    return this.taxform.controls;
  }

  changeTax(e) {
    console.log(e.target.value)
    
    // this.stateName.setValue(e.target.value, {
    //   onlySelf: true
    // })
  }

   
  createTax() {
    this.selectedTax = {} as TaxDto;
    this.buildForm(); // add this line
    this.isModalOpen = true;
  }

  // Add editBook method
  editTax(id: string) {
  this.taxService.get(id).subscribe((tax) => {
    this.selectedTax = tax;
    this.buildForm();
    this.isModalOpen = true;
    
    
  });
  
  
}

   // Add a delete method
delete(id: string) {
  this.confirmation.warn('::Are You Sure To Delete', '::AreYouSure').subscribe((status) => {
    if (status === Confirmation.Status.confirm) {
      this.taxService.delete(id).subscribe(() => this.list.get());
      this.notifyService.showSuccess('taxes deleted Successfully', '');

    }
  });
}

  

  // add buildForm method
  buildForm() {
    this.taxform = this.fb.group({
      taxName: [this.selectedTax.taxName ||'', Validators.required],
      taxPercentage: [this.selectedTax.taxPercentage ||'', Validators.required],
     
    });
  }

  // add save method
  save() {
    this.submitted = true;
    if (this.taxform.invalid) {
      return;
    }

// console.log(this.taxform.value);

    // this.taxService.create(this.taxform.value).subscribe(() => {
    //   this.isModalOpen = false;
    //   this.taxform.reset();
    //   this.list.get();
    // });


    const request = this.selectedTax.id
    ? this.taxService.update(this.selectedTax.id, this.taxform.value).subscribe(() =>{
    this.notifyService.showSuccess('taxes updated Successfully.', '');
    })
    : this.taxService.create(this.taxform.value).subscribe(() => {
      this.notifyService.showSuccess('taxes added Successfully.', '');
    })

 // request.subscribe(() => {
    this.isModalOpen = false;
    this.taxform.reset();
    this.list.get();
    
 // });


  }
}

  


