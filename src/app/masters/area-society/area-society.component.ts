import { ListService, PagedResultDto } from '@abp/ng.core';
import { Confirmation, ConfirmationService } from '@abp/ng.theme.shared';
import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { AreaService } from '@proxy/market-place/area-masters';
import { AreaDto } from '@proxy/market-place/area-masters/dtos';
import { CityService } from '@proxy/market-place/cities';
import { CityDto } from '@proxy/market-place/cities/dtos';
import { NotificationService } from 'src/app/shared/notification.service';

@Component({
  selector: 'app-area-society',
  templateUrl: './area-society.component.html',
  styleUrls: ['./area-society.component.scss'],
  providers: [ListService],
})
export class AreaSocietyComponent implements OnInit {
  Areaform: FormGroup;
  areaList = { items: [], totalCount: 0 } as PagedResultDto<AreaDto>;

  isModalOpen = false; // add this line
  selectedArea = {} as AreaDto;
  cityId : any;
  citylist: any;
  name: any;
  city: CityDto;
  submitted: boolean = false;
  
  
  

  constructor(private fb: FormBuilder ,
             public readonly list: ListService,
              private cityService:CityService, 
              private areaService: AreaService, 
              private confirmation: ConfirmationService,
              private notifyService: NotificationService,
              ) { }

  ngOnInit(): void {
    const areaStreamCreator = (query) => this.areaService.getList(query);

    this.list.hookToQuery(areaStreamCreator).subscribe((response) => {
      this.areaList = response;
    });

    this.cityService.getCity().subscribe((data:any) =>{
      // this.city = data.data
      // console.log(this.city,"kjsjkshd")

      // data.items.forEach(element => {
      //   this.name=this.citylist=element.name;
      // });
    
    })
  }
  
  get f(): { [key: string]: AbstractControl } {
    return this.Areaform.controls;
  }

  changeCity(e) {
    console.log(e.target.value)
    
    // this.stateName.setValue(e.target.value, {
    //   onlySelf: true
    // })
  }

  // add new method
  createArea() {
    this.cityService.getCity().subscribe((data:any) =>{
      this.city = data.data
      // data.items.forEach(element => {
      //   this.city = data.items;    
      // });
    
    })
   this.selectedArea = {} as AreaDto;
   this.buildForm();
   this.isModalOpen = true;
  }

   // Add editBook method
 editArea(id: string) {
    this.cityService.getCity().subscribe((data:any) =>{
    this.city = data.data
    // this.notifyService.showSuccess('Area updated Successfully', '');
    // data.items.forEach(element => {
    //   this.city = data.items; 
    // });
  
  })
  this.areaService.get(id).subscribe((Area) => {
    this.selectedArea = Area;
    this.buildForm();
    this.isModalOpen = true;
  });
 
}

//    // Add a delete method
delete(id: string) {
  this.confirmation.warn('::Are You Sure To Delete', '::AreYouSure').subscribe((status) => {
    if (status === Confirmation.Status.confirm) {
      this.areaService.delete(id).subscribe(() => this.list.get());
      this.notifyService.showSuccess('Area deleted Successfully', '');
    }
  });
}

  // add buildForm method
  buildForm() {
    this.Areaform = this.fb.group({
      areaAddress: [this.selectedArea.areaAddress ||'', Validators.required],
      cityId: [this.selectedArea.cityId ||'', Validators.required],
      
    });
  }


  
  // add save method
  save() {
    this.submitted = true;
    if (this.Areaform.invalid) {
      return;
    }
    
    // this.areaService.create(this.Areaform.value).subscribe(() => {
    //   this.notifyService.showSuccess(' added Successfully.', '');

    //   this.isModalOpen = false;
    //   this.Areaform.reset();
    //   this.list.get();
    // });

    const request = this.selectedArea.id
    ? this.areaService.update(this.selectedArea.id, this.Areaform.value).subscribe(() =>{
      this.notifyService.showSuccess('area updated Successfully.', '');
    })
    : this.areaService.create(this.Areaform.value).subscribe(() => {
      this.notifyService.showSuccess(' area added Successfully.', '');
    })

  //request.subscribe(() => {
    this.isModalOpen = false;
    this.Areaform.reset();
    this.list.get();
    
//});

}
}
