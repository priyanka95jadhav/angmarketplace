import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaSocietyComponent } from './area-society.component';

describe('AreaSocietyComponent', () => {
  let component: AreaSocietyComponent;
  let fixture: ComponentFixture<AreaSocietyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AreaSocietyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaSocietyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
