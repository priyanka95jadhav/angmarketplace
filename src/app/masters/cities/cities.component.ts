import { ListService , PagedResultDto } from '@abp/ng.core';
import { ConfirmationService, Confirmation } from '@abp/ng.theme.shared';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms'; 
import { CityService ,} from '@proxy/market-place/cities';
import { CityDto } from '@proxy/market-place/cities/dtos';
import { StateService } from '@proxy/market-place/states';
import { StateDto } from '@proxy/market-place/states/dtos';
import { NotificationService } from 'src/app/shared/notification.service';


@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.scss'],
  providers: [ListService],
})
export class CitiesComponent implements OnInit {

  cityform: FormGroup; 
  isModalOpen = false; // add this line

  citylist = { items: [], totalCount: 0 } as PagedResultDto<CityDto>;
 
  selectedCity = {} as CityDto;
  stateName: any;
  statelist: any;
  stateId: any;
  state: StateDto;
  submitted: boolean = false;

  

   constructor( private fb: FormBuilder ,
     public readonly list: ListService ,
     private cityService: CityService ,
     private stateService : StateService ,
     private confirmation: ConfirmationService,
     private notifyService: NotificationService,
    ) { }

   ngOnInit(): void {
    const cityStreamCreator = (query) => this.cityService.getList(query);

    this.list.hookToQuery(cityStreamCreator).subscribe((response) => {
      this.citylist = response;
      console.log(response);
    });
    

      this.stateService.getState().subscribe((data:any) =>{
        data.items.forEach(element => {
          this.stateName=this.statelist=element.stateName;
          // this.stateID=this.stateList=element.stateID;
          // console.log(this.stateName,"111111111111")    
        });
      
        
      })
    
   }

   get f(): { [key: string]: AbstractControl } {
    return this.cityform.controls;
  }

    // add buildForm method
  createCity() {
    this.stateService.getState().subscribe((data:any) =>{
      data.items.forEach(element => {
        this.state = data.items;    
        
          
      });
    
    })
   this.selectedCity = {} as CityDto;
   this.buildForm();
   this.isModalOpen = true;
  }

  buildForm() {
    this.cityform = this.fb.group({
      name: [this.selectedCity.name ||'', Validators.required],
      stateId: [this.selectedCity.stateId ||'', Validators.required],
      
    });
  }

   changeState(e) {
    console.log(e.target.value)
    
    // this.stateName.setValue(e.target.value, {
    //   onlySelf: true
    // })
  }
  
   // add save method

  save() {
    this.submitted = true;

    if (this.cityform.invalid) {
      return;
    }
    
    // this.cityService.create(this.cityform.value).subscribe(() => {
    //   this.isModalOpen = false;
    //   this.cityform.reset();
    //   this.list.get();
    // });

    const request = this.selectedCity.id
    ? this.cityService.update(this.selectedCity.id, this.cityform.value).subscribe(() =>{
      this.notifyService.showSuccess('city updated Successfully.', '');
    })
    : this.cityService.create(this.cityform.value).subscribe(() => {
      this.notifyService.showSuccess('city added Successfully.', '');
    })
  

  // request.subscribe(() => {
    this.isModalOpen = false;
    this.cityform.reset();
    this.list.get();
    
  // });

  }

 // Add editBook method
 editCity(id: string) {
  this.stateService.getState().subscribe((data:any) =>{
    data.items.forEach(element => {
      this.state = data.items; 
           
    });
  
  })
  this.cityService.get(id).subscribe((city) => {
    this.selectedCity = city;
    this.buildForm();
    this.isModalOpen = true;
    
    
  });
  
  
}

   // Add a delete method
delete(id: string) {
  this.confirmation.warn('::Are You Sure To Delete', '::AreYouSure').subscribe((status) => {
    if (status === Confirmation.Status.confirm) {
      this.cityService.delete(id).subscribe(() => this.list.get());
      this.notifyService.showSuccess('City deleted Successfully', '');
    }
  });
}

}



