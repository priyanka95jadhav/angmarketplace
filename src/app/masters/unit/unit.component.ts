import { ListService, PagedResultDto } from '@abp/ng.core';
import { Confirmation, ConfirmationService } from '@abp/ng.theme.shared';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UnitMasterService } from '@proxy/market-place/unit-masters';
import { UnitMasterDto } from '@proxy/market-place/unit-masters/dtos';
import { NotificationService } from 'src/app/shared/notification.service';

@Component({
  selector: 'app-unit',
  templateUrl: './unit.component.html',
  styleUrls: ['./unit.component.scss'],
  providers: [ListService],
})
export class UnitComponent implements OnInit {

  unitform: FormGroup; 
  isModalOpen = false; // add this lines

  unitlist = { items: [], totalCount: 0 } as PagedResultDto<UnitMasterDto>;
  
  selectedUnit = {} as UnitMasterDto; // declare selectedBook
  submitted: boolean=false;

  constructor(private fb: FormBuilder , 
    public readonly list: ListService ,
    private unitmasterService: UnitMasterService ,
    private confirmation: ConfirmationService,
    private notifyService: NotificationService,
    ) { }

  ngOnInit(): void {
    const unitStreamCreator = (query) => this.unitmasterService.getList(query);

    this.list.hookToQuery(unitStreamCreator).subscribe((response) => {
      this.unitlist = response;
      console.log(response);
    });
  }

  get f(): { [key: string]: AbstractControl } {
    return this.unitform.controls;
  }

  createUnit() {
    this.selectedUnit = {} as UnitMasterDto; 
    this.buildForm();
    this.isModalOpen = true;
  }

  // Add editBook method
  editUnit(id: string) {
    this.unitmasterService.get(id).subscribe((unit) => {
      this.selectedUnit = unit;
      this.buildForm();
      this.isModalOpen = true;
      
      
    });
  }

  // Add a delete method
delete(id: string) {
  this.confirmation.warn('::Are You Sure To Delete', '::AreYouSure').subscribe((status) => {
    if (status === Confirmation.Status.confirm) {
      this.unitmasterService.delete(id).subscribe(() => this.list.get());
      this.notifyService.showSuccess('unit deleted Successfully', '');

    }
  });
}

  buildForm() {
    this.unitform = this.fb.group({
      unitName: [this.selectedUnit.unitName || '', Validators.required],
      unitCode: [this.selectedUnit.unitCode || '', Validators.required],
    });
  }

  // change the save method
  save() {
    this.submitted = true;
    if (this.unitform.invalid) {
      return;
    }
  console.log(this.selectedUnit.id, 'stateupdate');

    const request = this.selectedUnit.id
      ? this.unitmasterService.update(this.selectedUnit.id, this.unitform.value).subscribe(() =>{
      this.notifyService.showSuccess('unit updated Successfully.', '');
    })
      : this.unitmasterService.create(this.unitform.value).subscribe(() =>{
      this.notifyService.showSuccess('unit added Successfully.', '');
    })

   // request.subscribe(() => {
      this.isModalOpen = false;
      this.unitform.reset();
      this.list.get();
    //});
  }

}

