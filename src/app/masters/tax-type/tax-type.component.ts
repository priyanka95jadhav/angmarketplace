import { ListService, PagedResultDto } from '@abp/ng.core';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TaxTypeService } from '@proxy/market-place/tax-type-masters';
import { TaxTypeDto } from '@proxy/market-place/tax-type-masters/dtos';
import { ConfirmationService, Confirmation } from '@abp/ng.theme.shared';
import { NotificationService } from 'src/app/shared/notification.service';

@Component({
  selector: 'app-tax-type',
  templateUrl: './tax-type.component.html',
  styleUrls: ['./tax-type.component.scss'],
  providers: [ListService],
})
export class TaxTypeComponent implements OnInit {

  taxtypelist = { items: [], totalCount: 0 } as PagedResultDto<TaxTypeDto>;
  taxtypeform: FormGroup; // add this line
  isModalOpen = false; // add this line
  // taxTypeName: any;
  selectedTaxType = {} as TaxTypeDto;
  submitted: boolean = false;
  
  constructor(public readonly list: ListService,
     private taxtypeService: TaxTypeService,
     private fb: FormBuilder,
     private confirmation: ConfirmationService,
     private notifyService: NotificationService,
     ) { }

  ngOnInit(): void {
    const taxtypeStreamCreator = (query) => this.taxtypeService.getList(query);

    this.list.hookToQuery(taxtypeStreamCreator).subscribe((response) => {
      this.taxtypelist = response;
    });
  }

  get f(): { [key: string]: AbstractControl } {
    return this.taxtypeform.controls;
  }

   // add new method
   createTaxtype() {
    this.selectedTaxType = {} as TaxTypeDto;
    this.buildForm();
    this.isModalOpen = true;
  }

  // Add editBook method
  editTaxtype(id: string) {
    this.taxtypeService.get(id).subscribe((taxtype) => {
      this.selectedTaxType = taxtype;
      this.buildForm();
      this.isModalOpen = true;
      
      
    });
  }

  // Add a delete method
delete(id: string) {
  this.confirmation.warn('::Are You Sure To Delete', '::AreYouSure').subscribe((status) => {
    if (status === Confirmation.Status.confirm) {
      this.taxtypeService.delete(id).subscribe(() => this.list.get());
      this.notifyService.showSuccess('tax type deleted Successfully', '');

    }
  });
}

   // add buildForm method
   buildForm() {
    this.taxtypeform = this.fb.group({
      taxTypeName: [this.selectedTaxType.taxTypeName ||'', Validators.required],
      
    });
  }

  // add save method
  // save() {
  //   if (this.taxtypeform.invalid) {
  //     return;
  //   }

  //   console.log(this.taxtypeform.value,'hjhj')

  //   this.taxtypeService.create(this.taxtypeform.value).subscribe(() => {
  //     this.isModalOpen = false;
  //     this.taxtypeform.reset();
  //     this.list.get();
  //   });
  // }

   // change the save method
   save() {
    this.submitted = true;
    if (this.taxtypeform.invalid) {
      return;
    }
  console.log(this.selectedTaxType.id, 'stateupdate');

    const request = this.selectedTaxType.id
      ? this.taxtypeService.update(this.selectedTaxType.id, this.taxtypeform.value).subscribe(() =>{
        this.notifyService.showSuccess('tax type updated Successfully.', '');
      })
      : this.taxtypeService.create(this.taxtypeform.value).subscribe(() => {
      this.notifyService.showSuccess('tax type added Successfully.', '');
    })

   // request.subscribe(() => {
      this.isModalOpen = false;
      this.taxtypeform.reset();
      this.list.get();
    //});
  }
 

  

}
