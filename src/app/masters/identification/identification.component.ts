import { ListService, PagedResultDto } from '@abp/ng.core';
import { Confirmation, ConfirmationService } from '@abp/ng.theme.shared';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IdentificationTypeService } from '@proxy/market-place/identification-type-masters';
import { IdentificationTypeDto } from '@proxy/market-place/identification-type-masters/dtos';
import { NotificationService } from 'src/app/shared/notification.service';

@Component({
  selector: 'app-identification',
  templateUrl: './identification.component.html',
  styleUrls: ['./identification.component.scss'],
  providers: [ListService],
})
export class IdentificationComponent implements OnInit {
  identificationform: FormGroup; 
  isModalOpen = false; // add this line
  identificationlist = { items: [], totalCount: 0 } as PagedResultDto<IdentificationTypeDto>;
  selectedIdentification = {} as IdentificationTypeDto;
  submitted: boolean=false;
  
  constructor(private fb: FormBuilder,
    public readonly list: ListService,
    private identificationtypeService: IdentificationTypeService,
    private confirmation: ConfirmationService,
    private notifyService: NotificationService,
    ) { }

  ngOnInit(): void {
    const identificationStreamCreator = (query) => this.identificationtypeService.getList(query);

    this.list.hookToQuery(identificationStreamCreator).subscribe((response) => {
      this.identificationlist= response;
    });
  }

  
  get f(): { [key: string]: AbstractControl } {
    return this.identificationform.controls;
  }
  
  createidentification(){
    this.selectedIdentification = {} as IdentificationTypeDto;
    this.buildForm();
    this.isModalOpen = true;
  }

   // Add editBook method
   editidentification(id: string) {
    this.identificationtypeService.get(id).subscribe((reason) => {
      this.selectedIdentification = reason;
      this.buildForm();
      this.isModalOpen = true;
      
      
    });
  }

  // Add a delete method
delete(id: string) {
  this.confirmation.warn('::Are You Sure To Delete', '::AreYouSure').subscribe((status) => {
    if (status === Confirmation.Status.confirm) {
      this.identificationtypeService.delete(id).subscribe(() => this.list.get());
      this.notifyService.showSuccess('Identification type deleted Successfully', '');

    }
  });
}

    // add buildForm method
    buildForm() {
      this.identificationform = this.fb.group({
        identificationTypeName: [this.selectedIdentification.identificationTypeName ||'', Validators.required],
        
        
      });
    }

    // add save method
  save() {
    this.submitted = true;
    if (this.identificationform.invalid) {
      return;
    }
    
    // this.cancellationreasonService.create(this.cancelform.value).subscribe(() => {
    //   this.isModalOpen = false;
    //   this.cancelform.reset();
    //   this.list.get();
    // });

    const request = this.selectedIdentification.id
    ? this.identificationtypeService.update(this.selectedIdentification.id, this.identificationform.value).subscribe(() =>{
    this.notifyService.showSuccess('identification type updated Successfully.', '');
    })
    : this.identificationtypeService.create(this.identificationform.value).subscribe(() => {
    this.notifyService.showSuccess('identification type added Successfully.', '');
  })

 // request.subscribe(() => {
    this.isModalOpen = false;
    this.identificationform.reset();
    this.list.get();
    
  //});


  }

}
