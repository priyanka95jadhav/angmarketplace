import { ListService, PagedResultDto } from '@abp/ng.core';
import { Confirmation, ConfirmationService } from '@abp/ng.theme.shared';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StoreTypeService } from '@proxy/market-place/store-type-masters';
import { StoreTypeDto } from '@proxy/market-place/store-type-masters/dtos';
import { NotificationService } from 'src/app/shared/notification.service';

@Component({
  selector: 'app-store-type',
  templateUrl: './store-type.component.html',
  styleUrls: ['./store-type.component.scss'],
  providers: [ListService],
})
export class StoreTypeComponent implements OnInit {
  storetypelist = { items: [], totalCount: 0 } as PagedResultDto<StoreTypeDto>;
  storetypeform: FormGroup; // add this line
  isModalOpen = false; // add this line
  // taxTypeName: any;
  selectedStoreType = {} as StoreTypeDto;
  submitted: boolean = false;
  constructor(public readonly list: ListService,
     private storetypeService: StoreTypeService,
     private fb: FormBuilder,
     private confirmation: ConfirmationService,
     private notifyService: NotificationService,
    ) { }

  ngOnInit(): void {
    const  storetypeStreamCreator = (query) => this.storetypeService.getList(query);

    this.list.hookToQuery( storetypeStreamCreator).subscribe((response) => {
      this.storetypelist = response;
    });
  }

  get f(): { [key: string]: AbstractControl } {
    return this.storetypeform.controls;
  }

   // add new method
   createstoretype() {
    this.selectedStoreType = {} as StoreTypeDto;
    this.buildForm();
    this.isModalOpen = true;
  }

  // Add editBook method
  editstoretype(id: string) {
    this.storetypeService.get(id).subscribe((storetype) => {
      this.selectedStoreType = storetype;
      this.buildForm();
      this.isModalOpen = true;
      
      
    });
  }

  // Add a delete method
delete(id: string) {
  this.confirmation.warn('::Are You Sure To Delete', '::AreYouSure').subscribe((status) => {
    if (status === Confirmation.Status.confirm) {
      this.storetypeService.delete(id).subscribe(() => this.list.get());
      this.notifyService.showSuccess('store type deleted Successfully', '');

    }
  });
}

   // add buildForm method
   buildForm() {
    this.storetypeform = this.fb.group({
      storeTypeName: [this.selectedStoreType.storeTypeName ||'', Validators.required],
      
    });
  }

  // add save method
  // save() {
  //   if (this.taxtypeform.invalid) {
  //     return;
  //   }

  //   console.log(this.taxtypeform.value,'hjhj')

  //   this.taxtypeService.create(this.taxtypeform.value).subscribe(() => {
  //     this.isModalOpen = false;
  //     this.taxtypeform.reset();
  //     this.list.get();
  //   });
  // }

   // change the save method
   save() {
    this.submitted = true;

    if (this.storetypeform.invalid) {
      return;
    }
  console.log(this.selectedStoreType.id, 'stateupdate');

    const request = this.selectedStoreType.id
      ? this.storetypeService.update(this.selectedStoreType.id, this.storetypeform.value).subscribe(() =>{
        this.notifyService.showSuccess('store type updated Successfully.', '');
      })
      : this.storetypeService.create(this.storetypeform.value).subscribe(() => {
      this.notifyService.showSuccess('store type added Successfully.', '');
    })


    // request.subscribe(() => {
      this.isModalOpen = false;
      this.storetypeform.reset();
      this.list.get();
    // });
  }

}
