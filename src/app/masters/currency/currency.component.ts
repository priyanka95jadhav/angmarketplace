import { ListService, PagedResultDto } from '@abp/ng.core';
import { Confirmation, ConfirmationService } from '@abp/ng.theme.shared';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CurrencyService } from '@proxy/market-place/currency-masters';
import { CurrencyDto } from '@proxy/market-place/currency-masters/dtos';
import { NotificationService } from 'src/app/shared/notification.service';

@Component({
  selector: 'app-currency',
  templateUrl: './currency.component.html',
  styleUrls: ['./currency.component.scss'],
  providers: [ListService],
})
export class CurrencyComponent implements OnInit {

  currencyform: FormGroup; 
  isModalOpen = false; // add this lines

  currencylist = { items: [], totalCount: 0 } as PagedResultDto<CurrencyDto>;
  
  selectedCurrency = {} as CurrencyDto; // declare selectedBook
  submitted: boolean = false;


  constructor(private fb: FormBuilder ,
     public readonly list: ListService , 
     private currencyService: CurrencyService ,
     private confirmation: ConfirmationService,
     private notifyService: NotificationService,
     ) { }

  ngOnInit(): void {
    const currencyStreamCreator = (query) => this.currencyService.getList(query);

    this.list.hookToQuery(currencyStreamCreator).subscribe((response) => {
      this.currencylist = response;
      console.log(response);
    });
  }

  get f(): { [key: string]: AbstractControl } {
    return this.currencyform.controls;
  }

  createcurrency() {
    this.selectedCurrency = {} as CurrencyDto; // reset the selected book
    this.buildForm();
    this.isModalOpen = true;
  }

  // Add editBook method
  editCurrency(id: string) {
    this.currencyService.get(id).subscribe((currency) => {
      this.selectedCurrency = currency;
      this.buildForm();
      this.isModalOpen = true;
      
      
    });
  }

  // Add a delete method
delete(id: string) {
  this.confirmation.warn('::Are You Sure To Delete', '::AreYouSure').subscribe((status) => {
    if (status === Confirmation.Status.confirm) {
      this.currencyService.delete(id).subscribe(() => this.list.get());
      this.notifyService.showSuccess('currency deleted Successfully', '');

    }
  });
}

  buildForm() {
    this.currencyform = this.fb.group({
      currencyName: [this.selectedCurrency.currencyName || '', Validators.required],
      currencySymbol: [this.selectedCurrency.currencySymbol || '', Validators.required],
    });
  }

  // change the save method
  save() {
    this.submitted = true;

    if (this.currencyform.invalid) {
      return;
    }
  console.log(this.selectedCurrency.id, 'sss');

    const request = this.selectedCurrency.id
      ? this.currencyService.update(this.selectedCurrency.id, this.currencyform.value).subscribe(() =>{
        this.notifyService.showSuccess('currency updated Successfully.', '');
      })
      : this.currencyService.create(this.currencyform.value).subscribe(() => {
        this.notifyService.showSuccess('currency added Successfully.', '');
      })
    

    //request.subscribe(() => {
      this.isModalOpen = false;
      this.currencyform.reset();
      this.list.get();
    //});

  }

}
