import { Component, OnInit } from '@angular/core';
import { ListService, PagedResultDto } from '@abp/ng.core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConfirmationService, Confirmation } from '@abp/ng.theme.shared';
import { RedeemPointDto } from '@proxy/market-place/redeem-points-masters/dtos';
import { RedeemPointService } from '@proxy/market-place/redeem-points-masters/redeem-point.service';
import { NotificationService } from 'src/app/shared/notification.service';

@Component({
  selector: 'app-redeem-values',
  templateUrl: './redeem-values.component.html',
  styleUrls: ['./redeem-values.component.scss'],
  providers: [ListService],
})
export class RedeemValuesComponent implements OnInit {

  redeemform: FormGroup; 
  isModalOpen = false; // add this lines

  redeemlist = { items: [], totalCount: 0 } as PagedResultDto<RedeemPointDto>;
  
  selectedRedeem = {} as RedeemPointDto; 
  submitted: boolean = false;

  constructor(private fb: FormBuilder , 
    public readonly list: ListService ,
    public redeempointService:RedeemPointService ,
    private confirmation: ConfirmationService,
    private notifyService: NotificationService,
    ) { }

  ngOnInit(): void {
    const redeemStreamCreator = (query) => this.redeempointService.getList(query);

    this.list.hookToQuery(redeemStreamCreator).subscribe((response) => {
      this.redeemlist = response;
      console.log(response);
    });
  }

  get f(): { [key: string]: AbstractControl } {
    return this.redeemform.controls;
  }


  createRedeem() {
    this.selectedRedeem = {} as RedeemPointDto; // reset the selected book
    this.buildForm();
    this.isModalOpen = true;
  }

  buildForm() {
    this.redeemform = this.fb.group({
      rewardPoints: [this.selectedRedeem.rewardPoints ||'', Validators.required],
      pointValue: [this.selectedRedeem.pointValue ||'', Validators.required],
      
    });
  }

  save() {
    this.submitted = true;
    if (this.redeemform.invalid) {
      return;
    }
    
    const request = this.selectedRedeem.id
    ? this.redeempointService.update(this.selectedRedeem.id, this.redeemform.value).subscribe(() =>{
      this.notifyService.showSuccess('redeem values updated Successfully.', '');
    })
    : this.redeempointService.create(this.redeemform.value).subscribe(() => {
      this.notifyService.showSuccess('redeem values added Successfully.', '');
    })
      

 // request.subscribe(() => {
    this.isModalOpen = false;
    this.redeemform.reset();
    this.list.get();
 // });
  }

// Add editBook method
editRedeem(id: string) {
  this.redeempointService.get(id).subscribe((redeem) => {
    this.selectedRedeem = redeem;
    this.buildForm();
    this.isModalOpen = true;
    
    
  });
}

// Add a delete method
delete(id: string) {
this.confirmation.warn('::Are You Sure To Delete', '::AreYouSure').subscribe((status) => {
  if (status === Confirmation.Status.confirm) {
    this.redeempointService.delete(id).subscribe(() => this.list.get());
    this.notifyService.showSuccess('Redeem points deleted Successfully', '');

  }
});
}

}
