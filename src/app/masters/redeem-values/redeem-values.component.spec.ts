import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RedeemValuesComponent } from './redeem-values.component';

describe('RedeemValuesComponent', () => {
  let component: RedeemValuesComponent;
  let fixture: ComponentFixture<RedeemValuesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RedeemValuesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RedeemValuesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
