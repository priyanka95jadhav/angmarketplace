import { Component, OnInit } from '@angular/core';
import { ListService, PagedResultDto } from '@abp/ng.core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { DeliveryBoyNotificationMasterService } from '@proxy/market-place/delivery-boy-notification-masters';
import { DeliveryBoyService } from '@proxy/market-place/delivery-boy-masters';
import { DeliveryBoyDto } from '@proxy/market-place/delivery-boy-masters/dtos';
import { ConfigStateService } from '@abp/ng.core';
import { Router } from '@angular/router';
import { ToasterService } from '@abp/ng.theme.shared';
import { NotificationService } from 'src/app/shared/notification.service';


@Component({
  selector: 'app-send-driver-notifications',
  templateUrl: './send-driver-notifications.component.html',
  styleUrls: ['./send-driver-notifications.component.scss'],
  providers: [ListService],
  
})
export class SendDriverNotificationsComponent implements OnInit {

  drivernotificationform: FormGroup;
  
  deliveryboy:DeliveryBoyDto;
  boyName: any;  
  dropdownList :any;
  deliveryBoyId : string[]=[];
  dropdownSettings = {};
  selectedFiles: any;
  imageFileName:File;
  deliveryboyarray: any = [];
  deliveryboyid:any;
  boyname:any;
  formFile: File;
  boyDetails: any;

  

  constructor(private fb: FormBuilder,
             private deliveryboyService:DeliveryBoyService,
             private deliveryboynotificationMasterService: DeliveryBoyNotificationMasterService,
              private config: ConfigStateService,
              private _route: Router,
              private _toaster: ToasterService,
              private notifyService: NotificationService,
              
              ) { }

  ngOnInit() {

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'deliveryBoyId',
      textField: 'boyName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      // itemsShowLimit: 3,
      allowSearchFilter: true
    };
    
    this.drivernotificationform = this.fb.group({
      deliveryBoyId: ['', Validators.required],
      title: ['', Validators.required],
      message: ['', Validators.required],
      imageFileName: ['', Validators.required],
     
    });

    this.getDeliveryBoy() ;

  }

getDeliveryBoy() {
  this.deliveryboyService.allDeliveryBoy().subscribe((data:any) =>{   
    
     this.boyDetails = data;
     data.items.forEach(element => {    
  
     });
   })

}
imgChange(event) {

  // this.fileName = event.target.files;
  // for (let i = 0; i < event.target.files; i++) {
  //   this.fileName = event.target.files.item(i);
  // }
  const file: File = event.target.files[0];

  if (file) {
    const formData = new FormData();
    this.selectedFiles = event.target.files;
    this.formFile = this.selectedFiles.item(0);

    // console.log(this.fileName);
    formData.append('file', this.formFile);
    formData.forEach((value, key) => {
      console.log(key + ' ' + value, 'hghg');
    });
  }
}

onItemSelect($event) {

  var deliveryboy = this.drivernotificationform.value.deliveryBoyId.length

  console.log($event, 'jkhkh');

  this.deliveryboyarray.push((deliveryboy))


}
onSelectAll(event) {
  var deliveryboy = this.drivernotificationform.value.deliveryBoyId.length;
  for (let i = 0; i < deliveryboy; i++) {
    this.deliveryboyarray = event[i]
   // this.deliveryBoyId.push(event.target.result);
    console.log(event, 'jkhkh');
  }
  // console.log(this.storearray,"vvvvvvvvv===========")
}


getFormData(object) {
  const formData = new FormData();
  Object.keys(object).forEach(key => formData.append(key, object[key]));
  return formData;
}

  save() {

 
    // if (this.deliveryboyaddform.invalid)
    //  {
    //    return;
    //  }
   // console.log(this.deliveryboyaddform.value);
   var deliveryBoy: string[] = [];
   this.drivernotificationform.value.formFile = this.formFile;


   let formData = this.drivernotificationform.value;
   this.drivernotificationform.value['deliveryBoyId']

   formData.deliveryBoyId.forEach(element => {
    deliveryBoy.push(element.deliveryBoyId);
   });
   let postMultipartformData: FormData = this.getFormData(formData);
   postMultipartformData.delete('deliveryBoyId');
   deliveryBoy.forEach(element => {
     postMultipartformData.append('deliveryBoyId', element);
   });
 
  // console.log(this.drivernotificationform.value);

 this.deliveryboynotificationMasterService.create(postMultipartformData).subscribe(() => {
  this.notifyService.showSuccess('Driver notification added Successfully.', '');

  console.log(postMultipartformData,'hhh');
   

    this.drivernotificationform.reset();
    this._route.navigate(["/driver-notifications"]);
   // this._toaster.success("Title","Message");
   //this.list.get();
 });
   }
}
