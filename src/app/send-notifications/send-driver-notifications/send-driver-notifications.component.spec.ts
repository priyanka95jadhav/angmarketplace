import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SendDriverNotificationsComponent } from './send-driver-notifications.component';

describe('SendDriverNotificationsComponent', () => {
  let component: SendDriverNotificationsComponent;
  let fixture: ComponentFixture<SendDriverNotificationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SendDriverNotificationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SendDriverNotificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
