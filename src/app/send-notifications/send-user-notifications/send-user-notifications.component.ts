import { IdentityUserDto, IdentityUserService } from '@abp/ng.identity/proxy';
import { Component, OnInit } from '@angular/core';
import { ListService, PagedResultDto } from '@abp/ng.core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserNotificationMasterService } from '@proxy/market-place/user-notification-masters';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ConfigStateService } from '@abp/ng.core';
import { Router } from '@angular/router';
import { ToasterService } from '@abp/ng.theme.shared';
import { NotificationService } from 'src/app/shared/notification.service';


@Component({
  selector: 'app-send-user-notifications',
  templateUrl: './send-user-notifications.component.html',
  styleUrls: ['./send-user-notifications.component.scss'],
  providers: [ListService],
  
})
export class SendUserNotificationsComponent implements OnInit {
  usernotificationform: FormGroup;

  user:IdentityUserDto;
  userName: any;  
  dropdownList :any;
  userId : string[]=[];
  dropdownSettings = {};
  selectedFiles: any;
  imageFileName:File;
  userarray: any = [];
  userid:any;
  username:any;
  formFile: File;
  userDetails: any;

  constructor(private fb: FormBuilder,
             private identityuserService: IdentityUserService,
             private usernotificationMasterService: UserNotificationMasterService,
             private config: ConfigStateService,
              private _route: Router,
              private _toaster: ToasterService,
              private notifyService: NotificationService,
             ) { }

  ngOnInit(){
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'userId',
      textField: 'userName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      // itemsShowLimit: 3,
      allowSearchFilter: true
    };
    
    this.usernotificationform = this.fb.group({
      userId: ['', Validators.required],
      title: ['', Validators.required],
      message: ['', Validators.required],
      imageFileName: ['', Validators.required],
     
    });

    // this.getUser() ;

  }

  // getUser() {
  //   this.identityuserService.get().subscribe((data:any) =>{   
    
  //     this.userDetails = data;
  //     data.items.forEach(element => {    
   
  //     });
  //   })
  // }

  imgChange(event) {

    // this.fileName = event.target.files;
    // for (let i = 0; i < event.target.files; i++) {
    //   this.fileName = event.target.files.item(i);
    // }
    const file: File = event.target.files[0];
  
    if (file) {
      const formData = new FormData();
      this.selectedFiles = event.target.files;
      this.formFile = this.selectedFiles.item(0);
  
      // console.log(this.fileName);
      formData.append('file', this.formFile);
      formData.forEach((value, key) => {
        console.log(key + ' ' + value, 'hghg');
      });
    }
  }
  
  onItemSelect($event) {
  
    var user = this.usernotificationform.value.userId.length 
    
    console.log($event,'jkhkh');
       
    this.userarray.push((user))
           
   
  }
  onSelectAll(event){
  var user = this.usernotificationform.value.userId.length;
  for (let i = 0; i < user; i++) {         
    this.userarray = event[i]
      // this.storeId.push(event.target.result); 
      console.log(event,'jkhkh');  
  }
  // console.log(this.storearray,"vvvvvvvvv===========")
  }
  
  getFormData(object) {
    const formData = new FormData();
    Object.keys(object).forEach(key => formData.append(key, object[key]));
    return formData;
  }
  
    save() {
  
      // if (this.deliveryboyaddform.invalid)
    //  {
    //    return;
    //  }
   // console.log(this.deliveryboyaddform.value);
   var user: string[] = [];
   this.usernotificationform.value.formFile = this.formFile;


   let formData = this.usernotificationform.value;
   this.usernotificationform.value['userId']

   formData.deliveryBoyId.forEach(element => {
    user.push(element.deliveryBoyId);
   });
   let postMultipartformData: FormData = this.getFormData(formData);
   postMultipartformData.delete('userId');
   user.forEach(element => {
     postMultipartformData.append('userId', element);
   });
 
  // console.log(this.drivernotificationform.value);

 this.usernotificationMasterService.create(postMultipartformData).subscribe(() => {
  this.notifyService.showSuccess('User notification added Successfully.', '');

  console.log(postMultipartformData,'hhh');
   

    this.usernotificationform.reset();
    this._route.navigate(["/user-notifications"]);
    //this._toaster.success("Title","Message");
   //this.list.get();
 });
   }
}
