import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SendUserNotificationsComponent } from './send-user-notifications.component';

describe('SendUserNotificationsComponent', () => {
  let component: SendUserNotificationsComponent;
  let fixture: ComponentFixture<SendUserNotificationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SendUserNotificationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SendUserNotificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
