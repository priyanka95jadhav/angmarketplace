import { Component, OnInit } from '@angular/core';
import { ListService, PagedResultDto } from '@abp/ng.core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {IDropdownSettings } from 'ng-multiselect-dropdown';
import { StoreMasterService } from '@proxy/market-place/store-masters';
import { StoreMasterDto } from '@proxy/market-place/store-masters/dtos';
import { StoreNotificationMasterService } from '@proxy/market-place/store-notification-masters';
import { ConfigStateService } from '@abp/ng.core';
import { Router } from '@angular/router';
import { ToasterService } from '@abp/ng.theme.shared';
import { NotificationService } from 'src/app/shared/notification.service';

@Component({
  selector: 'app-send-store-notifications',
  templateUrl: './send-store-notifications.component.html',
  styleUrls: ['./send-store-notifications.component.scss'],
  providers: [ListService],
})
export class SendStoreNotificationsComponent implements OnInit {

  storenotificationform: FormGroup;
  
  store:StoreMasterDto;
  storeName: any;  
  // storeName = [];
  dropdownList :any;
  storeId : string[]=[];
  dropdownSettings = {};
  selectedFiles: any;
  imageFileName:File;
  storearray: any = [];
  storeid:any;
  storename:any;
  formFile: File;
 
  constructor(private fb: FormBuilder,
              private storeMasterService: StoreMasterService,
              private storenotificationMasterService: StoreNotificationMasterService,
              private config: ConfigStateService,
              private _route: Router,
              private _toaster: ToasterService,
              private notifyService: NotificationService,
              
              ) { }

  ngOnInit() {

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'storeId',
      textField: 'storeName',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      // itemsShowLimit: 3,
      allowSearchFilter: true
    };
    
    
    this.storenotificationform = this.fb.group({
      storeId: ['', Validators.required],
      title: ['', Validators.required],
      message: ['', Validators.required],
      imageFileName: ['', Validators.required],
     
    });

    this.getStore() ;

  }

getStore() {
  this.storeMasterService.allStores().subscribe((data:any) =>{
    
    this.storeName = data;
     data.forEach(element => {    
      
     });
  })

}

imgChange(event) {

  // this.fileName = event.target.files;
  // for (let i = 0; i < event.target.files; i++) {
  //   this.fileName = event.target.files.item(i);
  // }
  const file: File = event.target.files[0];

  if (file) {
    const formData = new FormData();
    this.selectedFiles = event.target.files;
    this.formFile = this.selectedFiles.item(0);

    // console.log(this.fileName);
    formData.append('file', this.formFile);
    formData.forEach((value, key) => {
      console.log(key + ' ' + value, 'hghg');
    });
  }
}

onItemSelect($event) {

  var store = this.storenotificationform.value.storeId.length

  console.log($event, 'jkhkh');

  this.storearray.push((store))


}
onSelectAll(event) {
  var store = this.storenotificationform.value.storeId.length;
  for (let i = 0; i < store; i++) {
    this.storearray = event[i]
    // this.storeId.push(event.target.result);
    console.log(event, 'jkhkh');
  }
  // console.log(this.storearray,"vvvvvvvvv===========")
}


getFormData(object) {
  const formData = new FormData();
  Object.keys(object).forEach(key => formData.append(key, object[key]));
  return formData;
}

  save() {

 
    // if (this.deliveryboyaddform.invalid)
    //  {
    //    return;
    //  }
   // console.log(this.deliveryboyaddform.value);
   var stors: string[] = [];
   this.storenotificationform.value.formFile = this.formFile;


   let formData = this.storenotificationform.value;
   this.storenotificationform.value['storeId']

   formData.storeId.forEach(element => {
     stors.push(element.storeId);
   });
   let postMultipartformData: FormData = this.getFormData(formData);
   postMultipartformData.delete('storeId');
   stors.forEach(element => {
     postMultipartformData.append('storeId', element);
   });
 
 

 this.storenotificationMasterService.create(postMultipartformData).subscribe(() => {
  this.notifyService.showSuccess('Store notification added Successfully.', '');

  console.log(postMultipartformData,'hhh');
   
    this.storenotificationform.reset();
    this._route.navigate(["/store-notifications"]);
   // this._toaster.success("Title","Message");
   //this.list.get();
 });
   }

}