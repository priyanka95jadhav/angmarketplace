import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SendStoreNotificationsComponent } from './send-store-notifications.component';

describe('SendStoreNotificationsComponent', () => {
  let component: SendStoreNotificationsComponent;
  let fixture: ComponentFixture<SendStoreNotificationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SendStoreNotificationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SendStoreNotificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
