import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SendDriverNotificationsComponent } from './send-driver-notifications/send-driver-notifications.component';
import { SendNotificationsComponent } from './send-notifications.component';
import { SendStoreNotificationsComponent } from './send-store-notifications/send-store-notifications.component';
import { SendUserNotificationsComponent } from './send-user-notifications/send-user-notifications.component';

const routes: Routes = [
  { path: '', component: SendNotificationsComponent},
  { 
    path: 'send/user-notification', component: SendUserNotificationsComponent,
  },
  { 
    path: 'send/store-notification', component: SendStoreNotificationsComponent,
  },
  { 
    path: 'send/driver-notification', component: SendDriverNotificationsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SendNotificationsRoutingModule { }
