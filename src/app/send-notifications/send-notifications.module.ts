import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SendNotificationsRoutingModule } from './send-notifications-routing.module';
import { SendNotificationsComponent } from './send-notifications.component';
import { SendUserNotificationsComponent } from './send-user-notifications/send-user-notifications.component';
import { SendStoreNotificationsComponent } from './send-store-notifications/send-store-notifications.component';
import { SendDriverNotificationsComponent } from './send-driver-notifications/send-driver-notifications.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { MastersModule } from '../masters/masters.module';
import { MastersRoutingModule } from '../masters/masters-routing.module';



@NgModule({
  declarations: [
    SendNotificationsComponent,
    SendUserNotificationsComponent,
    SendStoreNotificationsComponent,
    SendDriverNotificationsComponent
  ],
  imports: [
    CommonModule,
    SendNotificationsRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    NgMultiSelectDropDownModule.forRoot(),
    MastersRoutingModule,
    MastersModule,
    
 
  ]
})
export class SendNotificationsModule { }
