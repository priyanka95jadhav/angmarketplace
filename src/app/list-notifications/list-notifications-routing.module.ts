import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DriverNotificationsComponent } from './driver-notifications/driver-notifications.component';
import { ListNotificationsComponent } from './list-notifications.component';
import { StoreNotificationsComponent } from './store-notifications/store-notifications.component';
import { UserNotificationsComponent } from './user-notifications/user-notifications.component';

const routes: Routes = [
  { path: '', component: ListNotificationsComponent},
  { path: 'user-notifications', component: UserNotificationsComponent},
  { path: 'store-notifications', component: StoreNotificationsComponent},
  { path: 'driver-notifications', component: DriverNotificationsComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListNotificationsRoutingModule { }
