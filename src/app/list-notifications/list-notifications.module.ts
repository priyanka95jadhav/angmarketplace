import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListNotificationsRoutingModule } from './list-notifications-routing.module';
import { ListNotificationsComponent } from './list-notifications.component';
import { StoreNotificationsComponent } from './store-notifications/store-notifications.component';
import { DriverNotificationsComponent } from './driver-notifications/driver-notifications.component';
import { UserNotificationsComponent } from './user-notifications/user-notifications.component';
import { SendNotificationsModule } from '../send-notifications/send-notifications.module';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    ListNotificationsComponent,
    StoreNotificationsComponent,
    DriverNotificationsComponent,
    UserNotificationsComponent
  ],
  imports: [
    CommonModule,
    ListNotificationsRoutingModule,
    SendNotificationsModule,
    SharedModule,
  ]
})
export class ListNotificationsModule { }
