import { Component, OnInit } from '@angular/core';
import { ListService, PagedResultDto } from '@abp/ng.core';
import { UserNotificationMasterService } from '@proxy/market-place/user-notification-masters';
import { UserNotificationMasterDto } from '@proxy/market-place/user-notification-masters/dtos';
import { Confirmation, ConfirmationService } from '@abp/ng.theme.shared';
import { NotificationService } from 'src/app/shared/notification.service';

@Component({
  selector: 'app-user-notifications',
  templateUrl: './user-notifications.component.html',
  styleUrls: ['./user-notifications.component.scss'],
  providers: [ListService],
})
export class UserNotificationsComponent implements OnInit {

  usernotificationlist = { items: [], totalCount: 0 } as PagedResultDto<UserNotificationMasterDto>;


  constructor(public readonly list: ListService ,
              private usernotificationMasterService: UserNotificationMasterService,
              private confirmation: ConfirmationService,
              private notifyService:NotificationService
              ) { }

  ngOnInit(): void {
    const usernotificationStreamCreator = (query) => this.usernotificationMasterService.getList(query);

    this.list.hookToQuery(usernotificationStreamCreator).subscribe((response) => {
      this.usernotificationlist = response;
      console.log(response);
    });
  }

 // Add a delete method
 delete(id: string) {
  this.confirmation.warn('::Are You Sure To Delete', '::AreYouSure').subscribe((status) => {
    if (status === Confirmation.Status.confirm) {
      this.usernotificationMasterService.delete(id).subscribe(() =>{
        this.notifyService.showSuccess(' Deleted Successfully.', '');
        this.list.get()
      });
    }
  });
}

}
