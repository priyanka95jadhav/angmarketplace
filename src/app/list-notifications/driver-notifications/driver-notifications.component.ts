import { Component, OnInit } from '@angular/core';
import { ListService, PagedResultDto } from '@abp/ng.core';
import { DeliveryBoyNotificationMasterService } from '@proxy/market-place/delivery-boy-notification-masters';
import { DeliveryBoyNotificationMasterDto } from '@proxy/market-place/delivery-boy-notification-masters/dtos';
import { NotificationService } from 'src/app/shared/notification.service';
import { Confirmation, ConfirmationService } from '@abp/ng.theme.shared';

@Component({
  selector: 'app-driver-notifications',
  templateUrl: './driver-notifications.component.html',
  styleUrls: ['./driver-notifications.component.scss'],
  providers: [ListService],
})
export class DriverNotificationsComponent implements OnInit {

  deliveryboynotificationlist = { items: [], totalCount: 0 } as PagedResultDto<DeliveryBoyNotificationMasterDto>;

  constructor(public readonly list: ListService ,
              private deliveryboynotificationMasterService: DeliveryBoyNotificationMasterService,
              private confirmation: ConfirmationService,
              private notifyService:NotificationService
              ) { }

  ngOnInit(): void {
    const deliveryboyStreamCreator = (query) => this.deliveryboynotificationMasterService.getList(query);

    this.list.hookToQuery(deliveryboyStreamCreator).subscribe((response) => {
      this.deliveryboynotificationlist = response;
      console.log(response);
    });
  }

  // Add a delete method
delete(id: string) {
  this.confirmation.warn('::Are You Sure To Delete', '::AreYouSure').subscribe((status) => {
    if (status === Confirmation.Status.confirm) {
      this.deliveryboynotificationMasterService.delete(id).subscribe(() =>{
        this.notifyService.showSuccess(' Deleted Successfully.', '');
        this.list.get()
      });
    }
  });
}
  

}
