import { Component, OnInit } from '@angular/core';
import { ListService, PagedResultDto } from '@abp/ng.core';
import { FormGroup } from '@angular/forms';
import { StoreNotificationMasterService } from '@proxy/market-place/store-notification-masters';
import { StoreNotificationMasterDto } from '@proxy/market-place/store-notification-masters/dtos';
import { NotificationService } from 'src/app/shared/notification.service';
import { Confirmation, ConfirmationService } from '@abp/ng.theme.shared';

@Component({
  selector: 'app-store-notifications',
  templateUrl: './store-notifications.component.html',
  styleUrls: ['./store-notifications.component.scss'],
  providers: [ListService],
})
export class StoreNotificationsComponent implements OnInit {

  // storenotificationform: FormGroup; 

  storenotificationlist = { items: [], totalCount: 0 } as PagedResultDto<StoreNotificationMasterDto>;
  


  constructor(public readonly list: ListService ,
              private storenotificationMasterService: StoreNotificationMasterService, 
              private confirmation: ConfirmationService,
              private notifyService:NotificationService
              ) { }

  ngOnInit(): void {
    const storenotificationStreamCreator = (query) => this.storenotificationMasterService.getList(query);

    this.list.hookToQuery(storenotificationStreamCreator).subscribe((response) => {
      this.storenotificationlist = response;
      console.log(response);
    });
  }

 // Add a delete method
delete(id: string) {
  this.confirmation.warn('::Are You Sure To Delete', '::AreYouSure').subscribe((status) => {
    if (status === Confirmation.Status.confirm) {
      this.storenotificationMasterService.delete(id).subscribe(() =>{
        this.notifyService.showSuccess(' Deleted Successfully.', '');
        this.list.get()
      });
    }
  });
}

}
