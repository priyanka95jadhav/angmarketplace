import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SettingsAddComponent } from './settings-add/settings-add.component';

const routes: Routes = [
  { path: '' , component : SettingsAddComponent},
  // { path: ':id', component: CategoryAddComponent }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
