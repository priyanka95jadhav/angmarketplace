import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { SettingMasterService } from '@proxy/market-place/setting-masters';
import { NotificationService } from 'src/app/shared/notification.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-settings-add',
  templateUrl: './settings-add.component.html',
  styleUrls: ['./settings-add.component.scss'],
})
export class SettingsAddComponent implements OnInit {
  settingAddForm: FormGroup;
  submitted= false;
  logo: string[] = [];
  logoImg: File[] = [];
  favicon: string[] = [];
  faviconImg: File[] = [];
  id:any
  responseId: any;
  apiURL = "https://marketplace.projectnimbus.co.in/"
  serverFavicon: any;
  serverLogo: any;
  constructor(
    private fb: FormBuilder,
    private notifyService: NotificationService,
    private settingService: SettingMasterService
  ) {}

  ngOnInit(): void {
      this.getSettingsData()
    this.settingAddForm = this.fb.group({
      appName: [''],
      countryCode: ['', Validators.required],
      phoneNumberLength: ['', Validators.required],
      siteLogoFile: ['', ],
      faviconFile: ['',],
      keywordsFooterText: [''],
      androidAppLink: [''],
      iosLink: [''],
    });
  }
  get f(): { [key: string]: AbstractControl } {
    return this.settingAddForm.controls;
  }

  LogoFile(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        var reader = new FileReader();

        reader.onload = (event: any) => {
          // console.log(event.target.result);
          this.logo.push(event.target.result);
        }

        reader.readAsDataURL(event.target.files[i]);
      }
    }

    for (let index = 0; index < event.target.files.length; index++) {
      this.logoImg.push(event.target.files[index]);
    }
  }

  faviconFile(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        var reader = new FileReader();

        reader.onload = (event: any) => {
          // console.log(event.target.result);
          this.favicon.push(event.target.result);
        }

        reader.readAsDataURL(event.target.files[i]);
      }
    }

    for (let index = 0; index < event.target.files.length; index++) {
      this.faviconImg.push(event.target.files[index]);
    }
  }

  

  getFormData(object) {
    const formData = new FormData();
    Object.keys(object).forEach(key => formData.append(key, object[key]));
    return formData;
  }

  getSettingsData(){
  this.settingService.getSettingDetails().subscribe(res=>{
    this.id = res
    this.responseId = this.id.id 
    this.serverFavicon = this.id.faviconPath
    this.serverLogo = this.id.siteLogoPath 
    // console.log(this.id)

    this.settingService.get(this.responseId)
    .pipe()
    .subscribe(x => this.settingAddForm.patchValue(x)
    );

  })
}

  submit() {
    this.submitted = true;

    if (this.settingAddForm.invalid) {
      return;
    }
    let postMultipartformData: FormData = this.getFormData(this.settingAddForm.value);

    this.logoImg.forEach(element => {
      postMultipartformData.append('siteLogoFile', element);
    });

    this.faviconImg.forEach(element => {
      postMultipartformData.append('faviconFile', element);
    });


    postMultipartformData.forEach(element => {
    })


    this.settingService.update(this.responseId,postMultipartformData)
        .pipe()
        .subscribe({
          next: e => {
            this.notifyService.showSuccess('Details added Successfully.', '');
            this.submitted = false;
            this.getSettingsData();
            // console.log(e,"response")
          },
          error: error => {
            this.notifyService.showError('Your request is not valid.', '');
            // console.log(error.message, 'error');
          },
        });

  }



}


