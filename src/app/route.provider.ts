import { RoutesService, eLayoutType } from '@abp/ng.core';
import { APP_INITIALIZER } from '@angular/core';

export const APP_ROUTE_PROVIDER = [
  { provide: APP_INITIALIZER, useFactory: configureRoutes, deps: [RoutesService], multi: true },
];

function configureRoutes(routesService: RoutesService) {
  return () => {
    routesService.add([
      {
        path: '/',
        name: '::Menu:Home',
        iconClass: 'fas fa-home',
        order: 1,
        layout: eLayoutType.application,
      },
      {
        path: '/masters',
        name: 'Masters',
        iconClass: 'fas fa-cubes',
        order: 2,
        layout: eLayoutType.application,
      },
      
      {
        path: '/unit',
        name: 'Unit',
        parentName: 'Masters',
        layout: eLayoutType.application,
      },
      {
        path: '/cities',
        name: 'Cities',
        parentName: 'Masters',
        layout: eLayoutType.application,
      },
      {
        path: '/state',
        name: 'State',
        parentName: 'Masters',
        layout: eLayoutType.application,
      },
      // {
      //   path: '/rewards',
      //   name: 'Rewards',
      //   parentName: 'Masters',
      //   layout: eLayoutType.application,
      // },
      {
        path: '/redeem-values',
        name: 'Redeem Points',
        parentName: 'Masters',
        layout: eLayoutType.application,
      },
      {
        path: '/order-status',
        name: 'Order Status',
        parentName: 'Masters',
        layout: eLayoutType.application,
      },
      {
        path: '/product-type',
        name: 'Product Type',
        parentName: 'Masters',
        layout: eLayoutType.application,
      },
      {
        path: '/product-status',
        name: 'Product Status',
        parentName: 'Masters',
        layout: eLayoutType.application,
      },
      {
        path: '/store-type',
        name: 'Store Type',
        parentName: 'Masters',
        layout: eLayoutType.application,
      },
      {
        path: '/taxes',
        name: 'Taxes',
        parentName: 'Masters',
        layout: eLayoutType.application,
      },
      {
        path: '/tax-type',
        name: 'Tax Type',
        parentName: 'Masters',
        layout: eLayoutType.application,
      },
      {
        path: '/identification',
        name: 'Identification',
        parentName: 'Masters',
        layout: eLayoutType.application,
      },
      {
        path: '/currency',
        name: 'Currency',
        parentName: 'Masters',
        layout: eLayoutType.application,
      },
      {
        path: '/cancelling-reasons',
        name: 'Cancelling Reasons',
        parentName: 'Masters',
        layout: eLayoutType.application,
      },
      {
        path: '/area-society',
        name: 'Area/Society',
        parentName: 'Masters',
        layout: eLayoutType.application,
      },
      {
        path: '/time-slot',
        name: 'Time Slot',
        parentName: 'Masters',
        layout: eLayoutType.application,
      },
      
      {
        path: '/store',
        name: 'Store',
        iconClass: 'fas fa-store',
        order: 3,
        layout: eLayoutType.application,
      },
      // {
      //   path: '/storedocument',
      //   name: 'Store Documentation ',
      //   parentName: 'Store',
      //   layout: eLayoutType.application,
      // },
      // {
      //   path: '/storeearning',
      //   name: 'Store Earning/Payments  ',
      //   parentName: 'Store',
      //   layout: eLayoutType.application,
      // },
      // {
      //   path: '/storeimage',
      //   name: 'Store Images  ',
      //   parentName: 'Store',
      //   layout: eLayoutType.application,
      // },
      {
        path: '/store',
        name: 'Store List ',
        parentName: 'Store',
        layout: eLayoutType.application,
      },
      // {
      //   path: '/storeadd',
      //   name: 'Store Add',
      //   parentName: 'Store',
      //   layout: eLayoutType.application,
      // },
      // {
      //   path: '/storepayoutrequest',
      //   name: 'Store Payout Request ',
      //   parentName: 'Store',
      //   layout: eLayoutType.application,
      // },
      // {
      //   path: '/storepayoutvalidation',
      //   name: 'Store Payout Validation',
      //   parentName: 'Store',
      //   layout: eLayoutType.application,
      // },


      {
        path: '/category',
        name: 'Category',
        iconClass: 'fas fa-list-alt',
        order: 4,
        layout: eLayoutType.application,
      },

      {
        path: '/delivery-boy',
        name: 'Delivery Boy',
        iconClass: 'fas fa-shipping-fast',
        order: 5,
        layout: eLayoutType.application,
      },
      {
        path: '/deliveryboy',
        name: 'Delivery Boy List',
        parentName: 'Delivery Boy',
        layout: eLayoutType.application,
      },
      {
        path: '/deliveryboy-incentive',
        name: 'Delivery Boy Incentive',
        parentName: 'Delivery Boy',
        layout: eLayoutType.application,
      },

      {
        path: '/SendNotifications',
        name: 'Send Notifications',
        iconClass: 'fas fa-bell',
        order: 6,
        layout: eLayoutType.application,
      },
      {
        path: '/send/user-notification',
        name: 'Send Notification to Users',
        parentName: 'Send Notifications',
        layout: eLayoutType.application,
      },
      {
        path: '/send/store-notification',
        name: 'Send Notification to Store',
        parentName: 'Send Notifications',
        layout: eLayoutType.application,
      },
      {
        path: '/send/driver-notification',
        name: 'Send Notification to Driver',
        parentName: 'Send Notifications',
        layout: eLayoutType.application,
      },

      
      {
        path: '/ListNotifications',
        name: 'List Notifications',
        iconClass: 'fas fa-bell',
        order: 7,
        layout: eLayoutType.application,
      },
      {
        path: '/user-notifications',
        name: 'User Notifications',
        parentName: 'List Notifications',
        layout: eLayoutType.application,
      },
      {
        path: '/store-notifications',
        name: 'Store Notifications',
        parentName: 'List Notifications',
        layout: eLayoutType.application,
      },
      {
        path: '/driver-notifications',
        name: 'Driver Notifications',
        parentName: 'List Notifications',
        layout: eLayoutType.application,
      },

      {
        path: '/product',
        name: 'Product',
        iconClass: 'fas fa-list-alt',
        order: 8,
        layout: eLayoutType.application,
      },
      {
        path: '/product-list',
        name: 'Product List',
        parentName: 'Product',
        layout: eLayoutType.application,
      },
      {
        path: '/product-attribute',
        name: 'Product Attribute',
        parentName: 'Product',
        layout: eLayoutType.application,
      },

      {
        path: '/ordermanagement',
        name: 'Order Management',
        iconClass: 'fas fa-list',
        order: 9,
        layout: eLayoutType.application,
      },
      {
        path: '/all-orders',
        name: 'All Order',
        parentName: 'Order Management',
        layout: eLayoutType.application,
      },

      {
        path: '/feedBack',
        name: 'Feedback',
        iconClass: 'fas fa-solid fa-comments',
        order: 10,
        layout: eLayoutType.application,
      },
      {
        path: '/user-feedback',
        name: 'User Feedback',
        parentName: 'Feedback',
        layout: eLayoutType.application,
      },
      {
        path: '/store-feedback',
        name: 'Store Feedback',
        parentName: 'Feedback',
        layout: eLayoutType.application,
      },
      {
        path: '/deliveryboy-feedback',
        name: 'Deliveryboy Feedback',
        parentName: 'Feedback',
        layout: eLayoutType.application,
      },

      {
        path: '/settings',
        name: 'Settings',
        iconClass: 'fas fa-list-alt',
        order: 11,
        layout: eLayoutType.application,
      },

    ]);
    
  };
}
