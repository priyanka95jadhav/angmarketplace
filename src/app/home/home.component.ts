import { AuthService } from '@abp/ng.core';
import { Component } from '@angular/core';
import { SettingMasterService } from '@proxy/market-place/setting-masters';
import { OAuthService } from 'angular-oauth2-oidc';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
  dashboardCounts: any;
  get hasLoggedIn(): boolean {
    return this.oAuthService.hasValidAccessToken();
  }

  constructor(private oAuthService: OAuthService, private authService: AuthService,private settingService: SettingMasterService) {}

  ngOnInit(): void {
    this.dashboard()
}

  login() {
    this.authService.navigateToLogin();
  }

  dashboard(){
    this.settingService.getDashBoardData().subscribe(res=>{
      this.dashboardCounts = res
      console.log(this.dashboardCounts);  
    })
  }

}
