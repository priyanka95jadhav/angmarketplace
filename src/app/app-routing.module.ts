import { AuthGuard } from '@abp/ng.core';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeliveryBoyIncentiveComponent } from './delivery-boy/delivery-boy-incentive/delivery-boy-incentive.component';
import { DeliveryBoyListComponent } from './delivery-boy/delivery-boy-list/delivery-boy-list.component';
import { DriverNotificationsComponent } from './list-notifications/driver-notifications/driver-notifications.component';
import { StoreNotificationsComponent } from './list-notifications/store-notifications/store-notifications.component';
import { UserNotificationsComponent } from './list-notifications/user-notifications/user-notifications.component';
import { AreaSocietyComponent } from './masters/area-society/area-society.component';
import { CancellingReasonsComponent } from './masters/cancelling-reasons/cancelling-reasons.component';
import { CitiesComponent } from './masters/cities/cities.component';
import { CurrencyComponent } from './masters/currency/currency.component';
import { IdentificationComponent } from './masters/identification/identification.component';
import { OrderStatusComponent } from './masters/order-status/order-status.component';
import { ProductStatusComponent } from './masters/product-status/product-status.component';
import { ProductTypeComponent } from './masters/product-type/product-type.component';
import { RedeemValuesComponent } from './masters/redeem-values/redeem-values.component';
import { RewardsComponent } from './masters/rewards/rewards.component';
import { StateComponent } from './masters/state/state.component';
import { StoreTypeComponent } from './masters/store-type/store-type.component';
import { TaxTypeComponent } from './masters/tax-type/tax-type.component';
import { TaxesComponent } from './masters/taxes/taxes.component';
import { UnitComponent } from './masters/unit/unit.component';
import { SendDriverNotificationsComponent } from './send-notifications/send-driver-notifications/send-driver-notifications.component';
import { SendStoreNotificationsComponent } from './send-notifications/send-store-notifications/send-store-notifications.component';
import { SendUserNotificationsComponent } from './send-notifications/send-user-notifications/send-user-notifications.component';
import { StoreAddComponent } from './store/store-add/store-add.component';
import { StoreDocumentMasterComponent } from './store/store-document-master/store-document-master.component';
import { StoreEarningComponent } from './store/store-earning/store-earning.component';
import { StoreImageComponent } from './store/store-image/store-image.component';
import { StoreListComponent } from './store/store-list/store-list.component';
import { StorePayoutRequestComponent } from './store/store-payout-request/store-payout-request.component';
import { StorePayoutValidationComponent } from './store/store-payout-validation/store-payout-validation.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    canActivate: [AuthGuard],
    loadChildren: () => import('./home/home.module').then(m => m.HomeModule),
  },
  {
    path: 'account',
    loadChildren: () => import('@abp/ng.account').then(m => m.AccountModule.forLazy()),
  },
  {
    path: 'identity',
    canActivate: [AuthGuard],
    loadChildren: () => import('@abp/ng.identity').then(m => m.IdentityModule.forLazy()),
  },
  {
    path: 'tenant-management',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('@abp/ng.tenant-management').then(m => m.TenantManagementModule.forLazy()),
  },
  {
    path: 'setting-management',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('@abp/ng.setting-management').then(m => m.SettingManagementModule.forLazy()),
  },
  {
    path: '',
    canActivate: [AuthGuard],
    // pathMatch: 'full',
    loadChildren: () => import('./masters/masters.module').then(m => m.MastersModule),
  },
  
  {
   path: '',
   canActivate: [AuthGuard],
   loadChildren: () => import('./store/store.module').then(m => m.StoreModule)
  },
  
  { path: '',
  canActivate: [AuthGuard],
   loadChildren: () => import('./delivery-boy/delivery-boy.module').then(m => m.DeliveryBoyModule)
  },
  

  { 
    path: '', 
    canActivate: [AuthGuard],
    loadChildren: () => import('./send-notifications/send-notifications.module').then(m => m.SendNotificationsModule)
  },
  

  { 
    path: '', 
    canActivate: [AuthGuard],
    loadChildren: () => import('./list-notifications/list-notifications.module').then(m => m.ListNotificationsModule)
  },
  
  { path: 'category', 
  canActivate: [AuthGuard],
  loadChildren: () => import('./category/category.module').then(m => m.CategoryModule)
  },
  { path: '', 
  canActivate: [AuthGuard],
  loadChildren: () => import('./product/product.module').then(m => m.ProductModule)
  },
  { path: '',
   canActivate: [AuthGuard],
   loadChildren: () => import('./feedback/feedback.module').then(m => m.FeedbackModule)
  },
  { path: '',
   canActivate: [AuthGuard],
   loadChildren: () => import('./order-management/order-management.module').then(m => m.OrderManagementModule)
  },
  { path: 'settings',
   canActivate: [AuthGuard],
   loadChildren: () => import('./settings/settings.module').then(m => m.SettingsModule)
  },
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
