import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryBoyFeedbackComponent } from './delivery-boy-feedback.component';

describe('DeliveryBoyFeedbackComponent', () => {
  let component: DeliveryBoyFeedbackComponent;
  let fixture: ComponentFixture<DeliveryBoyFeedbackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeliveryBoyFeedbackComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryBoyFeedbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
