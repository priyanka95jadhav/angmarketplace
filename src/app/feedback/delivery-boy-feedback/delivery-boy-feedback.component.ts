import { Component, OnInit } from '@angular/core';
import { ListService, PagedResultDto } from '@abp/ng.core';
import { FeedbackService } from '@proxy/market-place/feedback-masters';
import { FeedbackDto } from '@proxy/market-place/feedback-masters/dtos';


@Component({
  selector: 'app-delivery-boy-feedback',
  templateUrl: './delivery-boy-feedback.component.html',
  styleUrls: ['./delivery-boy-feedback.component.scss'],
  providers: [ListService],
})
export class DeliveryBoyFeedbackComponent implements OnInit {

  deliveryboyfeedbacklist = { items: [], totalCount: 0 } as PagedResultDto<FeedbackDto>;


  constructor(public readonly list: ListService ,
              private feedbackService: FeedbackService,
             
              ) { }

  ngOnInit(): void {
    const deliveryboyfeedbackStreamCreator = (query) => this.feedbackService.getList(query);

    this.list.hookToQuery(deliveryboyfeedbackStreamCreator).subscribe((response) => {
      this.deliveryboyfeedbacklist = response;
      console.log(response);
    });
  }

}
