import { Component, OnInit } from '@angular/core';
import { ListService, PagedResultDto } from '@abp/ng.core';
import { FeedbackService } from '@proxy/market-place/feedback-masters';
import { FeedbackDto } from '@proxy/market-place/feedback-masters/dtos';

@Component({
  selector: 'app-store-feedback',
  templateUrl: './store-feedback.component.html',
  styleUrls: ['./store-feedback.component.scss'],
  providers: [ListService],
})
export class StoreFeedbackComponent implements OnInit {
  storefeedbacklist = { items: [], totalCount: 0 } as PagedResultDto<FeedbackDto>;


  constructor(public readonly list: ListService ,
              private feedbackService: FeedbackService,
             
              ) { }

  ngOnInit(): void {
    const storefeedbackStreamCreator = (query) => this.feedbackService.getList(query);

    this.list.hookToQuery(storefeedbackStreamCreator).subscribe((response) => {
      this.storefeedbacklist = response;
      console.log(response);
    });
  }

}
