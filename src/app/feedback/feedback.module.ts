import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FeedbackRoutingModule } from './feedback-routing.module';
import { FeedbackComponent } from './feedback.component';
import { UsersFeedbackComponent } from './users-feedback/users-feedback.component';
import { StoreFeedbackComponent } from './store-feedback/store-feedback.component';
import { DeliveryBoyFeedbackComponent } from './delivery-boy-feedback/delivery-boy-feedback.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    FeedbackComponent,
    UsersFeedbackComponent,
    StoreFeedbackComponent,
    DeliveryBoyFeedbackComponent
  ],
  imports: [
    CommonModule,
    FeedbackRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class FeedbackModule { }
