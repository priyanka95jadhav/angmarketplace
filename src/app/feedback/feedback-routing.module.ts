import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeliveryBoyFeedbackComponent } from './delivery-boy-feedback/delivery-boy-feedback.component';
import { FeedbackComponent } from './feedback.component';
import { StoreFeedbackComponent } from './store-feedback/store-feedback.component';
import { UsersFeedbackComponent } from './users-feedback/users-feedback.component';

const routes: Routes = [
  { path: '', component: FeedbackComponent },
  { path: 'user-feedback', component: UsersFeedbackComponent },
  { path: 'store-feedback', component:StoreFeedbackComponent },
  { path: 'deliveryboy-feedback', component: DeliveryBoyFeedbackComponent },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeedbackRoutingModule { }
