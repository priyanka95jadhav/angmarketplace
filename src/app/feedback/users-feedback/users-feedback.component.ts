import { Component, OnInit } from '@angular/core';
import { ListService, PagedResultDto } from '@abp/ng.core';
import { FeedbackService } from '@proxy/market-place/feedback-masters';
import { FeedbackDto } from '@proxy/market-place/feedback-masters/dtos';

@Component({
  selector: 'app-users-feedback',
  templateUrl: './users-feedback.component.html',
  styleUrls: ['./users-feedback.component.scss'],
  providers: [ListService],
})
export class UsersFeedbackComponent implements OnInit {

  userfeedbacklist = { items: [], totalCount: 0 } as PagedResultDto<FeedbackDto>;


  constructor(public readonly list: ListService ,
              private feedbackService: FeedbackService,
             
              ) { }

  ngOnInit(): void {
    const userfeedbackStreamCreator = (query) => this.feedbackService.getList(query);

    this.list.hookToQuery(userfeedbackStreamCreator).subscribe((response) => {
      this.userfeedbacklist = response;
      console.log(response);
    });
  }
}
