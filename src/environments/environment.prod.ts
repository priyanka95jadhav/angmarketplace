import { Environment } from '@abp/ng.core';

const baseUrl = 'http://angmarketplace.projectnimbus.co.in';

export const environment = {
  production: true,
  application: {
    baseUrl,
    name: 'Marketplace',
    logoUrl: '',
  },
  oAuthConfig: {
    // issuer: 'https://marketplace.projectnimbus.co.in',
    // redirectUri: baseUrl,
    // clientId: 'Marketplace_App',
    // dummyClientSecret: '1q2w3e*',
    // scope: 'offline_access Marketplace',
    // requireHttps: true,
    issuer: 'https://marketplace.projectnimbus.co.in',
    redirectUri: baseUrl,
    clientId: 'Marketplace_App',
    grant_type: 'password',
    dummyClientSecret: '1q2w3e*',
    scope: 'MarketPlace',
    requireHttps: true,
  },
  apis: {
    default: {
      url: 'https://marketplace.projectnimbus.co.in',
      rootNamespace: 'Acme.Marketplace',
    },
  },
} as Environment;
